import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/controller_page.dart';
import 'package:review_subject/controllers/history_posts_controller.dart';
import 'package:review_subject/controllers/home_controller.dart';
import 'package:review_subject/models/list_post_model.dart';
import 'package:review_subject/pages/post_page/post_page.dart';
import 'package:review_subject/controllers/search_subject_controller.dart';
import 'constants.dart';

// ignore: non_constant_identifier_names
Widget MyAppBar(
    {required String title,
    bool isMore = false,
    PostResponse? data,
    required HomeController homeController,
    required HistoryPostController historyPostController,
    required SearchSubjectController searchSubjectController}) {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
    child: AppBar(
      elevation: 0.0,
      backgroundColor: kPrimaryColor,
      leading: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
          ),
          child: IconButton(
            onPressed: () {
              if (title == "Đăng bài") {
                Get.to(() => const ControllerPage(index: 0));
              } else {
                searchSubjectController.resetSearchSubjectData();
                homeController.ressetHomeData();
                historyPostController.ressetHistoryData();
                Get.back();
              }
            },
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
              size: 30,
            ),
          )),
      centerTitle: true,
      title: Text(
        title,
        style: const TextStyle(
            color: Colors.white, fontSize: 22, fontWeight: FontWeight.bold),
      ),
      actions: <Widget>[
        ///click on button to edit or delete post
        if (isMore == true)         
        IconButton(
            onPressed: () => Get.to(() => PostPage(isNewPost: false),
                arguments: {"data": data}),
            icon: const Icon(Icons.more_horiz))
      ],
    ),
  );
}
