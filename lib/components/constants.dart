import 'package:flutter/material.dart';

const Color kPrimaryColor = Color.fromRGBO(33, 158, 193, 1);
const Color kSecondaryColor = Color.fromRGBO(66, 183, 215, 1);
const TextStyle kTitleTextStyle = TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.black);
const TextStyle kSubTextStyle = TextStyle(fontSize: 14, color: Colors.black26);
const Color kSubTextColor = Colors.black26;
const Color kTextSecondaryColor = Color.fromRGBO(0, 91, 117, 1);
const Color kInputFieldColor = Color.fromARGB(255, 240, 240, 240);
const String kNoSubjectImage = "https://cf.shopee.vn/file/abc7f6f883fa468973201d1153e36c6f";
const String kNoUserAvatar = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqn6tPELJgjHUY9qp5OF9l_K0KAzrWeru6VA&usqp=CAU";