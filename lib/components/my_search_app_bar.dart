import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:review_subject/controllers/search_subject_controller.dart';
import 'package:review_subject/controller_page.dart';

import 'constants.dart';

// ignore: non_constant_identifier_names
Widget MySearchAppBar(
    {required String searchHint,
    required SearchSubjectController controller,
    required animationController,
    required context}) {
  return Container(
    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
    child: Column(
      children: [
        AppBar(
          elevation: 0.0,
          backgroundColor: kPrimaryColor,
          leading: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
              ),
              child: IconButton(
                onPressed: () {
                  Get.to(() => const ControllerPage(index: 0),
                      preventDuplicates: false);
                },
                icon: const Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                  size: 30,
                ),
              )),
          centerTitle: true,
          title: Container(
            height: 50,
            width: Get.width * 0.7,
            decoration: BoxDecoration(
                color: const Color.fromRGBO(241, 241, 241, 1),
                borderRadius: BorderRadius.circular(10)),
            child: TextField(
              onSubmitted: (value) {
                controller.currentSchool.value = "-- Chọn trường";
                controller.currentFaculty.value = "-- Chọn khoa";
                controller.facultyList.value = ["-- Chọn khoa"];
                controller.searchKey.value = value;
                controller.onSearchWithKey();
              },
              controller: controller.searchController.value,
              maxLines: 1,
              decoration: InputDecoration(
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 16, horizontal: 5),
                  prefixIcon: (controller.isLoading.isTrue)
                      ? Container(
                          padding: const EdgeInsets.all(10),
                          height: 25,
                          width: 25,
                          child: const Center(
                              child: CircularProgressIndicator(
                            color: Colors.black26,
                          )),
                        )
                      : const Icon(
                          Icons.search,
                          size: 25,
                        ),
                  isDense: true,
                  hintText: searchHint,
                  hintStyle:
                      const TextStyle(fontSize: 15, color: Colors.black54),
                  border: InputBorder.none),
            ),
          ),
          actions: [buildAdvancedSearchButton(context, controller)],
        ),
      ],
    ),
  );
}

IconButton buildAdvancedSearchButton(
    context, SearchSubjectController controller) {
  return IconButton(
    icon: SvgPicture.asset(
      "assets/icons/settings.svg",
      width: 30,
      color: Colors.white,
    ),
    onPressed: () {
      showModalBottomSheet(
          backgroundColor: Colors.white,
          context: context,
          builder: (context) {
            return Obx(() => Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        child: Container(
                          margin: const EdgeInsets.only(top: 10),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 3),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                              boxShadow: const [
                                BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 10,
                                    spreadRadius: 1)
                              ]),
                          child: DropdownButtonFormField<String>(
                            menuMaxHeight: Get.height * 0.4,
                            decoration: const InputDecoration(
                                border: InputBorder.none,
                                prefixIcon: Icon(
                                  Icons.work_history_rounded,
                                  size: 25,
                                )),
                            isExpanded: true,
                            value: controller.currentSchool.value,
                            icon: const Icon(Icons.arrow_drop_down),
                            elevation: 16,
                            iconSize: 25,
                            onChanged: (String? value) {
                              // This is called when the user selects an item.
                              controller.currentSchool.value = value!;
                              controller.loadFacultyBySchoolId(
                                  context: context,
                                  id: controller.schoolListId[
                                      controller.currentSchool.value]!);
                            },
                            items: controller.schoolList
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  maxLines: 1,
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        child: Container(
                          margin: const EdgeInsets.only(top: 10),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 3),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                              boxShadow: const [
                                BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 10,
                                    spreadRadius: 1)
                              ]),
                          child: DropdownButtonFormField<String>(
                            decoration: const InputDecoration(
                                border: InputBorder.none,
                                prefixIcon: Icon(
                                  Icons.school,
                                  size: 25,
                                )),
                            isExpanded: true,
                            value: controller.currentFaculty.value,
                            icon: const Icon(Icons.arrow_drop_down),
                            elevation: 16,
                            iconSize: 25,
                            onChanged: (String? value) {
                              // This is called when the user selects an item.

                              // controller.checkValidFaculty();
                              controller.currentFaculty.value = value!;
                            },
                            items: controller.facultyList
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  maxLines: 1,
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                      const SizedBox(height: 12),
                      GestureDetector(
                        onTap: () {
                          controller.searchKey.value =
                              controller.searchController.value.text;
                          controller.onSearchWithKey();
                          Get.back();
                        },
                        child: Container(
                          margin: const EdgeInsets.only(top: 16),
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 24),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: const [
                                BoxShadow(
                                    spreadRadius: 1,
                                    blurRadius: 10,
                                    color: Colors.black12)
                              ],
                              color: kPrimaryColor),
                          child: const Text(
                            'Tìm kiếm',
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                      const SizedBox(height: 5)
                    ],
                  ),
                ));
          });
    },
  );
}
