import 'package:flutter/material.dart';
import 'package:review_subject/components/constants.dart';

// ignore: must_be_immutable
class InputBlank extends StatelessWidget {
  IconData prefixIcon;
  IconData suffixIcon;
  String title;
  String content;
  TextEditingController controller;

  InputBlank(
      {super.key,
      required this.prefixIcon,
      required this.suffixIcon,
      required this.title,
      required this.content,
      required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20, right: 25, left: 25),
      padding: const EdgeInsets.symmetric(horizontal: 7),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(color: Colors.black12, blurRadius: 10, spreadRadius: 1)
          ]),
      child: TextFormField(
        obscureText: suffixIcon == Icons.visibility_off,
        controller: controller,
        textInputAction: TextInputAction.done,
        cursorColor: kPrimaryColor,
        initialValue: (content.isNotEmpty) ? content : null,
        decoration: InputDecoration(
            filled: true,
            fillColor: Colors.transparent,
            // iconColor: Colors.black,
            contentPadding: const EdgeInsets.symmetric(vertical: 18),
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              borderSide: BorderSide.none,
            ),
            prefixIcon: Icon(
              prefixIcon,
              color: Colors.black54,
            ),
            suffixIcon: (suffixIcon == Icons.remove_red_eye || suffixIcon == Icons.visibility_off)
                ? Icon(
                    suffixIcon,
                    color: Colors.black54,
                  )
                : null,
            hintStyle: const TextStyle(color: Colors.black45),
            hintText: title),
      ),
    );
  }
}
