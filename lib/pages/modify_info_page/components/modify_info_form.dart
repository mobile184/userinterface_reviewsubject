// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/modify_info_form_controller.dart';
import 'package:review_subject/models/profile_model.dart';
import 'package:review_subject/components/input_blank.dart';

class ModifyInfoForm extends StatelessWidget {
  late Profile userProfile;
  ModifyInfoForm({super.key, required this.userProfile});

  @override
  Widget build(BuildContext context) {
    ModifyInfoFormController controller = Get.find();

    Future.delayed(Duration.zero, () => controller.loadData(context: context));

    controller.fullNameTextController.value.text = userProfile.user.fullname;
    controller.emailTextController.value.text = userProfile.user.email;
    controller.majorTextController.value.text = userProfile.user.major;

    return Obx(() => Column(
          children: [
            SizedBox(height: Get.width * 0.12),
            const Text('Chỉnh sửa thông tin',
                style: TextStyle(fontSize: 27, fontWeight: FontWeight.bold)),
            const SizedBox(height: 25),
            InputBlank(
                prefixIcon: Icons.person,
                suffixIcon: Icons.abc,
                title: 'Họ và tên',
                content: '',
                controller: controller.fullNameTextController.value),
            InputBlank(
                prefixIcon: Icons.email,
                suffixIcon: Icons.abc,
                title: 'Email',
                content: '',
                controller: controller.emailTextController.value),

            /// Drop Down Form of University
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Container(
                margin: const EdgeInsets.only(top: 24),
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 2),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.black12,
                          blurRadius: 10,
                          spreadRadius: 1)
                    ]),
                child: DropdownButtonFormField<String>(
                  decoration: const InputDecoration(
                      border: InputBorder.none,
                      prefixIcon: Icon(
                        Icons.build,
                        size: 25,
                      )),
                  isExpanded: true,
                  value: controller.currentSchool.value,
                  icon: const Icon(Icons.arrow_drop_down),
                  elevation: 16,
                  iconSize: 25,
                  onChanged: (String? value) {
                    // This is called when the user selects an item.

                    controller.currentSchool.value = value!;
                    controller.loadFacultyBySchoolId(
                        context: context,
                        id: controller
                            .schoolListId[controller.currentSchool.value]!);
                  },
                  items: controller.schoolList
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        maxLines: 1,
                      ),
                    );
                  }).toList(),
                ),
              ),
            ),

            /// Drop Down Form of Faculty
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Container(
                margin: const EdgeInsets.only(top: 24),
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 2),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.black12,
                          blurRadius: 10,
                          spreadRadius: 1)
                    ]),
                child: DropdownButtonFormField<String>(
                  decoration: const InputDecoration(
                      border: InputBorder.none,
                      prefixIcon: Icon(
                        Icons.book,
                        size: 25,
                      )),
                  isExpanded: true,
                  value: controller.currentFaculty.value,
                  icon: const Icon(Icons.arrow_drop_down),
                  elevation: 16,
                  iconSize: 25,
                  onChanged: (String? value) {
                    // This is called when the user selects an item.

                    controller.checkValidFaculty();
                    controller.currentFaculty.value = value!;
                  },
                  items: controller.facultyList
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        maxLines: 1,
                      ),
                    );
                  }).toList(),
                ),
              ),
            ),

            /// Check validation of Input Faculty
            Padding(
              padding: const EdgeInsets.only(right: 24, top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    controller.isValidFaculty.value == true
                        ? ""
                        : "Khoa không hợp lệ",
                    style: const TextStyle(color: Colors.red, fontSize: 14),
                  )
                ],
              ),
            ),

            InputBlank(
              prefixIcon: Icons.history_edu,
              suffixIcon: Icons.abc,
              title: 'Ngành',
              content: '',
              controller: controller.majorTextController.value,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: GestureDetector(
                onTap: () {
                  controller.onButtonChangeClick(
                      context: context, profile: userProfile);
                },
                child: Container(
                  height: Get.height * 0.065,
                  width: Get.width * 0.7,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: kPrimaryColor.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 10,
                          offset:
                              const Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(30)),
                  child: const Center(
                    child: Text(
                      'Đồng ý',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}
