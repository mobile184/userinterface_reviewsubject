import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/models/profile_model.dart';
import 'package:review_subject/pages/modify_info_page/components/modify_info_form.dart';

class ModifyInfoPage extends StatelessWidget {
  const ModifyInfoPage({super.key});

  @override
  Widget build(BuildContext context) {
    Profile userProfile = Get.arguments['info'];
    return Scaffold(
        body: SingleChildScrollView(
      scrollDirection: Axis.vertical,
      physics: const BouncingScrollPhysics(),
      child: Stack(children: [
        Padding(
          padding: EdgeInsets.only(top: Get.height * 0.1),
          child: Column(
            children: const [
              Image(image: AssetImage("assets/bg_sign_up/bg_sign_up_blue.png")),
              Image(image: AssetImage("assets/bg_sign_up/bg_sign_up_pink.png")),
            ],
          ),
        ),
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        GestureDetector(
                            onTap: () {
                              showDialogExit(context);
                            },
                            child: const Icon(Icons.arrow_back_ios_new))
                      ],
                    ),
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            ),
            ModifyInfoForm(userProfile: userProfile)
          ],
        ),
      ]),
    ));
  }

  void showDialogExit(context) {
    AwesomeDialog(
      context: context,
      animType: AnimType.scale,
      dialogType: DialogType.warning,
      btnOkColor: const Color.fromARGB(255, 255, 174, 0),
      // btnOkColor: Colors.green,
      btnCancelColor: Colors.grey,
      body: Center(
        child: Column(
          children: const [
            SizedBox(height: 10),
            Text(
              'Hủy các thay đổi?',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
            ),
            SizedBox(height: 7),
            Text('Bạn có chắc chắn muốn thoát')
          ],
        ),
      ),
      title: 'This is Ignored',
      desc: 'This is also Ignored',
      btnOkOnPress: () {
        Get.back();
      },
      btnCancelOnPress: () {},
    ).show();
  }
}
