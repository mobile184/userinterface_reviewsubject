import 'package:flutter/material.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/pages/sign_in_page/components/body.dart';

class SignInPage extends StatelessWidget {
  const SignInPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: kPrimaryColor,
      body: Body(),
    );
  }
}