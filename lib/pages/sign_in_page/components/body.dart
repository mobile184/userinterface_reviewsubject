import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/pages/sign_in_page/components/sign_in_field.dart';

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Stack(
            children: [
              Column(
                children: [
                  Container(
                    width: double.maxFinite,
                    height: Get.height,
                    alignment: Alignment.bottomCenter,
                    decoration: const BoxDecoration(color: kPrimaryColor),
                    child: Container(
                      width: double.maxFinite,
                      height: Get.height * 0.75,
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(35),
                              topRight: Radius.circular(35))),
                      child: const SignInField(),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: Get.height * 0.15),
                child: Center(
                  child: Positioned(
                      left: 0,
                      right: 0,
                      child: Container(
                        width: Get.height * 0.2,
                        height: Get.height * 0.2,
                        decoration: BoxDecoration(
                            border: Border.all(color: kPrimaryColor, width: 5.5),
                            shape: BoxShape.circle,
                            image: const DecorationImage(
                                image: AssetImage('assets/img_logo.png'))),
                      )),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
