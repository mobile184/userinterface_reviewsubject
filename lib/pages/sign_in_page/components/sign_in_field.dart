import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/network_controller.dart';
import 'package:review_subject/controllers/sign_in_controller.dart';
import 'package:review_subject/route_managements/routes.dart';

class SignInField extends StatelessWidget {
  const SignInField({super.key});

  @override
  Widget build(BuildContext context) {
    SignInController controller = Get.find();
    Get.put<NetworkController>(NetworkController());
    return Obx(
      () => Padding(
        padding: EdgeInsets.only(top: Get.height * 0.1),
        child: Column(
          children: [
            const Text('Chào mừng quay lại',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 32,
                    fontWeight: FontWeight.bold)),
            const SizedBox(height: 10),
            const Text(
              'Đăng nhập vào tài khoản',
              style: TextStyle(color: Colors.black54, fontSize: 16),
            ),

            // Ô Tài khoản
            Container(
              margin: const EdgeInsets.only(top: 50, right: 25, left: 25),
              padding: const EdgeInsets.symmetric(horizontal: 7),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black12, blurRadius: 10, spreadRadius: 1)
                  ]),
              child: TextFormField(
                controller: controller.userNameTextController.value,
                onChanged: (value) => controller.checkValidUserName(value),
                textInputAction: TextInputAction.done,
                cursorColor: kPrimaryColor,
                decoration: const InputDecoration(
                    filled: true,
                    fillColor: Colors.transparent,
                    // iconColor: Colors.black,
                    contentPadding: EdgeInsets.symmetric(vertical: 18),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      borderSide: BorderSide.none,
                    ),
                    prefixIconColor: Colors.black54,
                    prefixIcon: Icon(
                      Icons.person,
                      color: Colors.black54,
                    ),
                    hintStyle: TextStyle(color: Colors.black54),
                    hintText: "Tài khoản"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 24, top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    controller.isValidUserName.value == true
                        ? ""
                        : "Tài khoản không hợp lệ",
                    style: const TextStyle(color: Colors.red, fontSize: 14),
                  )
                ],
              ),
            ),
            // Ô Mat khau
            Container(
              margin: const EdgeInsets.only(top: 10, right: 25, left: 25),
              padding: const EdgeInsets.symmetric(horizontal: 7),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black12, blurRadius: 10, spreadRadius: 1)
                  ]),
              child: TextFormField(
                obscureText: !controller.isPassWordVisible.value,
                controller: controller.passwordTextController.value,
                onChanged: (value) => controller.checkValidPassword(value),
                textInputAction: TextInputAction.done,
                cursorColor: kPrimaryColor,
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.transparent,
                    // iconColor: Colors.black,
                    contentPadding: const EdgeInsets.symmetric(vertical: 18),
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      borderSide: BorderSide.none,
                    ),
                    prefixIcon: const Icon(
                      Icons.lock,
                      color: Colors.black54,
                    ),
                    suffixIcon: GestureDetector(
                      onTap: () => controller.isPassWordVisible.value =
                          !controller.isPassWordVisible.value,
                      child: Icon(
                        controller.isPassWordVisible.value == true
                            ? Icons.remove_red_eye
                            : Icons.visibility_off,
                        color: Colors.black54,
                      ),
                    ),
                    hintStyle: const TextStyle(color: Colors.black54),
                    hintText: "Mật khẩu"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 24, top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    controller.isValidPassword.value == true
                        ? ""
                        : "Mật khẩu không hợp lệ",
                    style: const TextStyle(color: Colors.red, fontSize: 14),
                  )
                ],
              ),
            ),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      Get.toNamed(kResetPasswordPage);
                    },
                    child: const Text(
                      'Quên mật khẩu ?',
                      style: TextStyle(color: Colors.black54),
                    ),
                  ),
                ],
              ),
            ),
            const Expanded(child: SizedBox()),
            Padding(
              padding: const EdgeInsets.only(bottom: 12.0, right: 35, left: 35),
              child: GestureDetector(
                onTap: () => controller.onSignInButtonClick(context: context),
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 17),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: kPrimaryColor.withOpacity(0.5),
                          spreadRadius: 1,
                          blurRadius: 7,
                          offset: const Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(12)),
                  child: const Center(
                    child: Text(
                      'Đăng nhập',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            ),
            const Text('hoặc', style: TextStyle(color: Colors.black45)),
            Padding(
              padding: const EdgeInsets.only(bottom: 40.0, top: 12, right: 35, left: 35),
              child: GestureDetector(
                onTap: () => controller.onToSignUpButtonClick(context: context),
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 17),
                  decoration: BoxDecoration(
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.black12,
                          spreadRadius: 1,
                          blurRadius: 5,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      color: Colors.white,
                      border: Border.all(color: Colors.black26),
                      borderRadius: BorderRadius.circular(12)),
                  child: const Center(
                    child: Text(
                      'Đăng ký',
                      style: TextStyle(
                          color: Colors.black38,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
