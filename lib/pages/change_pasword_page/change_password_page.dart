import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/components/input_blank.dart';
import 'package:review_subject/controllers/change_password_controller.dart';
import 'package:review_subject/route_managements/routes.dart';

class ChangePasswordPage extends StatelessWidget {
  const ChangePasswordPage({super.key});

  @override
  Widget build(BuildContext context) {
    ChangePasswordController controller = Get.find();

    return Scaffold(
        body: SingleChildScrollView(
      scrollDirection: Axis.vertical,
      physics: const BouncingScrollPhysics(),
      child: Stack(children: [
        Column(
          children: [
            SizedBox(
              height: Get.height,
              width: Get.width,
              child: const Image(
                image: AssetImage(
                    "assets/bg_reset_password/bg_reset_password.png"),
                fit: BoxFit.cover,
              ),
            ),
          ],
        ),
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        GestureDetector(
                            onTap: () {
                              controller.userNameTextController.value.clear();
                              controller.passwordTextController.value.clear();
                              controller.confirmPasswordTextController.value
                                  .clear();
                              Get.back();
                            },
                            child: const Icon(Icons.arrow_back_ios_new))
                      ],
                    ),
                  ),
                  SizedBox(height: Get.height * 0.16),
                  Obx(
                    () => Container(
                      width: Get.width * 0.8,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: const [
                            BoxShadow(
                              color: Colors.black12,
                              blurRadius: 10,
                              spreadRadius: 1,
                            )
                          ]),
                      child: Column(
                        children: [
                          const Padding(
                            padding: EdgeInsets.only(top: 22.0),
                            child: Text('Đổi mật khẩu',
                                style: TextStyle(
                                    fontSize: 27, fontWeight: FontWeight.bold)),
                          ),
                          const Padding(
                            padding: EdgeInsets.fromLTRB(30.0, 10, 30, 10),
                            child: Text(
                              'Nhập tài khoản và mật khẩu mới để thay đổi mật khẩu',
                              style: TextStyle(
                                  fontSize: 14, color: Colors.black54),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          InputBlank(
                              controller:
                                  controller.userNameTextController.value,
                              prefixIcon: Icons.person,
                              suffixIcon: Icons.abc,
                              title: 'Tài khoản',
                              content: ''),
                          (!controller.isValidUserName.value)
                              ? Padding(
                                  padding:
                                      const EdgeInsets.only(right: 24, top: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: const [
                                      Text(
                                        "Tài khoản không hợp lệ",
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 14),
                                      )
                                    ],
                                  ),
                                )
                              : Container(),
                          InputBlank(
                              controller:
                                  controller.passwordTextController.value,
                              prefixIcon: Icons.lock,
                              suffixIcon: Icons.abc,
                              title: 'Mật khẩu',
                              content: ''),
                          (!controller.isValidPassword.value)
                              ? Padding(
                                  padding:
                                      const EdgeInsets.only(right: 24, top: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: const [
                                      Text(
                                        "Mật khẩu không hợp lệ",
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 14),
                                      )
                                    ],
                                  ),
                                )
                              : Container(),
                          InputBlank(
                              controller: controller
                                  .confirmPasswordTextController.value,
                              prefixIcon: Icons.lock,
                              suffixIcon: Icons.abc,
                              title: 'Nhập lại mật khẩu',
                              content: ''),
                          (!controller.isValidConfirmPassword.value)
                              ? Padding(
                                  padding:
                                      const EdgeInsets.only(right: 24, top: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: const [
                                      Text(
                                        "Mật khẩu không trùng khớp",
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 14),
                                      )
                                    ],
                                  ),
                                )
                              : Container(),
                          GestureDetector(
                            onTap: () {
                              onClickOk(controller, context);
                            },
                            child: Container(
                              margin:
                                  const EdgeInsets.only(top: 22, bottom: 32),
                              height: Get.height * 0.065,
                              width: Get.width * 0.7,
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: kPrimaryColor.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 10,
                                      offset: const Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                  color: kPrimaryColor,
                                  borderRadius: BorderRadius.circular(30)),
                              child: const Center(
                                child: Text(
                                  'Đồng ý',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ]),
    ));
  }

  /// Catch event user click Ok
  /// 
  /// Transmiss 2 params [controller] and [context]. 
  /// Check validation for username, password, confirm-password.
  /// If validated, send request to change password and back to previous page.
  /// If invalidated, show error.
  void onClickOk(ChangePasswordController controller, BuildContext context) {
    if (controller.userNameTextController.value.text == '') {
      controller.isValidUserName.value = false;
    } else {
      controller.isValidUserName.value = true;
    }
    if (controller.passwordTextController.value.text == '') {
      controller.isValidPassword.value = false;
    } else {
      controller.isValidPassword.value = true;
    }
    if (controller.confirmPasswordTextController.value.text == '' ||
        controller.confirmPasswordTextController.value.text !=
            controller.passwordTextController.value.text) {
      controller.isValidConfirmPassword.value = false;
    } else {
      controller.isValidConfirmPassword.value = true;
    }

    // print(
    //     'username: ${controller.userNameTextController.value.text} - ${controller.isValidUserName.value}');
    // print(
    //     'password: ${controller.passwordTextController.value.text} - ${controller.isValidPassword.value}');
    // print(
    //     'confirm password: ${controller.confirmPasswordTextController.value.text} - ${controller.isValidConfirmPassword.value}');

    if (controller.isValidUserName.value &&
        controller.isValidPassword.value &&
        controller.isValidConfirmPassword.value) {
      controller.onOkButtonClick(
          context: context,
          username: controller.userNameTextController.value.text,
          password: controller.passwordTextController.value.text);

      controller.userNameTextController.value.clear();
      controller.passwordTextController.value.clear();
      Get.offAndToNamed(kSignInPage);
    }
  }
}
