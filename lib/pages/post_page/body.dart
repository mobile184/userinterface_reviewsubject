// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/components/my_app_bar.dart';
import 'package:review_subject/controllers/post_controller.dart';
import 'package:review_subject/controllers/search_subject_controller.dart';
import 'package:review_subject/pages/post_page/components/build_add_image.dart';

import '../../controllers/history_posts_controller.dart';
import '../../controllers/home_controller.dart';
import '../../models/list_post_model.dart';

class Body extends StatefulWidget {
  bool isNewPost;
  PostResponse? data;
  Body({super.key, this.isNewPost = true, this.data});

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  PostController controller = Get.put(PostController());
  HomeController homeController = Get.put(HomeController());
  HistoryPostController historyPostController =
      Get.put(HistoryPostController());
  SearchSubjectController searchSubjectController =
      Get.put(SearchSubjectController());
  @override
  void initState() {
    Future.delayed(
        Duration.zero,
        () => controller
            .loadData(context: context)
            .then((value) => setState(() {})));
    controller.loadDataFromLocal();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant Body oldWidget) {
    Future.delayed(
        Duration.zero,
        () => controller
            .loadData(context: context)
            .then((value) => setState(() {})));
    controller.loadDataFromLocal();
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    controller.resetController();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (controller.currentSchool.value == "Chọn trường") {
      return Container(
          color: Colors.white,
          child: const Center(
            child: CircularProgressIndicator(color: kPrimaryColor),
          ));
    } else {
      return Obx(
        () => Column(
          children: [
            MyAppBar(
                searchSubjectController: searchSubjectController,
                homeController: homeController,
                historyPostController: historyPostController,
                title: widget.isNewPost == true ? "Đăng bài" : "Chỉnh sửa bài"),
            Expanded(
              child: Container(
                width: Get.width,
                padding: const EdgeInsets.only(top: 32),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                ),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Thông tin",
                                style: kTitleTextStyle.copyWith(
                                    color: kTextSecondaryColor, fontSize: 24),
                              ),
                              const Divider(
                                thickness: 2,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Text(
                                    "Trường",
                                    style: TextStyle(
                                        color: kTextSecondaryColor,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Container(
                                    width: Get.width - 32,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        border: Border.all(
                                            width: 1, color: Colors.black26)),
                                    margin: const EdgeInsets.only(top: 10),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16, vertical: 10),
                                    child: Obx(
                                      () => DropdownButtonFormField<String>(
                                        decoration: const InputDecoration(
                                            border: InputBorder.none,
                                            prefixIcon: Icon(
                                              Icons.build,
                                              size: 25,
                                            )),
                                        isExpanded: true,
                                        value: controller.currentSchool.value,
                                        icon: const Icon(Icons.arrow_drop_down),
                                        elevation: 16,
                                        iconSize: 25,
                                        onChanged: (String? value) {
                                          // This is called when the user selects an item.
                                          setState(() {
                                            controller.currentSchool.value =
                                                value!;
                                            controller.loadFacultyBySchoolId(
                                                context: context,
                                                id: controller.schoolListId[
                                                    controller
                                                        .currentSchool.value]!);
                                          });
                                        },
                                        items: controller.schoolList
                                            .map<DropdownMenuItem<String>>(
                                                (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(
                                              value,
                                              maxLines: 1,
                                            ),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Text(
                                    "Khoa",
                                    style: TextStyle(
                                        color: kTextSecondaryColor,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Container(
                                    width: Get.width - 32,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        border: Border.all(
                                            width: 1, color: Colors.black26)),
                                    margin: const EdgeInsets.only(top: 10),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16, vertical: 10),
                                    child: Obx(
                                      () => DropdownButtonFormField<String>(
                                        decoration: const InputDecoration(
                                            border: InputBorder.none,
                                            prefixIcon: Icon(
                                              Icons.build,
                                              size: 25,
                                            )),
                                        isExpanded: true,
                                        value: controller.currentFaculty.value,
                                        icon: const Icon(Icons.arrow_drop_down),
                                        elevation: 16,
                                        iconSize: 25,
                                        onChanged: (String? value) {
                                          // This is called when the user selects an item.
                                          setState(() {
                                            controller.currentFaculty.value =
                                                value!;
                                          });
                                        },
                                        items: controller.facultyList
                                            .map<DropdownMenuItem<String>>(
                                                (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(value),
                                          );
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        right: 24, top: 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          controller.isValidFaculty.value ==
                                                  true
                                              ? ""
                                              : "Vui lòng chọn khoa",
                                          style: const TextStyle(
                                              color: Colors.red, fontSize: 14),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              buildMyTextInput(
                                  title: "Ngành",
                                  image: "branch",
                                  textController: controller.majorController,
                                  controller: controller),
                              Padding(
                                padding:
                                    const EdgeInsets.only(right: 24, top: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(
                                      controller.isValidMajor.value == true
                                          ? ""
                                          : "Ngành không hợp lệ",
                                      style: const TextStyle(
                                          color: Colors.red, fontSize: 14),
                                    )
                                  ],
                                ),
                              ),
                              buildMyTextInput(
                                  title: "Môn học",
                                  image: "subject",
                                  textController: controller.subjectController,
                                  controller: controller),
                              Padding(
                                padding:
                                    const EdgeInsets.only(right: 24, top: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(
                                      controller.isValidSubject.value == true
                                          ? ""
                                          : "Môn học không hợp lệ",
                                      style: const TextStyle(
                                          color: Colors.red, fontSize: 14),
                                    )
                                  ],
                                ),
                              ),
                              buildMyTextInput(
                                  title: "Giảng viên",
                                  image: "teacher",
                                  textController: controller.teacherController,
                                  controller: controller),
                              Padding(
                                padding:
                                    const EdgeInsets.only(right: 24, top: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(
                                      controller.isValidTeacher.value == true
                                          ? ""
                                          : "Giảng viên không hợp lệ",
                                      style: const TextStyle(
                                          color: Colors.red, fontSize: 14),
                                    )
                                  ],
                                ),
                              ),
                              buildMyTextInput(
                                  title: "Giáo trình",
                                  image: "link",
                                  canAdd: true,
                                  textController:
                                      controller.cirriculumController,
                                  controller: controller),
                              Obx(
                                () => Column(
                                  children: [
                                    for (int i = 0;
                                        i < controller.cirriculumList.length;
                                        i++)
                                      buildCirriculumItem(
                                          data: controller.cirriculumList[i],
                                          index: i,
                                          controller: controller),
                                  ],
                                ),
                              ),
                              Text(
                                "Đánh giá",
                                style: kTitleTextStyle.copyWith(
                                    color: kTextSecondaryColor, fontSize: 24),
                              ),
                              const Divider(
                                thickness: 2,
                              ),
                              buildRatingInput(
                                  title: "Độ khó",
                                  reasonController:
                                      controller.hardRateReasonController,
                                  rate: controller.hardRate),
                              if (controller.hardRate.value < 1)
                                Padding(
                                  padding:
                                      const EdgeInsets.only(right: 24, top: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: const [
                                      Text(
                                        "Vui lòng đánh giá độ khó",
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 14),
                                      )
                                    ],
                                  ),
                                ),
                              buildRatingInput(
                                  title: "Yêu thích",
                                  reasonController:
                                      controller.likeRateReasonController,
                                  rate: controller.likeRate),
                              if (controller.likeRate.value < 1)
                                Padding(
                                  padding:
                                      const EdgeInsets.only(right: 24, top: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: const [
                                      Text(
                                        "Vui lòng đánh giá độ yêu thích",
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 14),
                                      )
                                    ],
                                  ),
                                ),
                              buildRatingInput(
                                  title: "Kỳ thi",
                                  reasonController:
                                      controller.examRateReasonController,
                                  rate: controller.examRate),
                              if (controller.examRate.value < 1)
                                Padding(
                                  padding:
                                      const EdgeInsets.only(right: 24, top: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: const [
                                      Text(
                                        "Vui lòng đánh giá kỳ thi",
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 14),
                                      )
                                    ],
                                  ),
                                ),
                              const SizedBox(
                                height: 16,
                              ),
                              Text(
                                "File phương tiện",
                                style: kTitleTextStyle.copyWith(
                                    color: kTextSecondaryColor, fontSize: 24),
                              ),
                              const Divider(
                                thickness: 2,
                              ),
                              BuildAddImage(controller: controller),
                            ],
                          )),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }
  }

  /// Build Curriculum Item
  ///
  /// Transmit [data], [index] and [controller]
  /// Return Widget of Curriculum
  Widget buildCirriculumItem(
      {required String data, required index, required controller}) {
    return Row(
      children: [
        IconButton(
          onPressed: () {
            setState(() {
              controller.removeCirriculum(index);
            });
          },
          icon: const Icon(
            Icons.close_outlined,
            color: Colors.red,
            size: 25,
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        Expanded(
          child: Text(
            data,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
                fontStyle: FontStyle.italic,
                decoration: TextDecoration.underline,
                color: kSecondaryColor,
                fontSize: 18),
          ),
        )
      ],
    );
  }

  /// Build Rating Input
  ///
  /// Transmit [title], [reasonController] and [rate]
  /// Return Widget of Rating Bar and Rating Comment
  Widget buildRatingInput(
      {required String title, required reasonController, required rate}) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Row(
          children: [
            Text(
              title,
              style: const TextStyle(
                  fontSize: 20,
                  color: kTextSecondaryColor,
                  fontWeight: FontWeight.bold),
            ),
            const Spacer(),
            Obx(
              () => RatingBar.builder(
                itemSize: 35,
                initialRating: rate.value * 1.0,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: false,
                itemCount: 5,
                itemPadding: const EdgeInsets.symmetric(horizontal: 0.0),
                itemBuilder: (context, _) => const Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: (rating) {
                  rate.value = rating.toInt();
                },
              ),
            ),
          ],
        ),
        Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(width: 1, color: Colors.black26)),
            child: Obx(
              () => TextFormField(
                controller: reasonController.value,
                textAlignVertical: TextAlignVertical.top,
                minLines: 4,
                maxLines: 4,
                decoration: InputDecoration(
                    isDense: true,
                    border: InputBorder.none,
                    prefix: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10,
                        ),
                        child: SvgPicture.asset(
                          "assets/icons/comment.svg",
                          width: 30,
                          height: 30,
                        ))),
              ),
            )),
      ],
    );
  }

  /// Build Curriculum Item
  ///
  /// Transmit [title], [image], [canAdd], [textController] and [controller]
  /// Return Widget of Input field
  Widget buildMyTextInput(
      {required String title,
      required String image,
      bool canAdd = false,
      required Rx<TextEditingController> textController,
      required controller}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 10,
        ),
        Text(
          title,
          style: const TextStyle(
              color: kTextSecondaryColor,
              fontSize: 20,
              fontWeight: FontWeight.bold),
        ),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              border: Border.all(width: 1, color: Colors.black26)),
          margin: const EdgeInsets.only(top: 10),
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
          child: Obx(
            () => TextFormField(
              controller: textController.value,
              decoration: InputDecoration(
                  isDense: true,
                  border: InputBorder.none,
                  contentPadding: const EdgeInsets.symmetric(vertical: 10),
                  prefixIconConstraints:
                      const BoxConstraints(maxWidth: 35, maxHeight: 35),
                  prefixIcon: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: SvgPicture.asset(
                      "assets/icons/$image.svg",
                      color: Colors.black,
                    ),
                  ),
                  suffixIconConstraints:
                      const BoxConstraints(maxHeight: 30, maxWidth: 30),
                  suffixIcon: canAdd == true
                      ? GestureDetector(
                          onTap: () {
                            setState(() {
                              if (textController.value.text != "") {
                                controller
                                    .addCirriculum(textController.value.text);
                              }
                            });
                            textController.value.text = "";
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.green.shade400,
                                shape: BoxShape.circle),
                            padding: const EdgeInsets.all(5),
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                          ),
                        )
                      : Container()),
            ),
          ),
        ),
      ],
    );
  }
}
