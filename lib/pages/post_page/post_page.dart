// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:review_subject/models/list_post_model.dart';

import '../../components/constants.dart';
import 'body.dart';

class PostPage extends StatelessWidget {
  bool isNewPost;
  PostResponse? data;
  PostPage({super.key, this.isNewPost = true, this.data});

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      backgroundColor: kPrimaryColor,
      body: Body(isNewPost: isNewPost,)));
  }
}