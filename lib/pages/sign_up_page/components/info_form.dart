// ignore_for_file: unrelated_type_equality_checks

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/sign_up_controller.dart';
import 'package:review_subject/components/input_blank.dart';

import '../../../controllers/network_controller.dart';

class InfoForm extends StatefulWidget {
  const InfoForm({super.key});

  @override
  State<InfoForm> createState() => _InfoFormState();
}

class _InfoFormState extends State<InfoForm> {
  SignUpController controller = Get.find();
  @override
  void initState() {
    Future.delayed(Duration.zero, () => controller.loadData(context: context))
        .then((value) => setState(() {}));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Get.put<NetworkController>(NetworkController());
    return controller.currentSchool == "Chọn trường"
        ? Container()
        : Obx(
            () => Column(
              children: [
                InputBlank(
                    controller: controller.userNameTextController.value,
                    prefixIcon: Icons.person,
                    suffixIcon: Icons.abc,
                    title: 'Tài khoản',
                    content: ""),
                (controller.isValidUserName.value == false)
                    ? Padding(
                        padding: const EdgeInsets.only(right: 24, top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const [
                            Text(
                              "Tài khoản không hợp lệ",
                              style: TextStyle(color: Colors.red, fontSize: 12),
                            )
                          ],
                        ),
                      )
                    : Container(),

                // Ô Mat khau
                Container(
                  margin: const EdgeInsets.only(top: 20, right: 25, left: 25),
                  padding: const EdgeInsets.symmetric(horizontal: 7),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                      boxShadow: const [
                        BoxShadow(
                            color: Colors.black12,
                            blurRadius: 10,
                            spreadRadius: 1)
                      ]),
                  child: TextFormField(
                    obscureText: !controller.isPassWordVisible.value,
                    controller: controller.passwordTextController.value,
                    onChanged: (value) => controller.checkValidPassword(),
                    textInputAction: TextInputAction.done,
                    cursorColor: kPrimaryColor,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.transparent,
                        contentPadding: const EdgeInsets.symmetric(vertical: 18),
                        border: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide: BorderSide.none,
                        ),
                        prefixIcon: const Icon(
                          Icons.lock,
                          color: Colors.black54,
                        ),
                        suffixIcon: GestureDetector(
                          onTap: () => controller.isPassWordVisible.value =
                              !controller.isPassWordVisible.value,
                          child: Icon(
                            controller.isPassWordVisible.isTrue
                                ? Icons.remove_red_eye
                                : Icons.visibility_off,
                            color: Colors.black54,
                          ),
                        ),
                        hintStyle: const TextStyle(color: Colors.black54),
                        hintText: "Mật khẩu"),
                  ),
                ),
                (controller.isValidPassword.value == false)
                    ? Padding(
                        padding: const EdgeInsets.only(right: 24, top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const [
                            Text(
                              "Mật khẩu không hợp lệ",
                              style: TextStyle(color: Colors.red, fontSize: 14),
                            )
                          ],
                        ),
                      )
                    : Container(),
                Container(
                  margin: const EdgeInsets.only(top: 20, right: 25, left: 25),
                  padding: const EdgeInsets.symmetric(horizontal: 7),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                      boxShadow: const [
                        BoxShadow(
                            color: Colors.black12,
                            blurRadius: 10,
                            spreadRadius: 1)
                      ]),
                  child: TextFormField(
                    obscureText: !controller.isConfirmPassWordVisible.value,
                    controller: controller.passwordConfirmTextController.value,
                    onChanged: (value) =>
                        controller.checkValidPasswordConfirm(),
                    textInputAction: TextInputAction.done,
                    cursorColor: kPrimaryColor,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.transparent,
                        contentPadding: const EdgeInsets.symmetric(vertical: 18),
                        border: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide: BorderSide.none,
                        ),
                        prefixIcon: const Icon(
                          Icons.lock,
                          color: Colors.black54,
                        ),
                        suffixIcon: GestureDetector(
                          onTap: () =>
                              controller.isConfirmPassWordVisible.value =
                                  !controller.isConfirmPassWordVisible.value,
                          child: Icon(
                            controller.isConfirmPassWordVisible.isTrue
                                ? Icons.remove_red_eye
                                : Icons.visibility_off,
                            color: Colors.black54,
                          ),
                        ),
                        hintStyle: const TextStyle(color: Colors.black54),
                        hintText: "Nhập lại mật khẩu"),
                  ),
                ),
                (controller.isValidPasswordConfirm.value == false)
                    ? Padding(
                        padding: const EdgeInsets.only(right: 24, top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const [
                            Text(
                              "Mật khẩu không khớp",
                              style: TextStyle(color: Colors.red, fontSize: 14),
                            )
                          ],
                        ),
                      )
                    : Container(),
                InputBlank(
                    controller: controller.fullNameTextController.value,
                    prefixIcon: Icons.person,
                    suffixIcon: Icons.abc,
                    title: 'Họ và tên',
                    content: ''),
                (controller.isValidFullName.value == false)
                    ? Padding(
                        padding: const EdgeInsets.only(right: 24, top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const [
                            Text(
                              "Họ tên không hợp lệ",
                              style: TextStyle(color: Colors.red, fontSize: 14),
                            )
                          ],
                        ),
                      )
                    : Container(),
                InputBlank(
                    controller: controller.emailTextController.value,
                    prefixIcon: Icons.email,
                    suffixIcon: Icons.abc,
                    title: 'Email',
                    content: ''),
                (controller.isValidEmail.value == false)
                    ? Padding(
                        padding: const EdgeInsets.only(right: 24, top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const [
                            Text(
                              "Email không hợp lệ",
                              style: TextStyle(color: Colors.red, fontSize: 14),
                            )
                          ],
                        ),
                      )
                    : Container(),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Container(
                    margin: const EdgeInsets.only(top: 24),
                    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 3),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.black12,
                              blurRadius: 10,
                              spreadRadius: 1)
                        ]),
                    child: DropdownButtonFormField<String>(
                      decoration: const InputDecoration(
                          border: InputBorder.none,
                          prefixIcon: Icon(
                            Icons.build,
                            size: 25,
                          )),
                      isExpanded: true,
                      value: controller.currentSchool.value,
                      icon: const Icon(Icons.arrow_drop_down),
                      elevation: 16,
                      iconSize: 25,
                      onChanged: (String? value) {
                        // This is called when the user selects an item.
                        setState(() {
                          controller.currentSchool.value = value!;
                          controller.loadFacultyBySchoolId(
                              context: context,
                              id: controller.schoolListId[
                                  controller.currentSchool.value]!);
                        });
                      },
                      items: controller.schoolList
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, maxLines: 1,),
                        );
                      }).toList(),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Container(
                    margin: const EdgeInsets.only(top: 24),
                    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 3),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.black12,
                              blurRadius: 10,
                              spreadRadius: 1)
                        ]),
                    child: DropdownButtonFormField<String>(
                      decoration: const InputDecoration(
                          border: InputBorder.none,
                          prefixIcon: Icon(
                            Icons.book,
                            size: 25,
                          )),
                      isExpanded: true,
                      value: controller.currentFaculty.value,
                      icon: const Icon(Icons.arrow_drop_down),
                      elevation: 16,
                      iconSize: 25,
                      onChanged: (String? value) {
                        // This is called when the user selects an item.
                        setState(() {
                          controller.checkValidFaculty();
                          controller.currentFaculty.value = value!;
                        });
                      },
                      items: controller.facultyList
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, maxLines: 1,),
                        );
                      }).toList(),
                    ),
                  ),
                ),
                (controller.isValidFaculty.value == false)
                    ? Padding(
                        padding: const EdgeInsets.only(right: 24, top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const [
                            Text(
                              "Khoa không hợp lệ",
                              style: TextStyle(color: Colors.red, fontSize: 14),
                            )
                          ],
                        ),
                      )
                    : Container(),
                InputBlank(
                    controller: controller.majorTextController.value,
                    prefixIcon: Icons.history_edu,
                    suffixIcon: Icons.abc,
                    title: 'Ngành',
                    content: ''),
                Padding(
                  padding: const EdgeInsets.only(top: 30.0, bottom: 10),
                  child: GestureDetector(
                    onTap: () {
                      controller.onSignUpButtonClick(context);
                    },
                    child: Container(
                      height: Get.height * 0.065,
                      width: Get.width * 0.7,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: kPrimaryColor.withOpacity(0.5),
                              spreadRadius: 2,
                              blurRadius: 10,
                              offset:
                                  const Offset(0, 3), // changes position of shadow
                            ),
                          ],
                          color: kPrimaryColor,
                          borderRadius: BorderRadius.circular(30)),
                      child: const Center(
                        child: Text(
                          'Đăng ký',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: Get.width * 0.65,
                  margin: const EdgeInsets.only(bottom: 20),
                  child: const Center(
                    child: Text(
                      'Khi đăng ký, bạn sẽ đồng ý với các Chính sách & Điều khoản của chúng tôi',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 12, color: Colors.black54),
                    ),
                  ),
                )
              ],
            ),
          );
  }
}