import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/pages/sign_up_page/components/info_form.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      scrollDirection: Axis.vertical,
      physics: const BouncingScrollPhysics(),
      child: Stack(children: [
        Padding(
          padding: EdgeInsets.only(top: Get.height * 0.1),
          child: Column(
            children: const [
              Image(image: AssetImage("assets/bg_sign_up/bg_sign_up_blue.png")),
              Image(image: AssetImage("assets/bg_sign_up/bg_sign_up_pink.png")),
            ],
          ),
        ),
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 45),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        GestureDetector(
                            onTap: () {
                              Get.back();
                            },
                            child: const Icon(Icons.arrow_back_ios_new))
                      ],
                    ),
                  ),
                  const SizedBox(height: 10),
                  const Text('Tạo tài khoản mới',
                      style:
                          TextStyle(fontSize: 27, fontWeight: FontWeight.bold))
                ],
              ),
            ),
            const InfoForm()
          ],
        ),
      ]),
    ));
  }
}
