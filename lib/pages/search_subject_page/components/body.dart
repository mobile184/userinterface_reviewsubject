// ignore_for_file: invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/search_subject_controller.dart';
import 'package:review_subject/route_managements/routes.dart';
import 'package:review_subject/service/service.dart';

import '../../../components/my_search_app_bar.dart';
import '../../../models/list_post_model.dart';

class Body extends StatefulWidget {
  const Body({super.key});

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    SearchSubjectController controller = Get.put(SearchSubjectController());
    controller.loadData(context: context);
    // print('Num of UnI: ' + controller.schoolList.value.length.toString());

    ScrollController sc = ScrollController();
    return Obx(
      () => Column(
        children: [
          MySearchAppBar(
              searchHint: "Nhập tên môn học",
              controller: controller,
              animationController: controller.animationController,
              context: context),
          Expanded(
            flex: 9,
            child: Container(
              padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(12),
                      topRight: Radius.circular(12))),
              child: Column(
                children: [
                  Row(
                    children: const [
                      Text(
                        "Danh sách tìm kiếm",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  const Divider(
                    color: Colors.black26,
                    thickness: 1,
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      controller: sc,
                      scrollDirection: Axis.vertical,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              for (int i = 0;
                                  i < controller.listSubject.length;
                                  i++)
                                buildSearchSubjectItem(
                                    id: i,
                                    controller: controller,
                                    listSubject: controller.listSubject),
                              (controller.listSubject.value.isEmpty)
                                  ? Column(
                                      children: [
                                        SizedBox(height: Get.height * 0.2),
                                        Image(
                                            image: const NetworkImage(
                                                'https://cdn-icons-png.flaticon.com/512/2748/2748614.png'),
                                            width: Get.width * 0.4,
                                            height: Get.width * 0.4),
                                      ],
                                    )
                                  : Container()
                            ]),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildSearchSubjectItem(
      {required int id,
      required SearchSubjectController controller,
      required RxList<PostResponse> listSubject}) {
    return Obx(
      () => GestureDetector(
        onTap: () => Get.toNamed(kDetailPage, arguments: {
          "userId": controller.userId,
          "postId": controller.listSubject.value[id].post.id,
          "token": controller.token,
          "userFullname": controller.userFullname,
          "userAvatar": controller.userAvatar,
        }),
        child: Container(
          width: Get.width * 0.9,
          height: 220,
          margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
              boxShadow: const [
                BoxShadow(
                  offset: Offset(0, 2),
                  color: Colors.black26,
                  spreadRadius: 1,
                  blurRadius: 5,
                )
              ]),
          child: Row(
            children: [
              Container(
                width: Get.width * 0.4,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                        image: NetworkImage(
                          listSubject[id].post.images.isNotEmpty &&
                                  listSubject[id].post.images[0] != ""
                              ? HttpService.baseUrl +
                                  listSubject[id].post.images[0]
                              : kNoSubjectImage,
                        ),
                        fit: BoxFit.cover)),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 16, right: 16, top: 7),
                      child: Text(
                        listSubject[id].post.subject,
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          RatingBar.builder(
                            ignoreGestures: true,
                            itemSize: 20,
                            initialRating:
                                listSubject[id].post.rateLike * 1.0,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: false,
                            itemCount: 5,
                            itemPadding: const EdgeInsets.symmetric(horizontal: 0.0),
                            itemBuilder: (context, _) => const Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            onRatingUpdate: (rating) {},
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              children: [
                                Image.asset(
                                  'assets/icons/ic_college.png',
                                  height: 22,
                                  width: 22,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                    child: Text(
                                  listSubject[id].universityName,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                )),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              children: [
                                Image.asset(
                                  'assets/icons/ic_faculty_history.png',
                                  height: 22,
                                  width: 22,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                    child: Text(
                                  listSubject.value[id].facultyName,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                )),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              children: [
                                Image.asset(
                                  'assets/icons/ic_teacher.png',
                                  height: 22,
                                  width: 22,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                    child: Text(
                                  listSubject[id].post.teacher,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                )),
                              ],
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 4),
                            margin: const EdgeInsets.symmetric(horizontal: 12),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: kPrimaryColor.withOpacity(0.07)),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      listSubject[id].post.likeCount
                                          .toString(),
                                      style: const TextStyle(
                                          color: kPrimaryColor, fontSize: 16),
                                    ),
                                    const SizedBox(width: 7),
                                    const Icon(
                                      Icons.thumb_up_outlined,
                                      color: kPrimaryColor,
                                    )
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      listSubject[id].post.commentCount
                                          .toString(),
                                      style: const TextStyle(
                                          color: kPrimaryColor, fontSize: 16),
                                    ),
                                    const SizedBox(width: 7),
                                    Container(
                                      padding: const EdgeInsets.only(top: 2),
                                      child: const Icon(
                                        Icons.comment_outlined,
                                        color: kPrimaryColor,
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 2),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
