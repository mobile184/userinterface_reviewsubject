import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/route_managements/routes.dart';

import 'components/body.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({super.key});

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 3), () => Get.offNamed(kSignInPage));
  }
  @override
  Widget build(BuildContext context) {
    return const SafeArea(child: Scaffold(body: Body(),
    backgroundColor: kPrimaryColor,));
  }
}