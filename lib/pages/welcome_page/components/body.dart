import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Lottie.asset("assets/lotties/girl_with_book.json", width: 300),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "Vui lòng đợi 1 lúc",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
              ),
              AnimatedTextKit(
                repeatForever: true,
                pause: const Duration(milliseconds: 0),
                animatedTexts: [
                        TypewriterAnimatedText("...",
                        cursor: '',
                        speed: const Duration(milliseconds: 150),
                    textStyle: const TextStyle(
                        fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white))
              ]),
            ],
          )
        ],
      ),
    );
  }
}
