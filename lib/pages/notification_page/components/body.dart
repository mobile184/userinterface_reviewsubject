import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/notification_controller.dart';
import 'package:review_subject/models/list_post_model.dart';
import 'package:review_subject/models/notification_model_new.dart';
import 'package:review_subject/route_managements/routes.dart';
import 'package:review_subject/service/service.dart';

class Body extends StatelessWidget {
  const Body({super.key});

  /// Load more data whenever user scroll over
  ///
  /// Tranmiss [controller], BuildContext [context] and [countPage] (number of page)
  Future<void> loadMoreData(
      NotificationController controller, context, countPage) async {
    controller.getAllNotifications(context: context, pageNum: countPage);
  }

  Future<bool> _loadMore(
      NotificationController controller, context, countPage) async {
    controller.isEnd.value = false;
    await loadMoreData(controller, context, countPage);
    return true;
  }


  @override
  Widget build(BuildContext context) {
    int countPage = 1;
    NotificationController controller = Get.find();

    /// Load local data which saved in SharedPreferences
    controller.loadDataFromLocal();

    controller.fetchedData.value =
        NotificationModelNew(listNew: [], listOld: []);

    loadMoreData(controller, context, countPage);

    /// Add Listener to catch event user scrolls to the end of the list.
    controller.scrollController.value.addListener(() {
      if (controller.scrollController.value.position.pixels ==
          controller.scrollController.value.position.maxScrollExtent) {
        _loadMore(controller, context, countPage += 1);
      } else {}
    });

    return Column(
      children: [
        /// Build AppBar
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 3.0),
          child: AppBar(
            centerTitle: true,
            title: const Text(
              'Thông báo',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
            ),
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: GestureDetector(
                onTap: () => Get.back(),
                child: const Icon(Icons.arrow_back_ios_new)),
          ),
        ),

        /// Build list of notifications
        Expanded(
          child: Container(
            width: Get.width,
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(
                      "assets/bg_reset_password/bg_reset_password.png"),
                  fit: BoxFit.cover),
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            ),
            child: Container(
              decoration: BoxDecoration(color: Colors.white.withOpacity(0.5)),
              child: SingleChildScrollView(
                controller: controller.scrollController.value,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Obx(() => (controller.listNewRx.isNotEmpty)
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: 25),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20),
                                  child: Text(
                                    "Mới",
                                    style: kTitleTextStyle.copyWith(
                                      color: kPrimaryColor,
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )),
                              const SizedBox(
                                height: 16,
                              ),
                              for (int i = 0;
                                  i < controller.listNewRx.length;
                                  i++)
                                buildNotification(
                                    data: controller.listNewRx[i],
                                    isNew: true,
                                    notiId:
                                        controller.listNewRx[i].notification.id,
                                    listId: i,
                                    controller: controller,
                                    context: context),
                              const Divider(),
                            ],
                          )
                        : Container()),
                    Obx(() => (controller.listOldRx.isNotEmpty)
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: 20),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20),
                                  child: Text(
                                    "Trước đó",
                                    style: kTitleTextStyle.copyWith(
                                      color: kPrimaryColor,
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )),
                              const SizedBox(
                                height: 16,
                              ),
                              for (int i = 0;
                                  i < controller.listOldRx.length;
                                  i++)
                                buildNotification(
                                    data: controller.listOldRx[i],
                                    isNew: false,
                                    notiId:
                                        controller.listOldRx[i].notification.id,
                                    listId: i,
                                    controller: controller,
                                    context: context),
                            ],
                          )
                        : Container()),
                    Obx(() => (!controller.isEnd.value)
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Lottie.asset("assets/lotties/lottie_loading.json",
                                  width: 150),
                            ],
                          )
                        : Container())
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  GestureDetector buildNotification(
      {required ListElement data,
      required bool isNew,
      required int notiId,
      required int listId,
      required NotificationController controller,
      required context}) {
    return GestureDetector(
      onTap: () async {
        if (!data.notification.isReaded) {
          controller.updateReadNotifications(
              context: context, notificationId: data.notification.id);

          if (isNew) {
            for (int i = 0; i < controller.listNewRx.length; i++) {
              if (controller.listNewRx[i].notification.id == notiId) {
                ListElement tempElement = controller.listNewRx[i];
                tempElement.notification.isReaded = true;
                controller.listNewRx[i] = tempElement;
              }
            }
          } else {
            for (int i = 0; i < controller.listOldRx.length; i++) {
              if (controller.listOldRx[i].notification.id == notiId) {
                ListElement tempElement = controller.listOldRx[i];
                tempElement.notification.isReaded = true;
                controller.listOldRx[i] = tempElement;
              }
            }
          }
        }

        PostResponse p = await controller.loadPostById(
            context, data.notification.postId, data.notification.userId);

        /// On Click
        ///
        /// Navigate to Detail Page with following arguments
        Get.toNamed(kDetailPage, arguments: {
          "postId": p.post.id,
          "userId": data.notification.userId,
          "token": controller.token,
          "userAvatar": controller.userAvatar,
          "userFullname": controller.userFullname
        });
      },
      child: Container(
        decoration: checkIsRead(isNew, controller, notiId),
        child: ListTile(
          leading: Stack(children: [
            CircleAvatar(
              backgroundColor: kPrimaryColor,
              radius: 30,
              child: CircleAvatar(
                radius: 26,
                backgroundImage: NetworkImage((data.userBAvatar != '')
                    ? HttpService.baseUrl + data.userBAvatar
                    : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqn6tPELJgjHUY9qp5OF9l_K0KAzrWeru6VA&usqp=CAU"),
              ),
            ),
            Positioned(
              right: 0,
              bottom: 0,
              child: Container(
                padding: const EdgeInsets.all(3),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black26,
                          spreadRadius: 1,
                          blurRadius: 10)
                    ]),
                child: Icon(
                  data.notification.type == 1
                      ? Icons.thumb_up
                      : Icons.chat_bubble,
                  size: 13,
                  color: kSecondaryColor,
                ),
              ),
            ),
          ]),
          title: Row(
            children: [
              Text(
                data.userBFullname,
                style:
                    const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Expanded(
                  child: Text(
                (data.notification.type == 1)
                    ? ' đã thích bài viết của bạn'
                    : ' đã bình luận bài viết của bạn',
                overflow: TextOverflow.ellipsis,
              )),
            ],
          ),
          subtitle: Text(
            "${data.countTime} ${getTypeTime(data.typeTime)}",
            style: const TextStyle(color: Colors.grey),
          ),
          trailing: SizedBox(
            width: 40,
            child: IconButton(
              onPressed: () {},
              icon: const Icon(Icons.more_vert),
            ),
          ),
        ),
      ),
    );
  }

  /// Parse type time to text
  ///
  /// Transmit [typeId]. Return text
  String getTypeTime(int typeId) {
    if (typeId == 1) {
      return 'phút trước';
    } else if (typeId == 2) {
      return 'giờ trước';
    } else {
      return 'ngày trước';
    }
  }

  /// Check is readed?
  ///
  /// Transmit [isNew], [controller], [notiId]
  /// If false, return widget hovered with blue color.
  BoxDecoration checkIsRead(isNew, NotificationController controller, notiId) {
    if (isNew) {
      for (int i = 0; i < controller.listNewRx.length; i++) {
        if (controller.listNewRx[i].notification.id == notiId) {
          if (controller.listNewRx[i].notification.isReaded == false) {
            return BoxDecoration(color: kPrimaryColor.withOpacity(0.1));
          } else {
            return const BoxDecoration();
          }
        }
      }
    } else {
      for (int i = 0; i < controller.listOldRx.length; i++) {
        if (controller.listOldRx[i].notification.id == notiId) {
          if (controller.listOldRx[i].notification.isReaded == false) {
            return BoxDecoration(color: kPrimaryColor.withOpacity(0.1));
          } else {
            return const BoxDecoration();
          }
        }
      }
    }
    return const BoxDecoration();
  }
}
