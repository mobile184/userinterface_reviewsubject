// ignore_for_file: must_be_immutable

import 'dart:math';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/models/top_user_model.dart';
import 'package:review_subject/pages/user_profile_page/user_profile_page.dart';
import 'package:review_subject/service/service.dart';

class GetTopReviewers extends StatelessWidget {
  RxList<TopUser> topUsers;
  GetTopReviewers({super.key, required this.topUsers});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        width: Get.width * 0.9,
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: const [
              BoxShadow(color: Colors.black12, blurRadius: 10, spreadRadius: 5)
            ],
            borderRadius: BorderRadius.circular(20)),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Row(
                children: List.generate(topUsers.length, (index) {
              String pathCrown = '';
              if (index == 0) {
                pathCrown = 'assets/icons/gold_crown.png';
              } else if (index == 1) {
                pathCrown = 'assets/icons/ic_silver_crown.png';
              }

              int displayedUserId = topUsers[index].id;
              return Padding(
                padding: const EdgeInsets.fromLTRB(5, 10, 15, 10),
                child: GestureDetector(
                  /// Catch event Click on User in leaderboard
                  ///
                  /// Navigate to [UserProfilePage()] with argument [displayedUserId]
                  onTap: () => Get.to(() => const UserProfilePage(),
                      arguments: displayedUserId),
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          SizedBox(
                            height: 65,
                            width: 50,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  height: 50,
                                  width: 50,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                          color: kPrimaryColor, width: 1.5),
                                      image: DecorationImage(
                                          image: NetworkImage(topUsers[index]
                                                      .avatar ==
                                                  ""
                                              ? kNoUserAvatar
                                              : "${HttpService.baseUrl}${topUsers[index].avatar}?v=${Random().nextInt(1000)}"),
                                          fit: BoxFit.cover)),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                              top: 0,
                              left: 0,
                              child: (index < 2)
                                  ? SizedBox(
                                      width: 30,
                                      height: 25,
                                      child: Transform.rotate(
                                          angle: -math.pi / 7.0,
                                          child: Image(
                                            image: AssetImage(pathCrown),
                                            fit: BoxFit.cover,
                                          )))
                                  : Container())
                        ],
                      ),
                      const SizedBox(height: 5),
                      Text(
                        '${topUsers[index].point} điểm',
                        style: const TextStyle(
                            fontSize: 12,
                            color: Color.fromARGB(255, 255, 208, 0),
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        topUsers[index].fullname,
                        style: const TextStyle(
                            fontSize: 11, color: Colors.black87),
                      )
                    ],
                  ),
                ),
              );
            })),
          ),
        ),
      ),
    );
  }
}
