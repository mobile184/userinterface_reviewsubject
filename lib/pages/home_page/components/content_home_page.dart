import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/home_controller.dart';

import 'package:review_subject/pages/home_page/components/get_just_for_you.dart';
import 'package:review_subject/pages/home_page/components/get_top_reviews.dart';
import 'package:review_subject/pages/home_page/components/post_by_faculty.dart';

class ContentHomePage extends StatelessWidget {
  const ContentHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    HomeController controller = Get.put(HomeController());
    return Obx(
      () => Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 25, 20, 10),
        child: Column(
          children: [
            Row(
              children: const [
                Padding(
                  padding: EdgeInsets.only(left: 5.0),
                  child: Text(
                    'Top Reviewers',
                    style: TextStyle(
                        color: kPrimaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 15),

            /// Get Top Users
            /// 
            /// Tranmiss list [topUsers]. Return widget of leaderboard 
            GetTopReviewers(topUsers: controller.topUsers),
            const SizedBox(height: 10),
            const Divider(),
            const SizedBox(height: 15),
            Row(
              children: const [
                Padding(
                  padding: EdgeInsets.only(left: 5.0),
                  child: Text(
                    'Dành cho bạn',
                    style: TextStyle(
                        color: kPrimaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 15),

            /// Get "Just for you" posts
            /// 
            /// Tranmiss list [listPostResponse] and HomeController [controller]
            /// Return widget of "Just for you" posts
            GetJustForYou(
                listPostResponse: controller.listPostResponse,
                controller: controller),
            const SizedBox(height: 10),

            /// Get Posts sorted by Faculty
            /// 
            /// Tranmiss [facultyName], list posts of this faculty and HomeController controller
            /// Return ListView of posts in this faculty
            for (int i = 0; i < controller.listFacultyResponse.length; i++)
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Divider(),
                  const SizedBox(height: 15),
                  PostByFaculty(
                    facultyName: controller.listFacultyResponse[i].name,
                    posts: controller.listFacultyResponse[i].posts,
                    controller: controller,
                  ),
                  const SizedBox(height: 10),
                ],
              ),
          ],
        ),
      ),
    );
  }
}
