// ignore_for_file: must_be_immutable, invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/home_controller.dart';
import 'package:review_subject/models/list_post_model.dart';
import 'package:review_subject/route_managements/routes.dart';
import 'package:review_subject/service/service.dart';

class GetJustForYou extends StatelessWidget {
  RxList<PostResponse> listPostResponse;
  HomeController controller;
  GetJustForYou(
      {super.key, required this.listPostResponse, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => listPostResponse == []
          ? const Text("Chưa có dữ liệu")
          : SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(children: [
                      for (int index = 0;
                          index < (listPostResponse.length / 2).ceil();
                          index++)

                        /// Catch event Click
                        ///
                        /// Whenever user clicks, navigate to DetailPage with following arguments
                        GestureDetector(
                          onTap: () => Get.toNamed(kDetailPage, arguments: {
                            "postId":
                                controller.listPostResponse[index].post.id,
                            "userId": controller.userId,
                            "token": controller.token,
                            'userFullname': controller.userFullname,
                            'userAvatar': controller.userAvatar,
                          }),
                          child: Container(
                            margin: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 5),
                            width: (Get.width - 60) / 2,
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    spreadRadius: 1,
                                    blurRadius: 10,
                                  ),
                                ]),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                /// Image
                                Container(
                                  height: (Get.width - 160) / 2,
                                  decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.only(
                                          topLeft: Radius.circular(12),
                                          topRight: Radius.circular(12)),
                                      image: DecorationImage(
                                          image: NetworkImage(
                                              listPostResponse[index]
                                                          .post
                                                          .images
                                                          .isNotEmpty &&
                                                      listPostResponse[index]
                                                              .post
                                                              .images[0] !=
                                                          ""
                                                  ? HttpService.baseUrl +
                                                      listPostResponse[index]
                                                          .post
                                                          .images[0]
                                                  : kNoSubjectImage),
                                          fit: BoxFit.cover)),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, right: 15, bottom: 10, top: 3),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      /// Subject name
                                      SizedBox(
                                        height: 35,
                                        child: Center(
                                          child: Text(
                                            listPostResponse
                                                .value[index].post.subject,
                                            textAlign: TextAlign.center,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: const TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(height: 3),

                                      /// Rating Bar
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          RatingBar.builder(
                                            itemSize: 20,
                                            initialRating:
                                                listPostResponse[index]
                                                        .post
                                                        .rateLike *
                                                    1.0,
                                            minRating: 1,
                                            ignoreGestures: true,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemPadding:
                                                const EdgeInsets.symmetric(
                                                    horizontal: 1.0),
                                            itemBuilder: (context, _) =>
                                                const Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            onRatingUpdate: (rating) {},
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 7),

                                      /// Name of University
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            'assets/icons/ic_college.png',
                                            height: 22,
                                            width: 22,
                                          ),
                                          const SizedBox(width: 7),
                                          Expanded(
                                            child: Text(
                                              listPostResponse[index]
                                                  .universityName,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(
                                                  color: Colors.black),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 5),

                                      /// Name of teacher
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            'assets/icons/ic_teacher.png',
                                            height: 22,
                                            width: 22,
                                          ),
                                          const SizedBox(width: 7),
                                          Expanded(
                                            child: SizedBox(
                                              height: 32,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    listPostResponse[index]
                                                        .post
                                                        .teacher,
                                                    maxLines: 2,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: const TextStyle(
                                                        color: Colors.black),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 5),

                                      /// Likes and comments
                                      Container(
                                        height: 36,
                                        decoration: BoxDecoration(
                                            // border: Border.all(),
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            color: kPrimaryColor
                                                .withOpacity(0.07)),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                Text(
                                                  listPostResponse.value[index]
                                                      .post.likeCount
                                                      .toString(),
                                                  style: const TextStyle(
                                                      color: kPrimaryColor,
                                                      fontSize: 16),
                                                ),
                                                const SizedBox(width: 5),
                                                const Icon(
                                                  Icons.thumb_up_outlined,
                                                  color: kPrimaryColor,
                                                )
                                              ],
                                            ),
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                Text(
                                                  listPostResponse.value[index]
                                                      .post.commentCount
                                                      .toString(),
                                                  style: const TextStyle(
                                                      color: kPrimaryColor,
                                                      fontSize: 16),
                                                ),
                                                const SizedBox(width: 5),
                                                const Icon(
                                                  Icons.comment_outlined,
                                                  color: kPrimaryColor,
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                    ]),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(children: [
                      for (int index = (listPostResponse.length / 2).ceil();
                          index < listPostResponse.length;
                          index++)
                        GestureDetector(
                          onTap: () => Get.toNamed(kDetailPage, arguments: {
                            "postId":
                                controller.listPostResponse[index].post.id,
                            "userId": controller.userId,
                            "token": controller.token,
                            'userFullname': controller.userFullname,
                            'userAvatar': controller.userAvatar,
                          }),
                          child: Container(
                            margin: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 5),
                            width: (Get.width - 60) / 2,
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    spreadRadius: 1,
                                    blurRadius: 10,
                                  ),
                                ]),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  height: (Get.width - 160) / 2,
                                  decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.only(
                                          topLeft: Radius.circular(12),
                                          topRight: Radius.circular(12)),
                                      image: DecorationImage(
                                          image: NetworkImage(
                                              listPostResponse[index]
                                                          .post
                                                          .images
                                                          .isNotEmpty &&
                                                      listPostResponse[index]
                                                              .post
                                                              .images[0] !=
                                                          ""
                                                  ? HttpService.baseUrl +
                                                      listPostResponse[index]
                                                          .post
                                                          .images[0]
                                                  : kNoSubjectImage),
                                          fit: BoxFit.cover)),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, right: 15, bottom: 10, top: 3),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 35,
                                        child: Center(
                                          child: Text(
                                            listPostResponse
                                                .value[index].post.subject,
                                            textAlign: TextAlign.center,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: const TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(height: 3),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          RatingBar.builder(
                                            itemSize: 20,
                                            initialRating: listPostResponse
                                                    .value[index]
                                                    .post
                                                    .rateLike *
                                                1.0,
                                            minRating: 1,
                                            ignoreGestures: true,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemPadding: const EdgeInsets.symmetric(
                                                horizontal: 1.0),
                                            itemBuilder: (context, _) =>
                                                const Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            onRatingUpdate: (rating) {
                                              // print(rating);
                                            },
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 7),
                                      Row(
                                        children: [
                                          Image.asset(
                                            'assets/icons/ic_college.png',
                                            height: 22,
                                            width: 22,
                                          ),
                                          const SizedBox(width: 7),
                                          Expanded(
                                            child: Text(
                                              listPostResponse
                                                  .value[index].universityName,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(
                                                  color: Colors.black),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 5),
                                      Row(
                                        children: [
                                          Image.asset(
                                            'assets/icons/ic_teacher.png',
                                            height: 22,
                                            width: 22,
                                          ),
                                          const SizedBox(width: 7),
                                          Expanded(
                                            child: SizedBox(
                                              height: 32,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    listPostResponse
                                                        .value[index]
                                                        .post
                                                        .teacher,
                                                    maxLines: 2,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: const TextStyle(
                                                        color: Colors.black),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 5),
                                      Container(
                                        height: 36,
                                        decoration: BoxDecoration(
                                            // border: Border.all(),
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            color: kPrimaryColor
                                                .withOpacity(0.07)),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                Text(
                                                  listPostResponse.value[index]
                                                      .post.likeCount
                                                      .toString(),
                                                  style: const TextStyle(
                                                      color: kPrimaryColor,
                                                      fontSize: 16),
                                                ),
                                                const SizedBox(width: 5),
                                                const Icon(
                                                  Icons.thumb_up_outlined,
                                                  color: kPrimaryColor,
                                                )
                                              ],
                                            ),
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                Text(
                                                  listPostResponse[index]
                                                      .post
                                                      .commentCount
                                                      .toString(),
                                                  style: const TextStyle(
                                                      color: kPrimaryColor,
                                                      fontSize: 16),
                                                ),
                                                const SizedBox(width: 5),
                                                const Icon(
                                                  Icons.comment_outlined,
                                                  color: kPrimaryColor,
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                    ]),
                  ]),
            ),
    );
  }
}
