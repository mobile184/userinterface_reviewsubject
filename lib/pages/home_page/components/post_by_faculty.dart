// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/home_controller.dart';
import 'package:review_subject/models/list_post_model.dart';
import 'package:review_subject/route_managements/routes.dart';

import '../../../service/service.dart';

class PostByFaculty extends StatelessWidget {
  HomeController controller;
  String facultyName;
  List<PostResponse> posts;
  PostByFaculty(
      {super.key,
      required this.facultyName,
      required this.posts,
      required this.controller});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      // mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 5.0),
          child: Text(
            'Khoa $facultyName',
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
                color: kPrimaryColor,
                fontWeight: FontWeight.bold,
                fontSize: 20),
          ),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 10, 10, 12),
            child: Row(
              children: List.generate(posts.length, (index) {
                PostResponse post = posts[index];
                RxInt likeCount = post.post.likeCount.obs;
                RxInt commentCount = post.post.commentCount.obs;
                return Obx(
                  () => GestureDetector(
                    onTap: () => Get.toNamed(kDetailPage, arguments: {
                      "postId": post.post.id,
                      "userId": controller.userId,
                      "token": controller.token,
                      'userFullname': controller.userFullname,
                      'userAvatar': controller.userAvatar,
                    }),
                    child: Container(
                        margin: const EdgeInsets.only(right: 20),
                        width: Get.width * 0.85,
                        height: 215,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: const [
                              BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 10,
                                  spreadRadius: 1)
                            ]),
                        child: Row(
                          children: [
                            Container(
                              width: Get.width * 0.37,
                              height: 215,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(7),
                                  image: DecorationImage(
                                      image: NetworkImage(
                                        post.post.images.isNotEmpty &&
                                                post.post.images[0] != ""
                                            ? HttpService.baseUrl +
                                                post.post.images[0]
                                            : kNoSubjectImage,
                                      ),
                                      fit: BoxFit.cover)),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(18, 10, 12, 10),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      post.post.subject,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Expanded(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [
                                          RatingBar.builder(
                                            itemSize: 20,
                                            initialRating:
                                                post.post.rateLike * 1.0,
                                            minRating: 1,
                                            ignoreGestures: true,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemPadding: const EdgeInsets.symmetric(
                                                horizontal: 1.0),
                                            itemBuilder: (context, _) => const Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            onRatingUpdate: (rating) {
                                            },
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              const Image(
                                                image: AssetImage(
                                                    'assets/icons/ic_college.png'),
                                                height: 24,
                                                width: 24,
                                              ),
                                              const SizedBox(width: 10),
                                              Expanded(
                                                  child: Text(
                                                post.universityName,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              ))
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              const Image(
                                                image: AssetImage(
                                                    'assets/icons/ic_teacher.png'),
                                                height: 24,
                                                width: 24,
                                              ),
                                              const SizedBox(width: 10),
                                              Expanded(
                                                  child: Text(
                                                post.post.teacher,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                              ))
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              const SizedBox(width: 3),
                                              Image.asset(
                                                'assets/icons/ic_faculty_history.png',
                                                height: 20,
                                                width: 20,
                                              ),
                                              const SizedBox(width: 10),
                                              Expanded(
                                                  child: Text(
                                                post.post.major,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                              ))
                                            ],
                                          ),
                                          // ignore: prefer_const_constructors
                                          Container(
                                            padding:
                                                const EdgeInsets.symmetric(vertical: 4),
                                            decoration: BoxDecoration(
                                                // border: Border.all(),
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                color: kPrimaryColor
                                                    .withOpacity(0.07)),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      likeCount.value.toString(),
                                                      style: const TextStyle(
                                                          color: kPrimaryColor,
                                                          fontSize: 16),
                                                    ),
                                                    const SizedBox(width: 5),
                                                    const Icon(
                                                      Icons.thumb_up_outlined,
                                                      color: kPrimaryColor,
                                                    )
                                                  ],
                                                ),
                                                Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      commentCount.value
                                                          .toString(),
                                                      style: const TextStyle(
                                                          color: kPrimaryColor,
                                                          fontSize: 16),
                                                    ),
                                                    const SizedBox(width: 5),
                                                    const Icon(
                                                      Icons.comment_outlined,
                                                      color: kPrimaryColor,
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        )),
                  ),
                );
              }),
            ),
          ),
        )
      ],
    );
  }
}
