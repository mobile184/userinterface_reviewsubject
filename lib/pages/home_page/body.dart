import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/input_blank.dart';
import 'package:review_subject/controllers/home_controller.dart';
import 'package:review_subject/pages/home_page/components/content_home_page.dart';
import 'package:review_subject/route_managements/routes.dart';
import 'package:review_subject/service/service.dart';

class Body extends StatefulWidget {
  const Body({super.key});

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  HomeController controller = Get.put(HomeController());
  final ScrollController _sc = ScrollController();
  @override
  void initState() {
    /// Load local data then get responsed data from server to display
    ///
    /// TopUser, RelatedPost and PostFromPage
    controller.loadDataFromLocal().then((_) => setState(() {
          controller.loadTopUser();
          controller.loadRelatedPost();
          controller.loadPostFromPage();
        }));

    /// Add Listener to catch event user scrolls to the end of list.
    _sc.addListener(() {
      if (_sc.position.pixels == _sc.position.maxScrollExtent) {
        setState(() {
          controller.loadPostFromPage();
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => controller.displayedUni.value.name == ""
          ? const Center(
              child: CircularProgressIndicator(
                color: Colors.white,
              ),
            )
          : Obx(
              /// Refresh page
              ///
              /// Whenever user scrolls to the top of page, call api again to refresh data
              () => RefreshIndicator(
                onRefresh: () => Future.delayed(
                    Duration.zero,
                    () =>
                        controller.loadDataFromLocal().then((_) => setState(() {
                              controller.ressetHomeData();
                            }))),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  controller: _sc,
                  child: Column(
                    children: [
                      const SizedBox(height: 17),
                      Row(
                        children: [
                          /// Avatar of chosen University
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 15.0, right: 12),
                            child: Obx(() => Container(
                                  width: 60,
                                  height: 60,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image: NetworkImage(controller
                                                      .displayedUni
                                                      .value
                                                      .avatar !=
                                                  ""
                                              ? HttpService.baseUrl +
                                                  controller
                                                      .displayedUni.value.avatar
                                              : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQnS7ofBCIjU62hzcRIwIEIljglY1qc4mo_rw&usqp=CAU")),
                                      border: Border.all(
                                          color:
                                              const Color.fromARGB(255, 20, 130, 160),
                                          width: 2)),
                                )),
                          ),

                          /// Info of the chosen University
                          ///
                          /// Display name and address of the University
                          /// Whenever user clicks, navigate to the Search School Page
                          Expanded(
                            child: GestureDetector(
                              onTap: () => Get.toNamed(kSearchSchoolPage),
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 7, horizontal: 10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.black.withOpacity(0.1)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5.0),
                                      child: Obx(() => Text(
                                            controller.displayedUni.value.name,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: const TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18),
                                          )),
                                    ),
                                    const SizedBox(height: 3),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        const Icon(
                                          Icons.location_on,
                                          color: Colors.red,
                                        ),
                                        Obx(() => Expanded(
                                              child: Text(
                                                controller.displayedUni.value
                                                        .address =
                                                    controller.displayedUni
                                                        .value.address,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ))
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),

                          /// Notification Icon
                          ///
                          /// Whenever user clicks on, navigate to the Notification page
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 12.0, right: 15),
                            child: GestureDetector(
                              onTap: () {
                                Get.toNamed(kNotificationPage);
                              },
                              child: const Icon(
                                Icons.notifications,
                                color: Colors.white,
                                size: 32,
                              ),
                            ),
                          )
                        ],
                      ),

                      /// Search Bar
                      ///
                      /// Whenever user clicks on, navigate to the Seach Subject page
                      GestureDetector(
                        onTap: () => Get.toNamed(kSearchSubjectPage),
                        child: AbsorbPointer(
                          absorbing: true,
                          child: InputBlank(
                              controller:
                                  controller.textSearchSubjectController.value,
                              prefixIcon: Icons.search,
                              suffixIcon: Icons.abc,
                              title: 'Tìm kiếm môn học',
                              content: ''),
                        ),
                      ),
                      const SizedBox(height: 20),
                      Container(
                        width: double.maxFinite,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                                topRight: Radius.circular(30)),
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image:
                                    AssetImage("assets/bg_home/bg_home.png"))),
                        child: const ContentHomePage(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
