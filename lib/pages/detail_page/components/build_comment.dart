// ignore_for_file: must_be_immutable, invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/detail_controller.dart';
import 'package:review_subject/service/service.dart';

class BuildComment extends StatelessWidget {
  DetailController controller;
  BuildComment({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Padding(
        padding: const EdgeInsets.only(left: 24, top: 10),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 24.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () => controller.onLikeButtonPress(),
                    child: Expanded(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 60, vertical: 12),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                            boxShadow: const [
                              BoxShadow(
                                  color: Colors.black12,
                                  spreadRadius: 1,
                                  blurRadius: 10)
                            ]),
                        child: Expanded(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                controller.postData.value!.post.likeCount
                                    .toString(),
                                style: const TextStyle(
                                    color: kSecondaryColor,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Icon(
                                controller.postData.value!.isLiked == true
                                    ? Icons.thumb_up
                                    : Icons.thumb_up_outlined,
                                size: 24,
                                color: kSecondaryColor,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      controller.commentFocusNode.value.requestFocus();
                    },
                    child: Expanded(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 60, vertical: 12),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                            boxShadow: const [
                              BoxShadow(
                                  color: Colors.black12,
                                  spreadRadius: 1,
                                  blurRadius: 10)
                            ]),
                        child: Expanded(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                controller.postData.value!.activeCommentCount
                                    .toString(),
                                style: const TextStyle(
                                    color: kSecondaryColor,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              const Icon(
                                Icons.comment,
                                size: 24,
                                color: kSecondaryColor,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 18,
            ),
            if (controller.postData.value!.activeCommentCount == 0)
              const Padding(
                  padding: EdgeInsets.symmetric(vertical: 16),
                  child: Text(
                    "Chưa có bình luận nào",
                    style: TextStyle(fontSize: 20),
                  ))
            else
              for (int i = 0; i < controller.listComment.value.length; i++)
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CircleAvatar(
                            radius: 22,
                            backgroundImage: NetworkImage(
                                controller.listComment.value[i].userAvatar != ""
                                    ? HttpService.baseUrl +
                                        controller
                                            .listComment.value[i].userAvatar
                                    : kNoUserAvatar),
                          ),
                          const SizedBox(width: 10),
                          Container(
                            width: Get.width * 0.74,
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 14),
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(12),
                                    bottomRight: Radius.circular(12),
                                    topRight: Radius.circular(12),
                                    topLeft: Radius.circular(2)),
                                boxShadow: [
                                  BoxShadow(
                                      blurRadius: 10,
                                      color: Colors.black12,
                                      spreadRadius: 1)
                                ]),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      controller
                                          .listComment.value[i].userFullname,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                    ),
                                    const SizedBox(width: 7),
                                    Text(
                                      controller.listComment.value[i]
                                                      .countTime ==
                                                  0 &&
                                              controller.listComment.value[i]
                                                      .typeTime ==
                                                  1
                                          ? "Vừa xong"
                                          : "${controller.listComment.value[i].countTime} ${controller.intToStringTimeDiffMap[controller.listComment.value[i].typeTime]}",
                                      style: const TextStyle(
                                          color: Colors.black54, fontSize: 11),
                                    ),
                                    const Spacer(),
                                    if (controller.listComment.value[i].comment
                                            .userId ==
                                        controller.userId)
                                      IconButton(
                                        constraints: const BoxConstraints(),
                                        padding: const EdgeInsets.all(0),
                                          onPressed: () {
                                            controller.showDialogDeleteComment(
                                                context: context,
                                                controller: controller,
                                                id: i);
                                          },
                                          icon:
                                              const Icon(Icons.delete_outline, size: 20, color: Colors.red,)),
                                  ],
                                ),
                                const SizedBox(height: 5),
                                Text(controller.listComment[i].comment.content!)
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
            if (controller.postData.value!.activeCommentCount >
                controller.listComment.length)
              Row(children: [
                GestureDetector(
                    onTap: () {
                      controller.loadCommentByPage();
                    },
                    child: const Padding(
                        padding: EdgeInsets.symmetric(vertical: 16),
                        child: Text(
                          "Xem thêm bình luận",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ))),
              ]),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
