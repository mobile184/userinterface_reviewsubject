// ignore_for_file: must_be_immutable

import 'package:carousel_indicator/carousel_indicator.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/detail_controller.dart';
import 'package:review_subject/pages/detail_page/components/build_curriculum.dart';
import 'package:review_subject/pages/detail_page/components/full_screen_images.dart';
import 'package:review_subject/service/service.dart';

class BuildSubjectOverview extends StatelessWidget {
  DetailController controller;
  BuildSubjectOverview({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 24),
              child: Text(
                controller.postData.value!.post.subject,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor,
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () => Get.to(() => FullScreenImages(
                  images: controller.postData.value!.post.images,
                )),
            child: SizedBox(
              width: Get.width,
              child: controller.postData.value!.post.images.isEmpty ||
                      controller.postData.value!.post.images[0] == ""
                  ? AspectRatio(
                      aspectRatio: 1.5,
                      child: Image.network(
                        kNoSubjectImage,
                        fit: BoxFit.cover,
                      ))
                  : Stack(children: [
                      CarouselSlider(
                        options: CarouselOptions(
                          autoPlay: true,
                          onPageChanged: (index, reason) {
                            controller.changeSlider(index);
                          },
                          aspectRatio: 1.5,
                          viewportFraction: 1,
                        ),
                        items:
                            controller.postData.value!.post.images.map((data) {
                          return Builder(
                            builder: (BuildContext context) {
                              return Container(
                                  width: Get.width - 20,
                                  decoration:
                                      const BoxDecoration(color: Colors.amber),
                                  child: Image.network(
                                    HttpService.baseUrl + data,
                                    fit: BoxFit.cover,
                                  ));
                            },
                          );
                        }).toList(),
                      ),
                      Positioned(
                        bottom: 10,
                        left: (Get.width -
                                (controller.postData.value!.post.images.length -
                                        1) *
                                    5 -
                                controller.postData.value!.post.images.length *
                                    10) /
                            2,
                        child: CarouselIndicator(
                          width: 10,
                          height: 10,
                          index: controller.curSlider.value,
                          count: controller.postData.value!.post.images.length,
                          color: Colors.grey,
                          activeColor: Colors.white,
                        ),
                      ),
                    ]),
            ),
          ),
          const SizedBox(
            height: 36,
          ),
          const Padding(
            padding: EdgeInsets.only(left: 15.0),
            child: Text(
              'Thông tin môn học',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                  color: kPrimaryColor),
            ),
          ),
          const Padding(
              padding: EdgeInsets.symmetric(horizontal: 24), child: Divider()),
          Container(
            width: Get.width,
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 7),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Trường",
                    style: TextStyle(
                        color: kSecondaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  const SizedBox(
                    height: 7,
                  ),
                  Row(
                    children: [
                      const SizedBox(width: 10),
                      Image.asset(
                        'assets/icons/ic_college.png',
                        height: 22,
                        width: 22,
                      ),
                      const SizedBox(width: 12),
                      Expanded(
                        child: Text(
                          controller.postData.value!.universityName,
                          style: const TextStyle(fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  const Text(
                    "Khoa",
                    style: TextStyle(
                        color: kSecondaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  const SizedBox(
                    height: 7,
                  ),
                  Row(
                    children: [
                      const SizedBox(width: 10),
                      Image.asset(
                        'assets/icons/diploma.png',
                        height: 22,
                        width: 22,
                      ),
                      const SizedBox(width: 12),
                      Expanded(
                        child: Text(
                          controller.postData.value!.facultyName,
                          style: const TextStyle(fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  const Text(
                    "Ngành",
                    style: TextStyle(
                        color: kSecondaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  const SizedBox(
                    height: 7,
                  ),
                  Row(
                    children: [
                      const SizedBox(width: 10),
                      Image.asset(
                        'assets/icons/ic_university.png',
                        height: 22,
                        width: 22,
                      ),
                      const SizedBox(width: 12),
                      Expanded(
                        child: Text(
                          controller.postData.value!.post.major == ""
                              ? "Chưa có dữ liệu"
                              : controller.postData.value!.post.major,
                          style: const TextStyle(fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  const Text(
                    "Giảng viên",
                    style: TextStyle(
                        color: kSecondaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  const SizedBox(
                    height: 7,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(width: 10),
                      Image.asset(
                        'assets/icons/ic_teacher.png',
                        height: 22,
                        width: 22,
                      ),
                      const SizedBox(
                        width: 12,
                      ),
                      Expanded(
                        child: Text(
                          controller.postData.value!.post.teacher,
                          style: const TextStyle(fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  BuildCurriculum(controller: controller)
                ]),
          )
        ],
      ),
    );
  }
}
