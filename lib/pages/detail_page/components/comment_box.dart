// ignore_for_file: must_be_immutable

import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/detail_controller.dart';
import 'package:review_subject/service/service.dart';

class CommentBox extends StatelessWidget {
  DetailController controller;
  CommentBox({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => WillPopScope(
        onWillPop: () {
          if (controller.emojiIsShowing.value) {
            controller.emojiIsShowing.value = false;
          } else {
            Get.back();
          }
          return Future.value(false);
        },
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 10, right: 5, top: 7, bottom: 7),
              decoration: const BoxDecoration(color: Colors.white, boxShadow: [
                BoxShadow(blurRadius: 10, spreadRadius: 1, color: Colors.black12)
              ]),
              child: Row(
                children: [
                  CircleAvatar(
                    backgroundColor: kPrimaryColor,
                    radius: 21,
                    child: CircleAvatar(
                      radius: 20,
                      backgroundImage: NetworkImage(controller.userAvatar != ""
                          ? HttpService.baseUrl + controller.userAvatar
                          : kNoUserAvatar),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: const [
                            BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 0.5,
                                blurRadius: 10)
                          ],
                          color: Colors.white),
                      child: Row(
                        children: [
                          Expanded(
                            child: TextField(
                              focusNode: controller.commentFocusNode.value,
                              controller: controller.commentTextController.value,
                              decoration: const InputDecoration(
                                  hintText: "Viết bình luận...",
                                  border: InputBorder.none),
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              controller.emojiIsShowing.value =
                                  !controller.emojiIsShowing.value;
                                  controller.commentFocusNode.value.unfocus();
                            },
                            icon: const Icon(Icons.emoji_emotions, color: kPrimaryColor,),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: controller.isPostingComment.value
                              ? kPrimaryColor.withOpacity(0.5)
                              : kPrimaryColor,
                          padding: const EdgeInsets.symmetric(vertical: 16),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30))),
                      onPressed: () {
                        controller.onPostCommentButtonPress();
                        controller.commentFocusNode.value.unfocus();
                        controller.emojiIsShowing.value = false;
                      },
                      child: controller.isPostingComment.value
                          ? const SizedBox(
                              width: 20, height: 20, child: CircularProgressIndicator())
                          : const Icon(
                              Icons.send,
                              size: 22,
                              color: Colors.white,
                            ))
                ],
              ),
            ),
            Offstage(
                  offstage: !controller.emojiIsShowing.value,
                  child: SizedBox(
                      height: 250,
                      child: EmojiPicker(
                        onEmojiSelected: (category, emoji) {
                          controller.commentTextController.value.text += emoji.emoji;
                        },
                        config: Config(
                          columns: 7,
                          emojiSizeMax: 32,
                          verticalSpacing: 0,
                          horizontalSpacing: 0,
                          gridPadding: EdgeInsets.zero,
                          initCategory: Category.RECENT,
                          bgColor: const Color(0xFFF2F2F2),
                          indicatorColor: Colors.blue,
                          iconColor: Colors.grey,
                          iconColorSelected: Colors.blue,
                          backspaceColor: Colors.blue,
                          skinToneDialogBgColor: Colors.white,
                          skinToneIndicatorColor: Colors.grey,
                          enableSkinTones: true,
                          showRecentsTab: true,
                          recentsLimit: 28,
                          replaceEmojiOnLimitExceed: false,
                          noRecents: const Text(
                            'No Recents',
                            style: TextStyle(fontSize: 20, color: Colors.black26),
                            textAlign: TextAlign.center,
                          ),
                          loadingIndicator: const SizedBox.shrink(),
                          tabIndicatorAnimDuration: kTabScrollDuration,
                          categoryIcons: const CategoryIcons(),
                          buttonMode: ButtonMode.MATERIAL,
                          checkPlatformCompatibility: true,
                        ),
                      )),
                )
          ],
        ),
      ),
    );
  }
}
