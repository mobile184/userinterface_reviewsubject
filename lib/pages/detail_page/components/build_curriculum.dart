// ignore_for_file: must_be_immutable, invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/detail_controller.dart';

class BuildCurriculum extends StatelessWidget {
  DetailController controller;
  BuildCurriculum({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Text(
              "Giáo trình (${controller.listCurriculum.length})",
              style: kTitleTextStyle.copyWith(
                  color: kSecondaryColor,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        controller.listCurriculum.value.isEmpty
            ? const Text(
                "Chưa có tài liệu",
                style: TextStyle(fontSize: 16),
              )
            : Column(
                children: [
                  for (int i = 0;
                      i < controller.listCurriculum.value.length;
                      i++)
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10, left: 12),
                      child: Row(
                        children: [
                          GestureDetector(
                              onTap: () {
                                Clipboard.setData(ClipboardData(
                                    text: controller.listCurriculum.value[i]));
                              },
                              child: const Icon(
                                Icons.content_copy,
                                size: 20,
                              )),
                          const SizedBox(
                            width: 12,
                          ),
                          Expanded(
                            child: Text(
                              controller.listCurriculum[i],
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                    ),
                ],
              ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
