// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:review_subject/service/service.dart';

import '../../../components/constants.dart';

class FullScreenImages extends StatelessWidget {
  List<String> images;
  FullScreenImages({super.key, this.images = const []});

  @override
  Widget build(BuildContext context) {
    return PhotoViewGallery.builder(
      scrollPhysics: const BouncingScrollPhysics(),
      builder: (BuildContext context, int index) {
    return images[index] != ""
        ? PhotoViewGalleryPageOptions(
          minScale: PhotoViewComputedScale.covered * 0.3,
          maxScale: PhotoViewComputedScale.covered * 2,
            imageProvider:
                NetworkImage(HttpService.baseUrl + images[index]),
            initialScale: PhotoViewComputedScale.contained,
          )
        : PhotoViewGalleryPageOptions(
          minScale: PhotoViewComputedScale.covered * 0.3,
          maxScale: PhotoViewComputedScale.covered * 2,
            imageProvider: const NetworkImage(
                kNoSubjectImage),
            initialScale: PhotoViewComputedScale.contained,
          );
      },
      itemCount: images.length,
      loadingBuilder: (context, event) => const Center(
    child: SizedBox(
      width: 20.0,
      height: 20.0,
      child: CircularProgressIndicator(),
    ),
      ),
    );
  }
}
