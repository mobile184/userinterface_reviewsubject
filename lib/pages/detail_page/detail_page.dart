import 'package:flutter/material.dart';
import 'package:review_subject/components/constants.dart';

import 'body.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
        child: Scaffold(backgroundColor: kPrimaryColor, body: Body()));
  }
}
