// ignore_for_file: deprecated_member_use

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/detail_controller.dart';
import 'package:review_subject/controllers/history_posts_controller.dart';
import 'package:review_subject/controllers/home_controller.dart';
import 'package:review_subject/controllers/search_subject_controller.dart';
import 'package:review_subject/pages/detail_page/components/build_comment.dart';
import 'package:review_subject/pages/detail_page/components/build_rate_subject.dart';
import 'package:review_subject/pages/detail_page/components/build_subject_overview.dart';
import 'package:review_subject/pages/detail_page/components/comment_box.dart';
import 'package:review_subject/pages/post_page/post_page.dart';
import 'package:review_subject/service/service.dart';

class Body extends StatefulWidget {
  const Body({super.key});

  @override
  State<Body> createState() => _BodyState();
}

enum Menu { itemOne, itemTwo }

class _BodyState extends State<Body> {
  DetailController controller = Get.put(DetailController());
  HomeController homeController = Get.put(HomeController());
  SearchSubjectController searchSubjectController =
      Get.put(SearchSubjectController());
  HistoryPostController historyPostController =
      Get.put(HistoryPostController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => controller.postData.value == null
          ? Container(
              color: Colors.white,
              child: const Center(
                  child: CircularProgressIndicator(
                color: kPrimaryColor,
              )))
          : Obx(
              () => RefreshIndicator(
                onRefresh: () => Future.delayed(
                    Duration.zero, () => controller.loadPostData()),
                child: GestureDetector(
                  onTap: () {
                    controller.commentFocusNode.value.unfocus();
                  },
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: AppBar(
                            elevation: 0.0,
                            backgroundColor: kPrimaryColor,
                            leading: Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 16,
                                ),
                                child: IconButton(
                                  onPressed: () {
                                    searchSubjectController
                                        .resetSearchSubjectData();
                                    homeController.ressetHomeData();
                                    historyPostController.ressetHistoryData();
                                    Get.back();
                                  },
                                  icon: const Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                    size: 30,
                                  ),
                                )),
                            centerTitle: true,
                            title: const Text(
                              "Chi tiết",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold),
                            ),
                            actions: <Widget>[
                              if (controller.userId ==
                                  controller.postData.value!.post.userId)
                                PopupMenuButton<Menu>(
                                    // Callback that sets the selected popup menu item.
                                    onSelected: (Menu item) {
                                      if (item.index == 0) {
                                        Get.to(() => PostPage(isNewPost: false),
                                            arguments: {
                                              "data": controller.postData.value!
                                            });
                                      } else {
                                        showDialogDelete(context, controller);
                                      }
                                    },
                                    itemBuilder: (BuildContext context) =>
                                        <PopupMenuEntry<Menu>>[
                                          const PopupMenuItem<Menu>(
                                            value: Menu.itemOne,
                                            child: Text('Chỉnh sửa'),
                                          ),
                                          const PopupMenuItem<Menu>(
                                            value: Menu.itemTwo,
                                            child: Text('Xóa'),
                                          ),
                                        ]),
                            ]),
                      ),
                      Expanded(
                        child: Container(
                          width: Get.width,
                          padding: const EdgeInsets.only(top: 10),
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                            ),
                          ),
                          child: SingleChildScrollView(
                              child: Column(
                            children: [
                              ListTile(
                                leading: CircleAvatar(
                                  backgroundImage: NetworkImage(
                                      controller.postData.value!.userAvatar ==
                                              ""
                                          ? kNoUserAvatar
                                          : HttpService.baseUrl +
                                              controller
                                                  .postData.value!.userAvatar),
                                  radius: 25,
                                ),
                                title: Text(
                                  controller.postData.value!.userFullname,
                                  style: const TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(controller
                                                .postData.value!.countTime ==
                                            0 &&
                                        controller.postData.value!.typeTime == 1
                                    ? "Vừa xong"
                                    : "${controller.postData.value!.countTime} ${controller.intToStringTimeDiffMap[controller.postData.value!.typeTime]}"),
                              ),
                              const Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 24),
                                  child: Divider()),
                              BuildSubjectOverview(controller: controller),
                              Column(
                                children: [
                                  const SizedBox(height: 12),
                                  BuildRateSubject(controller: controller),
                                  const SizedBox(height: 10),
                                  const Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 24),
                                      child: Divider()),
                                  BuildComment(controller: controller),
                                  const SizedBox(height: 16),
                                ],
                              ),
                            ],
                          )),
                        ),
                      ),
                      CommentBox(controller: controller)
                    ],
                  ),
                ),
              ),
            ),
    );
  }

  void showDialogDelete(context, DetailController controller) {
    AwesomeDialog(
      context: context,
      animType: AnimType.SCALE,
      dialogType: DialogType.error,
      btnOkColor: Colors.red,
      // btnOkColor: Colors.green,
      btnCancelColor: Colors.grey,
      body: Center(
        child: Column(
          children: const [
            SizedBox(height: 10),
            Text(
              'Xóa bài viết?',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
            ),
            SizedBox(height: 7),
            Text('Bạn có chắc chắn muốn xóa')
          ],
        ),
      ),
      title: 'This is Ignored',
      desc: 'This is also Ignored',
      btnOkOnPress: () {
        controller.deletePost();

        searchSubjectController.resetSearchSubjectData();
        homeController.ressetHomeData();
        historyPostController.ressetHistoryData();

        Get.back();
      },
      btnCancelOnPress: () {},
    ).show();
  }
}
