import 'package:flutter/material.dart';
import 'package:review_subject/components/constants.dart';

import 'components/body.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const SafeArea(child: Scaffold(
      backgroundColor: kPrimaryColor,
      body: Body(),
    ));
  }
}