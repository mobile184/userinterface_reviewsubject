// ignore_for_file: unrelated_type_equality_checks

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/controllers/my_shared_preference.dart';
import 'package:review_subject/controllers/change_password_controller.dart';
import 'package:review_subject/pages/change_pasword_page/change_password_page.dart';
import 'package:review_subject/route_managements/routes.dart';

import '../../../components/constants.dart';

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 5.0),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: kPrimaryColor,
            centerTitle: true,
            title: const Text(
              'Cài đặt',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 26,
                  fontWeight: FontWeight.bold),
            ),
            automaticallyImplyLeading: false,
          ),
        ),
        Expanded(
          child: Container(
            width: Get.width,
            padding: const EdgeInsets.symmetric(vertical: 32),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            ),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text(
                    "Quản lý tài khoản",
                    style: kTitleTextStyle.copyWith(
                        color: kTextSecondaryColor, fontSize: 24),
                  )),
              const Divider(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                child: GestureDetector(
                  onTap: () {
                    Get.to(() => const ChangePasswordPage());
                  },
                  child: Row(
                    children: [
                      const Icon(
                        Icons.key,
                        color: Colors.black,
                        size: 35,
                      ),
                      const SizedBox(
                        width: 16,
                      ),
                      const Text("Mật khẩu",
                          style: TextStyle(
                              color: kTextSecondaryColor,
                              fontSize: 22,
                              fontWeight: FontWeight.w500)),
                      const Spacer(),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 25,
                        color: Colors.grey.shade400,
                      )
                    ],
                  ),
                ),
              ),
              const Divider(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                child: GestureDetector(
                  onTap: () async {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('Chính sách và điều khoản'),
                        content: const Text(
                            'Các Điều khoản dịch vụ này phản ánh cách thức kinh doanh của Google, những điều luật mà công ty chúng tôi phải tuân theo và một số điều mà chúng tôi vẫn luôn tin là đúng. Do đó, các Điều khoản dịch vụ này giúp xác định mối quan hệ giữa Google với bạn khi bạn tương tác với các dịch vụ của chúng tôi. Ví dụ: Các điều khoản này trình bày các chủ đề sau'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context, 'Đóng');
                            },
                            child: const Text('Đóng'),
                          ),
                        ],
                      ),
                    );
                  },
                  child: Row(
                    children: [
                      const Icon(
                        Icons.policy_outlined,
                        color: Colors.black,
                        size: 35,
                      ),
                      const SizedBox(
                        width: 16,
                      ),
                      const Text("Chính sách và điều khoản",
                          style: TextStyle(
                              color: kTextSecondaryColor,
                              fontSize: 22,
                              fontWeight: FontWeight.w500)),
                      const Spacer(),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 25,
                        color: Colors.grey.shade400,
                      )
                    ],
                  ),
                ),
              ),
              const Divider(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                child: GestureDetector(
                  onTap: () {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('Nhà phát triển'),
                        content: const Text('Developed by TeamMobi'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context, 'Đóng');
                            },
                            child: const Text('Đóng'),
                          ),
                        ],
                      ),
                    );
                  },
                  child: Row(
                    children: [
                      const Icon(
                        Icons.info_outline,
                        color: Colors.black,
                        size: 35,
                      ),
                      const SizedBox(
                        width: 16,
                      ),
                      const Text("Nhóm phát triển",
                          style: TextStyle(
                              color: kTextSecondaryColor,
                              fontSize: 22,
                              fontWeight: FontWeight.w500)),
                      const Spacer(),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 25,
                        color: Colors.grey.shade400,
                      )
                    ],
                  ),
                ),
              ),
              const Divider(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                child: GestureDetector(
                  onTap: () {
                    MySharedPreference.clearData();
                    Get.offAllNamed(kSignInPage);
                  },
                  child: Row(
                    children: [
                      const Icon(
                        Icons.logout_outlined,
                        color: Colors.black,
                        size: 35,
                      ),
                      const SizedBox(
                        width: 16,
                      ),
                      const Text("Đăng xuất",
                          style: TextStyle(
                              color: kTextSecondaryColor,
                              fontSize: 22,
                              fontWeight: FontWeight.w500)),
                      const Spacer(),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 25,
                        color: Colors.grey.shade400,
                      )
                    ],
                  ),
                ),
              ),
              const Divider(),
            ]),
          ),
        ),
      ],
    );
  }

  Future openChangePasswordDialog(
          context, ChangePasswordController controller) =>
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Thay đổi mật khẩu'),
          content: SizedBox(
            height: Get.height * 0.18,
            child: Column(
              children: [
                TextField(
                  controller: controller.userNameTextController.value,
                  autofocus: true,
                  decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.person), hintText: 'Tài khoản'),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 24, top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        controller.isValidUserName.value == true
                            ? ""
                            : "Tài khoản không hợp lệ",
                        style: const TextStyle(color: Colors.red, fontSize: 14),
                      )
                    ],
                  ),
                ),
                TextField(
                  controller: controller.passwordTextController.value,
                  autofocus: true,
                  decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.lock), hintText: 'Mật khẩu'),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 24, top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        controller.isValidPassword.value == true
                            ? ""
                            : "Mật khẩu không hợp lệ",
                        style: const TextStyle(color: Colors.red, fontSize: 14),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                controller.userNameTextController.value.clear();
                controller.passwordTextController.value.clear();
                Navigator.pop(context, 'Hủy');
              },
              child: const Text('Hủy'),
            ),
            TextButton(
              onPressed: () {
                if (controller.userNameTextController.value == '') {
                  controller.isValidUserName.value = false;
                }
                if (controller.userNameTextController.value == '') {
                  controller.isValidPassword.value = false;
                }

                if (controller.userNameTextController.value != '' &&
                    controller.userNameTextController.value != '') {
                  controller.onOkButtonClick(
                      context: context,
                      username: controller.userNameTextController.value.text,
                      password: controller.passwordTextController.value.text);
                  controller.userNameTextController.value.clear();
                  controller.passwordTextController.value.clear();
                }
                Navigator.pop(context, 'Đồng ý');
              },
              child: const Text('Đồng ý'),
            ),
          ],
        ),
      );
}
