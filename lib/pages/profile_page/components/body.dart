import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/profile_controller.dart';
import 'package:review_subject/pages/profile_page/components/info_user.dart';
import 'package:review_subject/route_managements/routes.dart';

class Body extends StatefulWidget {
  const Body({super.key});

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  ProfileController controller = Get.put(ProfileController());

  @override
  void initState() {
    controller.loadDataFromLocal().then((_) {
    controller.loadProfileById(profileId: controller.userId);
    });
    
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
        return Stack(
      children: [
        Container(
          width: double.maxFinite,
          height: Get.height,
          alignment: Alignment.bottomCenter,
          decoration: const BoxDecoration(color: kPrimaryColor),
          child: Container(
            width: double.maxFinite,
            height: Get.height * 0.78,
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(35),
                    topRight: Radius.circular(35))),
          ),
        ),
        Center(child: InfoUser(controller: controller,)),
        Positioned(
            top: 20,
            right: 20,
            child: GestureDetector(
              onTap: () {
                Get.toNamed(kModifyInfoPage, arguments: {'info': controller.profile.value});
              },
              child: const Icon(
                Icons.edit,
                color: Colors.white,
                size: 30,
              ),
            ))
      ],
    );
  }
}
