import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/components/input_blank.dart';
import 'package:review_subject/controllers/change_password_controller.dart';
import 'package:review_subject/route_managements/routes.dart';

class ResetPasswordPage extends StatelessWidget {
  const ResetPasswordPage({super.key});

  @override
  Widget build(BuildContext context) {
    ChangePasswordController controller = Get.find();

    TextEditingController usernameController = TextEditingController();
    // TextEditingController passwordController = TextEditingController();
    TextEditingController emailController = TextEditingController();


    return Scaffold(
        body: SingleChildScrollView(
      scrollDirection: Axis.vertical,
      physics: const BouncingScrollPhysics(),
      child: Stack(children: [
        Column(
          children: [
            SizedBox(
              height: Get.height,
              width: Get.width,
              child: const Image(
                image: AssetImage(
                    "assets/bg_reset_password/bg_reset_password.png"),
                fit: BoxFit.cover,
              ),
            ),
          ],
        ),
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: [
                        GestureDetector(
                            onTap: () {
                              Get.back();
                            },
                            child: const Icon(Icons.arrow_back_ios_new))
                      ],
                    ),
                  ),
                  SizedBox(height: Get.height * 0.16),
                  Container(
                    width: Get.width * 0.8,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.black12,
                            blurRadius: 10,
                            spreadRadius: 1,
                            // offset: Offset(0, 4)
                          )
                        ]),
                    child: Column(
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(top: 22.0),
                          child: Text('Lấy lại mật khẩu',
                              style: TextStyle(
                                  fontSize: 27, fontWeight: FontWeight.bold)),
                        ),
                        const Padding(
                          padding: EdgeInsets.fromLTRB(30.0, 10, 30, 10),
                          child: Text(
                            'Nhập tài khoản và mật khẩu mới để thay đổi mật khẩu',
                            style:
                                TextStyle(fontSize: 14, color: Colors.black54),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        InputBlank(
                            controller: usernameController,
                            prefixIcon: Icons.person,
                            suffixIcon: Icons.abc,
                            title: 'Tài khoản',
                            content: ''),
                        InputBlank(
                            controller: emailController,
                            prefixIcon: Icons.email,
                            suffixIcon: Icons.abc,
                            title: 'Email xác thực',
                            content: ''),

                        GestureDetector(
                          onTap: () {
                            if (controller.isPressedRecoverPassword.value ==
                                false) {
                              controller.isPressedRecoverPassword.value = true;
                            }
                            Get.offAndToNamed(kSignInPage);
                          },
                          child: Container(
                            margin: const EdgeInsets.only(top: 22, bottom: 32),
                            height: Get.height * 0.065,
                            width: Get.width * 0.7,
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: kPrimaryColor.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 10,
                                    offset: const Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                                color: kPrimaryColor,
                                borderRadius: BorderRadius.circular(30)),
                            child: const Center(
                              child: Text(
                                'Đồng ý',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ]),
    ));
  }
}
