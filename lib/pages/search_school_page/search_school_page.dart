import 'package:flutter/material.dart';

import '../../components/constants.dart';
import 'components/body.dart';

class SearchSchoolPage extends StatelessWidget {
  const SearchSchoolPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const SafeArea(child: Scaffold(
      backgroundColor: kPrimaryColor,
      body: Body(),));
  }
}