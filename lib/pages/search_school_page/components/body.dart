import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/components/show_dialog.dart';
import 'package:review_subject/controllers/home_controller.dart';
import 'package:review_subject/controllers/search_school_controller.dart';
import 'package:review_subject/route_managements/routes.dart';
import 'package:review_subject/service/service.dart';

import '../../../models/school_model.dart';

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    SearchSchoolController searchSchoolcontroller = Get.find();
    HomeController homeController = Get.find();
    // getDefaultList(context, controller);

    TextEditingController keywordController = TextEditingController();

    searchSchoolcontroller.getDefaultListUni(context: context);
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 10.0, bottom: 8),
          child: AppBar(
            elevation: 0.0,
            backgroundColor: kPrimaryColor,
            leading: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                ),
                child: IconButton(
                  onPressed: () => Get.back(),
                  icon: const Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                    size: 30,
                  ),
                )),
            centerTitle: true,
            title: Container(
              height: 50,
              width: Get.width * 0.65,
              decoration: BoxDecoration(
                  color: const Color.fromRGBO(241, 241, 241, 1),
                  borderRadius: BorderRadius.circular(10)),
              child: TextField(
                controller: keywordController,
                maxLines: 1,
                decoration: const InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 16, horizontal: 5),
                    prefixIcon: Icon(
                      Icons.search,
                      color: kSubTextColor,
                      size: 25,
                    ),
                    isDense: true,
                    hintText: 'Nhập tên trường',
                    hintStyle: kSubTextStyle,
                    border: InputBorder.none),
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: IconButton(
                    onPressed: () {
                      searchSchoolcontroller.onSearchButtonClick(
                          context: context, keyword: keywordController.text);
                    },
                    icon: const Icon(
                      Icons.search,
                      size: 32,
                    )),
              )
            ],
          ),
        ),
        Container(
          decoration: const BoxDecoration(color: Colors.white),
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              SizedBox(
                height: 16,
              ),
              Text(
                "Danh sách kết quả tìm kiếm",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10,
              ),
              Divider(
                thickness: 1.5,
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            height: double.maxFinite,
            decoration: const BoxDecoration(color: Colors.white),
            child: 
            Obx(
                () => searchSchoolcontroller.fetchedList.isNotEmpty
                    ? 
            SingleChildScrollView(
              child: Column(
                        children: [
                          ...searchSchoolcontroller.fetchedList.map(
                            (data) => buildSchoolSearch(
                                context, homeController, data),
                          )
                        ],
                      )
                    
            ) : Center(
                        child: Image(
                            image: const NetworkImage(
                                'https://cdn-icons-png.flaticon.com/512/2748/2748614.png'),
                            width: Get.width * 0.4,
                            height: Get.width * 0.4),
                      ),
              ),
          ),
        ),
      ],
    );
  }

  GestureDetector buildSchoolSearch(context, homeController, SchoolModel data) {
    return GestureDetector(
      onTap: () {
        showLoaderDialog(context);
        homeController.displayedUni.value = data;
        // print(homeController.displayedUni.value);
        Get.back();
        Get.toNamed(kHomePage);
      },
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
            child: Row(children: [
              SizedBox(
                width: 50,
                height: 50,
                child: AspectRatio(
                  aspectRatio: 1,
                  child: Image.network(
                    HttpService.baseUrl + data.avatar,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
              const SizedBox(
                width: 16,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      data.name,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      data.address,
                      style: const TextStyle(
                          color: Color.fromRGBO(236, 155, 34, 1)),
                    ),
                  ],
                ),
              )
            ]),
          ),
          const Divider(),
        ],
      ),
    );
  }
}
