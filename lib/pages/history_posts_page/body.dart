// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/history_posts_controller.dart';
import 'package:review_subject/pages/history_posts_page/components/history_post.dart';

class Body extends StatelessWidget {
  int userId;
  Body({super.key, required this.userId});

  /// Load more data whenever user scroll over
  ///
  /// Tranmiss [controller], BuildContext [context] and [countPage] (number of page)
  Future<void> loadMoreData(
      HistoryPostController controller, context, countPage) async {
    controller.getAllHistoryPost(displayedUserId: userId, pageNum: countPage);
  }

  Future<bool> _loadMore(
      HistoryPostController controller, context, countPage) async {
    await loadMoreData(controller, context, countPage);
    return true;
  }


  @override
  Widget build(BuildContext context) {
    HistoryPostController controller = Get.put(HistoryPostController());

    /// Load local data which saved in SharedPreferences
    controller.loadDataFromLocal();
    controller.clientUserId.value = userId;

    controller.pageNum.value = 1;
    loadMoreData(controller, context, controller.pageNum.value);

    /// Add Listener to catch event user scrolls to the end of the list.
    controller.scrollController.value.addListener(() {
      if (controller.scrollController.value.position.pixels ==
          controller.scrollController.value.position.maxScrollExtent) {
        _loadMore(controller, context, controller.pageNum.value += 1);
      }
    });

    return Column(
      children: [
        /// Build AppBar
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 4.0),
          child: AppBar(
            elevation: 0,
            backgroundColor: kPrimaryColor,
            leading: GestureDetector(
                onTap: () {
                  controller.fetchedList.value = [];
                  controller.pageNum.value = 0;

                  controller.clientUserId.value = 0;
                  controller.isEnd.value = false;
                  Get.back();
                },
                child: const Icon(Icons.arrow_back_ios)),
            centerTitle: true,
            title: const Text('Lịch sử bài viết'),
          ),
        ),

        /// Build list of history posts
        Expanded(
          child: Container(
            width: double.maxFinite,
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30)),
                color: Colors.white),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: SingleChildScrollView(
                controller: controller.scrollController.value,
                child: Obx(() => Column(
                      children: [
                        const SizedBox(height: 20),
                        Wrap(
                            direction: Axis.horizontal,
                            spacing: 20,
                            runSpacing: 20,
                            children: List.generate(
                                controller.fetchedList.length, (index) {
                              return HistoryPost(
                                  displayedPost: controller.fetchedList[index]);
                            })),
                        const SizedBox(height: 20),
                        (!controller.isEnd.value)
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Lottie.asset(
                                      "assets/lotties/lottie_loading.json",
                                      width: 150),
                                ],
                              )
                            : Container()
                      ],
                    )),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
