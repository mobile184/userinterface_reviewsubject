import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/pages/history_posts_page/body.dart';

class HistoryPostsPage extends StatelessWidget {
  const HistoryPostsPage({super.key});

  @override
  Widget build(BuildContext context) {
    int userId = Get.arguments;
    return Scaffold(
        backgroundColor: kPrimaryColor,
        body: Body(userId: userId));
  }
}
