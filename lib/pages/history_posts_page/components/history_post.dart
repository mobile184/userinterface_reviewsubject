// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/history_posts_controller.dart';
import 'package:review_subject/models/list_post_model.dart';
import 'package:review_subject/route_managements/routes.dart';
import 'package:review_subject/service/service.dart';

class HistoryPost extends StatelessWidget {
  PostResponse displayedPost;

  HistoryPost({super.key, required this.displayedPost});

  @override
  Widget build(BuildContext context) {
    List listImages = displayedPost.post.images;
    HistoryPostController controller = Get.find();

    /// Load local data which saved in SharedPreferences
    controller.loadDataFromLocal();

    return GestureDetector(
      onTap: () {
        Get.toNamed(kDetailPage, arguments: {
          "userId": controller.userId,
          "token": controller.token,
          "postId": displayedPost.post.id,
          'userFullname': controller.userFullname,
          'userAvatar': controller.userAvatar,
        });
      },
      child: Container(
          width: Get.width * 0.9,
          height: 235,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: const [
                 BoxShadow(
                    color: Colors.black12, blurRadius: 10, spreadRadius: 1)
              ]),
          child: Row(
            children: [

              /// Image
              Container(
                width: Get.width * 0.4,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    image: DecorationImage(
                        image: NetworkImage(
                            listImages.isNotEmpty && listImages[0] != ""
                                ? HttpService.baseUrl + listImages[0]
                                : kNoSubjectImage),
                        fit: BoxFit.cover)),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(22, 12, 12, 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      /// Name of Subject
                      Text(
                        displayedPost.post.subject,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            /// Rate
                            RatingBar.builder(
                              itemSize: 20,
                              initialRating:
                                  displayedPost.post.rateLike.toDouble(),
                              minRating: 1,
                              ignoreGestures: true,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemPadding:
                                  const EdgeInsets.symmetric(horizontal: 1.0),
                              itemBuilder: (context, _) => const Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {
                                // print(rating);
                              },
                            ),
                            /// Name of University
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Row(
                                children: [
                                  Image.asset(
                                    'assets/icons/ic_college.png',
                                    height: 22,
                                    width: 22,
                                  ),
                                  const SizedBox(width: 7),
                                  Expanded(
                                    child: Text(
                                      displayedPost.universityName.toString(),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(color: Colors.black),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            /// Name of Faculty
                            Row(
                              children: [
                                Image.asset(
                                  'assets/icons/ic_faculty_history.png',
                                  height: 22,
                                  width: 22,
                                ),
                                const SizedBox(width: 7),
                                Expanded(
                                  child: Text(
                                    displayedPost.facultyName.toString(),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(color: Colors.black),
                                  ),
                                ),
                              ],
                            ),
                            /// Name of Teacher
                            Row(
                              children: [
                                Image.asset(
                                  'assets/icons/ic_teacher.png',
                                  height: 22,
                                  width: 22,
                                ),
                                const SizedBox(width: 7),
                                Expanded(
                                  child: Text(
                                    displayedPost.post.teacher,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(color: Colors.black),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 4),
                            /// Likes and Comments
                            Container(
                              padding: const EdgeInsets.symmetric(vertical: 4),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: kPrimaryColor.withOpacity(0.07)),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        displayedPost.post.likeCount.toString(),
                                        style: const TextStyle(
                                            color: kPrimaryColor, fontSize: 16),
                                      ),
                                      const SizedBox(width: 7),
                                      const Icon(
                                        Icons.thumb_up_outlined,
                                        color: kPrimaryColor,
                                      )
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        displayedPost.post.commentCount
                                            .toString(),
                                        style: const TextStyle(
                                            color: kPrimaryColor, fontSize: 16),
                                      ),
                                      const SizedBox(width: 7),
                                      Container(
                                        padding: const EdgeInsets.only(top: 2),
                                        child: const Icon(
                                          Icons.comment_outlined,
                                          color: kPrimaryColor,
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          )),
    );
  }
}
