// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/profile_user_controller.dart';
import 'package:review_subject/route_managements/routes.dart';
import 'package:review_subject/service/service.dart';

class InfoDisplayedUser extends StatelessWidget {
  ProfileUserController controller;
  InfoDisplayedUser({super.key, required this.controller});

  getAvatar() {
    return NetworkImage(controller.profile.value == null ||
            controller.profile.value!.user.avatar == ''
        ? "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqn6tPELJgjHUY9qp5OF9l_K0KAzrWeru6VA&usqp=CAU"
        : HttpService.baseUrl + controller.profile.value!.user.avatar);
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 40),
          Container(
            width: Get.height * 0.2,
            height: Get.height * 0.2,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 5.5),
                shape: BoxShape.circle,
                image:
                    DecorationImage(fit: BoxFit.cover, image: getAvatar())),
          ),
          const SizedBox(height: 15),
          Text(
            controller.profile.value == null
                ? ""
                : controller.profile.value!.user.fullname,
            style: const TextStyle(
                color: Colors.black, fontSize: 25, fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 30),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
            width: Get.width * 0.9,
            height: 65,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                boxShadow: const [
                  BoxShadow(
                      color: Colors.black12, spreadRadius: 4, blurRadius: 10)
                ]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Text(
                      controller.profile.value == null
                          ? ""
                          : controller.profile.value!.countPost.toString(),
                      style: const TextStyle(
                          color: kPrimaryColor,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 3),
                    const Text(
                      'Bài viết',
                      style: TextStyle(
                          color: kPrimaryColor,
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                const VerticalDivider(
                  color: kPrimaryColor,
                ),
                Column(
                  children: [
                    Text(
                      controller.profile.value == null
                          ? ""
                          : controller.profile.value!.user.point.toString(),
                      style: const TextStyle(
                          color: kPrimaryColor,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 3),
                    const Text(
                      'Điểm',
                      style: TextStyle(
                          color: kPrimaryColor,
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(height: 25),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 30),
            width: Get.width * 0.9,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
                boxShadow: const [
                  BoxShadow(
                      color: Colors.black12, spreadRadius: 4, blurRadius: 10)
                ]),
            child: Column(
              children: [
                getInfoUser(
                    'assets/icons/ic_college_profile.png',
                    controller.profile.value == null
                        ? ""
                        : controller.profile.value!.universityName),
                const SizedBox(height: 12),
                getInfoUser('assets/icons/ic_faculty.png',
                    "Khoa ${controller.profile.value == null ? "" : controller.profile.value!.facultyName}"),
                const SizedBox(height: 12),
                getInfoUser('assets/icons/ic_major.png',
                    "Ngành ${controller.profile.value == null ? "" : controller.profile.value!.user.major}"),
                const SizedBox(height: 12),
                getInfoUser('assets/icons/ic_point.png',
                    'Điểm ${controller.profile.value == null ? "" : controller.profile.value!.user.point}')
              ],
            ),
          ),
          const SizedBox(height: 25),
          GestureDetector(
            onTap: () {
              Get.toNamed(kHistoryPostsPage, arguments: controller.profile.value?.user.id);
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
              width: Get.width * 0.9,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                        color: Colors.black12, spreadRadius: 4, blurRadius: 10)
                  ]),
              child: Row(
                children: [
                  Image.asset(
                    'assets/icons/ic_history.png',
                    height: 26,
                    width: 26,
                  ),
                  const SizedBox(width: 30),
                  const Expanded(
                    child: Text(
                      'Lịch sử đăng bài',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.black, fontSize: 16),
                    ),
                  ),
                  const Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black54,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getInfoUser(String icon, String content) {
    return Row(
      children: [
        Image.asset(
          icon,
          height: 26,
          width: 26,
        ),
        const SizedBox(width: 30),
        Expanded(
          child: Text(
            content,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(color: Colors.black, fontSize: 16),
          ),
        ),
      ],
    );
  }
}
