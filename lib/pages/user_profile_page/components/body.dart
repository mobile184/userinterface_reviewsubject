// ignore_for_file: must_be_immutable, no_logic_in_create_state

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/controllers/profile_user_controller.dart';
import 'package:review_subject/pages/user_profile_page/components/info_displayed_user.dart';

class Body extends StatefulWidget {
  int displayedUserId;

  Body({super.key, required this.displayedUserId});

  @override
  State<Body> createState() => _BodyState(displayedUserId: displayedUserId);
}

class _BodyState extends State<Body> {
  ProfileUserController controller = Get.put(ProfileUserController());
  int displayedUserId;

  _BodyState({required this.displayedUserId});

  @override
  void initState() {
    controller.loadDataFromLocal().then((_) {
      controller.loadProfileById(context: context ,profileId: displayedUserId);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: double.maxFinite,
          height: Get.height,
          alignment: Alignment.bottomCenter,
          decoration: const BoxDecoration(color: kPrimaryColor),
          child: Container(
            width: double.maxFinite,
            height: Get.height * 0.78,
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(35),
                    topRight: Radius.circular(35))),
          ),
        ),
        Center(
            child: InfoDisplayedUser(
          controller: controller,
        )),
        Positioned(
            top: 20,
            right: 20,
            child: GestureDetector(
              onTap: () {
                // Get.toNamed(kModifyInfoPage, arguments: {'info': controller.profile.value});
              },
              child: const Icon(
                Icons.more_horiz,
                color: Colors.white,
                size: 30,
              ),
            )),
        Positioned(
            top: 20,
            left: 20,
            child: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: const Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 30,
              ),
            ))
      ],
    );
  }
}
