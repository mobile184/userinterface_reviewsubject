import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../components/constants.dart';
import 'components/body.dart';

class UserProfilePage extends StatelessWidget {
  const UserProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    int displayedUserId = Get.arguments;
    return SafeArea(
        child: Scaffold(
            backgroundColor: kPrimaryColor,
            body: Body(displayedUserId: displayedUserId)));
  }
}
