import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/constants.dart';
import 'package:review_subject/pages/home_page/home_page.dart';
import 'package:review_subject/pages/post_page/post_page.dart';
import 'package:review_subject/pages/profile_page/profile_page.dart';
import 'package:review_subject/pages/search_subject_page/search_subject_page.dart';
import 'package:review_subject/pages/settings_page/settings_page.dart';

import 'controllers/network_controller.dart';

class ControllerPage extends StatefulWidget {
  final int index;

  const ControllerPage({Key? key, required this.index}) : super(key: key);

  @override
  // ignore: no_logic_in_create_state
  State<ControllerPage> createState() => _ControllerPageState(index);
}

class _ControllerPageState extends State<ControllerPage> {
  int indexPage;

  _ControllerPageState(this.indexPage);

  /// Show dialog when user click back button.
  Future<bool> _onWillPop() async {
    // ignore: prefer_typing_uninitialized_variables
    var shouldPop;
    await AwesomeDialog(
            context: context,
            dialogType: DialogType.warning,
            headerAnimationLoop: false,
            animType: AnimType.topSlide,
            title: 'Cảnh báo',
            desc: 'Bạn có muốn thoát không?',
            btnCancelOnPress: () {
              shouldPop = Future.delayed(Duration.zero, () => false);
            },
            btnCancelText: "Hủy",
            onDismissCallback: (type) {},
            btnOkOnPress: () {
              shouldPop = Future.delayed(Duration.zero, () => true);
            },
            btnOkText: "Đồng ý")
        .show();
    return shouldPop;
  }

  @override
  Widget build(BuildContext context) {
    Get.put<NetworkController>(NetworkController());
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: getBody(),
        bottomNavigationBar: getConvexAppBar(),
      ),
    );
  }

  /// Body returns [IndexedStack] which includes 5 main pages of the Convex App Bar.
  Widget getBody() {
    return IndexedStack(
      index: indexPage,
      children: [
        const HomePage(),
        const SearchSubjectPage(),
        PostPage(),
        const ProfilePage(),
        const SettingsPage(),
      ],
    );
  }

  /// Set state for [indexPage] whenever user changes page.
  void _onItemTapped(int index) {
    setState(() {
      indexPage = index;
    });
  }

  /// Controller of the Convex App Bar
  Widget getConvexAppBar() {
    return Container(
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30), topRight: Radius.circular(30)),
          color: kPrimaryColor,
          boxShadow: [
            BoxShadow(color: Colors.black12, spreadRadius: 1, blurRadius: 10)
          ]),
      child: ConvexAppBar(
        backgroundColor: Colors.white,
        activeColor: kPrimaryColor,
        color: kSecondaryColor,

        items: const [
          TabItem(icon: Icons.home),
          TabItem(icon: Icons.search),
          TabItem(icon: Icons.edit),
          TabItem(icon: Icons.person),
          TabItem(icon: Icons.settings),
        ],
        initialActiveIndex: indexPage,
        onTap: _onItemTapped,
      ),
    );
  }
}
