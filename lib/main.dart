import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:review_subject/route_managements/get_all_pages.dart';
import 'package:review_subject/route_managements/routes.dart';

import 'route_managements/bindings.dart';

void main() async {
  HttpOverrides.global = MyHttpOverrides();
  GlobalBindings().dependencies();
  runApp(const MyApp());
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return GetMaterialApp(
      supportedLocales: const [Locale('en', 'US'), Locale('vi', 'VN')],
      debugShowCheckedModeBanner: false,
      title: 'Review Subject',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: kWelcomePage,
      getPages: GetAllPage.getAllPage(),
    );
  }
}

