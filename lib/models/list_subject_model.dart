// To parse this JSON data, do
//
//     final subjectResponse = subjectResponseFromJson(jsonString);

import 'dart:convert';

import 'list_post_model.dart';

List<PostResponse> listSubjectResponseFromJson(List str) => List<PostResponse>.from(str.map((x) => PostResponse.fromJson(x)));

String listSubjectResponseToJson(List<PostResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SubjectResponse {
    SubjectResponse({
        required this.postResponses,
    });

    List<PostResponse> postResponses;

    factory SubjectResponse.fromJson(Map<String, dynamic> json) => SubjectResponse(
        postResponses: List<PostResponse>.from(json["postResponses"].map((x) => PostResponse.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "postResponses": List<dynamic>.from(postResponses.map((x) => x.toJson())),
    };
}

