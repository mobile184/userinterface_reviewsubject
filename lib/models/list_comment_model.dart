// To parse this JSON data, do
//
//     final commentResponse = commentResponseFromJson(jsonString);

import 'dart:convert';

List<CommentResponse> listCommentResponseFromJson(List str) => List<CommentResponse>.from(str.map((x) => CommentResponse.fromJson(x)));

String listCommentResponseToJson(List<CommentResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
class CommentResponse {
    CommentResponse({
        required this.comment,
        required this.userFullname,
        required this.userAvatar,
        required this.countTime,
        required this.typeTime,
    });

    Comment comment;
    String userFullname;
    String userAvatar;
    int countTime;
    int typeTime;

    factory CommentResponse.fromJson(Map<String, dynamic> json) => CommentResponse(
        comment: Comment.fromJson(json["comment"]),
        userFullname: json["userFullname"],
        userAvatar: json["userAvatar"],
        countTime: json["countTime"],
        typeTime: json["typeTime"],
    );

    Map<String, dynamic> toJson() => {
        "comment": comment.toJson(),
        "userFullname": userFullname,
        "userAvatar": userAvatar,
        "countTime": countTime,
        "typeTime": typeTime,
    };
}

class Comment {
    Comment({
        required this.userId,
        required this.postId,
        required this.content,
        required this.id,
        required this.status,
        required this.created,
        required this.updated,
    });

    int userId;
    int postId;
    String? content;
    int id;
    int status;
    DateTime created;
    DateTime updated;

    factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        userId: json["userId"],
        postId: json["postId"],
        content: json["content"],
        id: json["id"],
        status: json["status"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
    );

    Map<String, dynamic> toJson() => {
        "userId": userId,
        "postId": postId,
        "content": content,
        "id": id,
        "status": status,
        "created": created.toIso8601String(),
        "updated": updated.toIso8601String(),
    };
}
