class SchoolModel {
  SchoolModel({
    required this.name,
    required this.address,
    required this.avatar,
    required this.id,
    required this.status,
    required this.created,
    required this.updated,
  });

  String name;
  String address;
  String avatar;
  int id;
  int status;
  DateTime created;
  dynamic updated;

  factory SchoolModel.fromJson(Map<String, dynamic> json) => SchoolModel(
        name: json["name"],
        address: json["address"],
        avatar: json["avatar"] ?? '',
        id: json["id"],
        status: json["status"],
        created: DateTime.parse(json["created"]),
        updated: json["updated"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "address": address,
        "avatar": avatar,
        "id": id,
        "status": status,
        "created": created.toIso8601String(),
        "updated": updated,
      };

  @override
  String toString() {
    // ignore: prefer_interpolation_to_compose_strings
    return name +
        ',' +
        address +
        ',' +
        avatar +
        ',' +
        id.toString() +
        ',' +
        status.toString() +
        ',' +
        created.toString() +
        ',' +
        updated.toString();
  }
}
