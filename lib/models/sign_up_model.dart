// To parse this JSON data, do
//
//     final signUpResponse = signUpResponseFromJson(jsonString);

import 'dart:convert';

List<SignUpResponse> signUpResponseFromJson(List<dynamic> str) => List<SignUpResponse>.from(str.map((x) => SignUpResponse.fromJson(x)));

String signUpResponseToJson(List<SignUpResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SignUpResponse {
    SignUpResponse({
        required this.name,
        required this.address,
        required this.avatar,
        required this.id,
        required this.created,
        required this.updated,
    });

    String name;
    String address;
    String? avatar;
    int id;
    DateTime created;
    DateTime? updated;

    factory SignUpResponse.fromJson(Map<String, dynamic> json) => SignUpResponse(
        name: json["name"],
        address: json["address"],
        avatar: json["avatar"],
        id: json["id"],
        created: DateTime.parse(json["created"]),
        updated: json["updated"] == null ? null : DateTime.parse(json["updated"]),
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "address": address,
        "avatar": avatar,
        "id": id,
        "created": created.toIso8601String(),
        "updated": updated == null ? null : updated!.toIso8601String(),
    };
}
