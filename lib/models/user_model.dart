class UserModel {
  int id;
  String username;
  String password;
  String fullname;
  String email;
  String avatar;
  int universityId;
  int facultyId;
  String major;
  int point;
  DateTime created;
  DateTime updated;

  UserModel(
      {required this.id,
      required this.username,
      required this.password,
      required this.fullname,
      required this.email,
      required this.avatar,
      required this.universityId,
      required this.facultyId,
      required this.major,
      required this.point,
      required this.created,
      required this.updated});
}
