// To parse this JSON data, do
//
//     final topUser = topUserFromJson(jsonString);

// ignore_for_file: unnecessary_null_in_if_null_operators

import 'dart:convert';

List<TopUser> topUserFromJson(List str) => List<TopUser>.from(str.map((x) => TopUser.fromJson(x)));

String topUserToJson(List<TopUser> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TopUser {
    TopUser({
        required this.userName,
        required this.password,
        required this.email,
        required this.fullname,
        required this.avatar,
        required this.universityId,
        required this.facultyId,
        required this.major,
        required this.point,
        required this.id,
        required this.status,
        required this.created,
        required this.updated,
    });

    String userName;
    String password;
    String? email;
    String fullname;
    String avatar;
    int universityId;
    int facultyId;
    String? major;
    int point;
    int id;
    int status;
    DateTime created;
    DateTime? updated;

    factory TopUser.fromJson(Map<String, dynamic> json) => TopUser(
        userName: json["userName"],
        password: json["password"],
        email: json["email"] ?? null,
        fullname: json["fullname"],
        avatar: json["avatar"] ?? null,
        universityId: json["universityId"],
        facultyId: json["facultyId"],
        major: json["major"] ?? null,
        point: json["point"],
        id: json["id"],
        status: json["status"],
        created: DateTime.parse(json["created"]),
        updated: json["updated"] == null ? null : DateTime.parse(json["updated"]),
    );

    Map<String, dynamic> toJson() => {
        "userName": userName,
        "password": password,
        "email": email ?? null,
        "fullname": fullname,
        "avatar": avatar,
        "universityId": universityId,
        "facultyId": facultyId,
        "major": major ?? null,
        "point": point,
        "id": id,
        "status": status,
        "created": created.toIso8601String(),
        "updated": updated == null ? null : updated!.toIso8601String(),
    };
}
