// To parse this JSON data, do
//
//     final listPostByFacultyResponse = listPostByFacultyResponseFromJson(jsonString);

import 'dart:convert';

import 'list_post_model.dart';

List<FacultyResponse> listPostByFacultyFromJson(List str) => List<FacultyResponse>.from(str.map((x) => FacultyResponse.fromJson(x)));

String listPostByFacultyToJson(List<FacultyResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FacultyResponse {
    FacultyResponse({
        required this.name,
        required this.posts,
    });

    String name;
    List<PostResponse> posts;

    factory FacultyResponse.fromJson(Map<String, dynamic> json) => FacultyResponse(
        name: json["name"],
        posts: List<PostResponse>.from(json["posts"].map((x) => PostResponse.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "posts": List<dynamic>.from(posts.map((x) => x.toJson())),
    };
}

