// To parse this JSON data, do
//
//     final notificationModelNew = notificationModelNewFromJson(jsonString);

import 'dart:convert';

NotificationModelNew notificationModelNewFromJson(String str) =>
    NotificationModelNew.fromJson(json.decode(str));

String notificationModelNewToJson(NotificationModelNew data) =>
    json.encode(data.toJson());

class NotificationModelNew {
  NotificationModelNew({
    required this.listNew,
    required this.listOld,
  });

  List<ListElement> listNew;
  List<ListElement> listOld;

  factory NotificationModelNew.fromJson(Map<String, dynamic> json) =>
      NotificationModelNew(
        listNew: List<ListElement>.from(
            json["listNew"].map((x) => ListElement.fromJson(x))),
        listOld: List<ListElement>.from(
            json["listOld"].map((x) => ListElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "listNew": List<dynamic>.from(listNew.map((x) => x.toJson())),
        "listOld": List<dynamic>.from(listOld.map((x) => x.toJson())),
      };
}

class ListElement {
  ListElement({
    required this.notification,
    required this.userBAvatar,
    required this.userBFullname,
    required this.typeTime,
    required this.countTime,
  });

  Notification notification;
  dynamic userBAvatar;
  String userBFullname;
  int typeTime;
  int countTime;

  factory ListElement.fromJson(Map<String, dynamic> json) => ListElement(
        notification: Notification.fromJson(json["notification"]),
        userBAvatar: json["userBAvatar"],
        userBFullname: json["userBFullname"],
        typeTime: json["typeTime"],
        countTime: json["countTime"],
      );

  Map<String, dynamic> toJson() => {
        "notification": notification.toJson(),
        "userBAvatar": userBAvatar,
        "userBFullname": userBFullname,
        "typeTime": typeTime,
        "countTime": countTime,
      };

  @override
  String toString() {
    return '$userBFullname - $notification';
  }
}

class Notification {
  Notification({
    required this.userId,
    required this.userBId,
    required this.postId,
    required this.commentId,
    required this.type,
    required this.isReaded,
    required this.id,
    required this.status,
    required this.created,
    required this.updated,
  });

  int userId;
  int userBId;
  int postId;
  int commentId;
  int type;
  bool isReaded;
  int id;
  int status;
  DateTime created;
  dynamic updated;

  factory Notification.fromJson(Map<String, dynamic> json) => Notification(
        userId: json["userId"],
        userBId: json["userBId"],
        postId: json["postId"],
        commentId: json["commentId"],
        type: json["type"],
        isReaded: json["isReaded"],
        id: json["id"],
        status: json["status"],
        created: DateTime.parse(json["created"]),
        updated: json["updated"],
      );

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "userBId": userBId,
        "postId": postId,
        "commentId": commentId,
        "type": type,
        "isReaded": isReaded,
        "id": id,
        "status": status,
        "created": created.toIso8601String(),
        "updated": updated,
      };

  @override
  String toString() {
    return '$userId - $userBId - $postId';
  }
}
