// To parse this JSON data, do
//
//     final listPostResponse = listPostResponseFromJson(jsonString);

import 'dart:convert';

List<PostResponse> listPostResponseFromJson(List str) => List<PostResponse>.from(str.map((x) => PostResponse.fromJson(x)));

String listPostResponseToJson(List<PostResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PostResponse {
  PostResponse({
    required this.userFullname,
    required this.userAvatar,
    required this.universityName,
    required this.facultyName,
    required this.post,
    required this.isLiked,
    required this.countTime,
    required this.typeTime,
    required this.activeCommentCount,
  });

  String userFullname;
  String userAvatar;
  String universityName;
  String facultyName;
  Post post;
  bool isLiked;
  int countTime;
  int typeTime;
  int activeCommentCount;

  factory PostResponse.fromJson(Map<String, dynamic> json) => PostResponse(
        userFullname: json["userFullname"],
        userAvatar: json["userAvatar"],
        universityName: json["universityName"],
        facultyName: json["facultyName"],
        post: Post.fromJson(json["post"]),
        isLiked: json["isLiked"],
        countTime: json["countTime"],
        typeTime: json["typeTime"],
        activeCommentCount: json["activeCommentCount"],
      );

  Map<String, dynamic> toJson() => {
        "post": post.toJson(),
        "isLiked": isLiked,
        "countTime": countTime,
        "typeTime": typeTime,
      };
}

class Post {
  Post({
    required this.id,
    required this.status,
    required this.created,
    required this.updated,
    required this.subject,
    required this.teacher,
    required this.universityId,
    required this.facultyId,
    required this.major,
    required this.document,
    required this.rateHard,
    required this.reviewHard,
    required this.rateLike,
    required this.reviewLike,
    required this.rateExam,
    required this.reviewExam,
    required this.point,
    required this.userId,
    required this.images,
    required this.likeCount,
    required this.commentCount,
  });

  int id;
  int status;
  DateTime created;
  DateTime updated;
  String subject;
  String teacher;
  int universityId;
  int facultyId;
  String major;
  String document;
  int rateHard;
  String? reviewHard;
  int rateLike;
  String? reviewLike;
  int rateExam;
  String? reviewExam;
  int point;
  int userId;
  List<String> images;
  int likeCount;
  int commentCount;

  factory Post.fromJson(Map<String, dynamic> json) => Post(
        id: json["id"],
        status: json["status"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
        subject: json["subject"],
        teacher: json["teacher"],
        universityId: json["universityId"],
        facultyId: json["facultyId"],
        major: json["major"],
        document: json["document"],
        rateHard: json["rateHard"],
        reviewHard: json["reviewHard"],
        rateLike: json["rateLike"],
        reviewLike: json["reviewLike"],
        rateExam: json["rateExam"],
        reviewExam: json["reviewExam"],
        point: json["point"],
        userId: json["userId"],
        images: json["images"].split(','),
        likeCount: json["likeCount"],
        commentCount: json["commentCount"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "status": status,
        "created": created.toIso8601String(),
        "updated": updated.toIso8601String(),
        "subject": subject,
        "teacher": teacher,
        "universityId": universityId,
        "facultyId": facultyId,
        "major": major,
        "document": document,
        "rateHard": rateHard,
        "reviewHard": reviewHard,
        "rateLike": rateLike,
        "reviewLike": reviewLike,
        "rateExam": rateExam,
        "reviewExam": reviewExam,
        "point": point,
        "userId": userId,
        "images": images.join(','),
        "likeCount": likeCount,
        "commentCount": commentCount,
      };
}
