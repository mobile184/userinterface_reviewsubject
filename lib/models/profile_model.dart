// To parse this JSON data, do
//
//     final profile = profileFromJson(jsonString);

import 'dart:convert';

Profile profileFromJson(Map<String, dynamic> str) => Profile.fromJson(str);

String profileToJson(Profile data) => json.encode(data.toJson());

class Profile {
    Profile({
        required this.user,
        required this.countPost,
        required this.universityName,
        required this.facultyName,
    });

    User user;
    int countPost;
    String universityName;
    String facultyName;

    factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        user: User.fromJson(json["user"]),
        countPost: json["countPost"],
        universityName: json["universityName"],
        facultyName: json["facultyName"],
    );

    Map<String, dynamic> toJson() => {
        "user": user.toJson(),
        "countPost": countPost,
        "universityName": universityName,
        "facultyName": facultyName,
    };
}

class User {
    User({
        required this.userName,
        required this.email,
        required this.fullname,
        required this.avatar,
        required this.universityId,
        required this.facultyId,
        required this.major,
        required this.point,
        required this.id,
        required this.status,
        required this.created,
        required this.updated,
    });

    String userName;
    String email;
    String fullname;
    String avatar;
    int universityId;
    int facultyId;
    String major;
    int point;
    int id;
    int status;
    DateTime created;
    DateTime updated;

    factory User.fromJson(Map<String, dynamic> json) => User(
        userName: json["userName"],
        email: json["email"],
        fullname: json["fullname"],
        avatar: json["avatar"],
        universityId: json["universityId"],
        facultyId: json["facultyId"],
        major: json["major"],
        point: json["point"],
        id: json["id"],
        status: json["status"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
    );

    Map<String, dynamic> toJson() => {
        "userName": userName,
        "email": email,
        "fullname": fullname,
        "avatar": avatar,
        "universityId": universityId,
        "facultyId": facultyId,
        "major": major,
        "point": point,
        "id": id,
        "status": status,
        "created": created.toIso8601String(),
        "updated": updated.toIso8601String(),
    };
}
