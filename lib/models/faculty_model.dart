// To parse this JSON data, do
//
//     final signUpResponse = signUpResponseFromJson(jsonString);

import 'dart:convert';

List<Faculty> facultyResponseFromJson(List<dynamic> str) => List<Faculty>.from(str.map((x) => Faculty.fromJson(x)));

String facultyResponseToJson(List<Faculty> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Faculty {
    Faculty({
        required this.name,
        required this.universityId,
        required this.id,
        required this.status,
        required this.created,
        required this.updated,
    });

    String name;
    int universityId;
    int id;
    int status;
    DateTime created;
    dynamic updated;

    factory Faculty.fromJson(Map<String, dynamic> json) => Faculty(
        name: json["name"],
        universityId: json["universityId"],
        id: json["id"],
        status: json["status"],
        created: DateTime.parse(json["created"]),
        updated: json["updated"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "universityId": universityId,
        "id": id,
        "status": status,
        "created": created.toIso8601String(),
        "updated": updated,
    };
}
