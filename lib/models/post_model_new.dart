// To parse this JSON data, do
//
//     final postModelNew = postModelNewFromJson(jsonString);

import 'dart:convert';

List<PostModelNew> postModelNewFromJson(String str) => List<PostModelNew>.from(
    json.decode(str).map((x) => PostModelNew.fromJson(x)));

String postModelNewToJson(List<PostModelNew> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PostModelNew {
  PostModelNew({
    required this.subject,
    required this.teacher,
    required this.universityId,
    required this.facultyId,
    required this.major,
    required this.document,
    required this.rateHard,
    required this.reviewHard,
    required this.rateLike,
    required this.reviewLike,
    required this.rateExam,
    this.reviewExam,
    required this.point,
    required this.userId,
    required this.images,
    required this.likeCount,
    required this.commentCount,
    required this.id,
    required this.status,
    required this.created,
    required this.updated,
  });

  String subject;
  String teacher;
  int universityId;
  int facultyId;
  String major;
  String document;
  int rateHard;
  String reviewHard;
  int rateLike;
  String reviewLike;
  int rateExam;
  dynamic reviewExam;
  int point;
  int userId;
  String images;
  int likeCount;
  int commentCount;
  int id;
  int status;
  DateTime created;
  DateTime updated;

  factory PostModelNew.fromJson(Map<String, dynamic> json) => PostModelNew(
        subject: json["subject"],
        teacher: json["teacher"],
        universityId: json["universityId"],
        facultyId: json["facultyId"],
        major: json["major"],
        document: json["document"],
        rateHard: json["rateHard"],
        reviewHard: json["reviewHard"],
        rateLike: json["rateLike"],
        reviewLike: json["reviewLike"],
        rateExam: json["rateExam"],
        reviewExam: json["reviewExam"],
        point: json["point"],
        userId: json["userId"],
        images: json["images"],
        likeCount: json["likeCount"],
        commentCount: json["commentCount"],
        id: json["id"],
        status: json["status"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
      );

  Map<String, dynamic> toJson() => {
        "subject": subject,
        "teacher": teacher,
        "universityId": universityId,
        "facultyId": facultyId,
        "major": major,
        "document": document,
        "rateHard": rateHard,
        "reviewHard": reviewHard,
        "rateLike": rateLike,
        "reviewLike": reviewLike,
        "rateExam": rateExam,
        "reviewExam": reviewExam,
        "point": point,
        "userId": userId,
        "images": images,
        "likeCount": likeCount,
        "commentCount": commentCount,
        "id": id,
        "status": status,
        "created": created.toIso8601String(),
        "updated": updated.toIso8601String(),
      };
}
