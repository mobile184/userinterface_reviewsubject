import 'dart:convert';

SignInResponse signInResponseFromJson(String str) =>
    SignInResponse.fromJson(json.decode(str));

String signInResponseToJson(SignInResponse data) => json.encode(data.toJson());

class SignInResponse {
  SignInResponse({
    required this.token,
    required this.user,
  });

  String token;
  User? user;

  factory SignInResponse.fromJson(Map<String, dynamic> json) => SignInResponse(
        token: json["token"],
        user: json["user"] == null ? null : User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "token": token,
        "user": user == null ? null : user!.toJson(),
      };
}

class User {
  User({
    required this.userName,
    required this.email,
    required this.fullname,
    required this.avatar,
    required this.universityId,
    required this.facultyId,
    required this.major,
    required this.point,
    required this.id,
    required this.created,
    required this.updated,
  });

  String userName;
  String email;
  String fullname;
  String avatar;
  int universityId;
  int facultyId;
  String major;
  int point;
  int id;
  DateTime created;
  DateTime updated;

  factory User.fromJson(Map<String, dynamic> json) => User(
        userName: json["userName"],
        email: json["email"],
        fullname: json["fullname"],
        avatar: json["avatar"],
        universityId: json["universityId"],
        facultyId: json["facultyId"],
        major: json["major"],
        point: json["point"],
        id: json["id"],
        created: DateTime.parse(json["created"]),
        updated: DateTime.parse(json["updated"]),
      );

  Map<String, dynamic> toJson() => {
        "userName": userName,
        "email": email,
        "fullname": fullname,
        "avatar": avatar,
        "universityId": universityId,
        "facultyId": facultyId,
        "major": major,
        "point": point,
        "id": id,
        "created": created.toIso8601String(),
        "updated": updated.toIso8601String(),
      };

  @override
  String toString() {
    // ignore: prefer_interpolation_to_compose_strings
    return userName +
        ',' +
        email +
        ',' +
        fullname +
        ',' +
        avatar +
        ',' +
        universityId.toString() +
        ',' +
        facultyId.toString() +
        ',' +
        major +
        ',' +
        point.toString() +
        ',' +
        id.toString() +
        ',' +
        created.toString() +
        ',' +
        updated.toString();
  }
}
