// ignore_for_file: depend_on_referenced_packages

import 'dart:io';

import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:review_subject/models/notification_model_new.dart';
import 'package:review_subject/models/school_model.dart';
import 'package:http_parser/http_parser.dart';

import '../models/list_post_model.dart';

class HttpService {
  late Dio _dio;
  

  static const baseUrl = "https://192.168.239.118:45455/";

  ///Initialized [HttpService] with Interceptor
  HttpService() {
    _dio = Dio(BaseOptions(
      baseUrl: baseUrl,
    ));
    initializeInterceptors();
  }

  ///Load list of image from server
  ///
  ///Load images from server using path [images] and save
  ///in temp directory
  Future<List<File>> fileListFromImageUrl(
      {required List<String> images}) async {
    List<File> fileList = [];
    for (int i = 0; i < images.length; i++) {
      Response response = await _dio.get(
        baseUrl + images[i],
        //Received data with List<int>
        options: Options(
            responseType: ResponseType.bytes,
            followRedirects: false,
            validateStatus: (status) {
              return status! < 500;
            }),
      );
      Directory tempDir = await getTemporaryDirectory();
      String tempPath = tempDir.path;
      // print("path ${tempPath + images[i]}");
      File file = File(tempPath + images[i]);
      if (file.existsSync()) {
        fileList.add(file);
        continue;
      }
      var raf = file.openSync(mode: FileMode.write);
      // response.data is List<int> type
      raf.writeFromSync(response.data);
      await raf.close();
      fileList.add(file);
    }
    return fileList;
  }

  ///Post new comment to server
  ///
  ///Send post new comment request to server 
  ///params [endPoint], [token], [userId],
  ///[postId], [content]. Throw exceptions if fails
  ///else return [response]
  Future<Response> postNewCommentRequest(
      {required String endPoint,
      required String token,
      required int userId,
      required int postId,
      required String content}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.post(endPoint, data: {
        "userId": userId,
        "postId": postId,
        "content": content,
      });
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Put image request to server
  ///
  ///Send put image request to server 
  ///params [endPoint], [token], [images],
  ///Throw exceptions if fails
  ///else return [response]
  Future<Response> putImageNewPostRequest(
      {required String endPoint,
      required String token,
      required List<File> images}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      _dio.options.headers["Accept"] = "*/*";
      _dio.options.headers["Content-Type"] = "multipart/form-data";
      var formData = FormData();
      List<MultipartFile> multipartImageList = [];
      for (var image in images) {
        MultipartFile pic = await MultipartFile.fromFile(image.path,
            filename: image.path.split("/").last,
            contentType: MediaType("multipart", "form-data"));
        multipartImageList.add(pic);
      }
      formData = FormData.fromMap({
        "images": multipartImageList,
      });
      response = await _dio.put(
        endPoint,
        data: formData,
      );
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Post text data request to server
  ///
  ///Send post text data request to server 
  ///params [endPoint], [token], [subject],
  ///[teacher], [universityId], [facultyId],
  ///[major], [document], [rateHard], [reviewHard],
  ///[rateLike], [reviewLike], [rateExam], [reviewExam],
  ///[userId]. Throw exceptions if fails
  ///else return [response]
  Future<Response> postTextNewPostRequest(
      {required String endPoint,
      required String token,
      required String subject,
      required String teacher,
      required int universityId,
      required int facultyId,
      required String major,
      required String document,
      required int rateHard,
      required String reviewHard,
      required int rateLike,
      required String reviewLike,
      required int rateExam,
      required String reviewExam,
      required int userId}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.post(endPoint, data: {
        "subject": subject,
        "teacher": teacher,
        "universityId": universityId,
        "facultyId": facultyId,
        "major": major,
        "document": document,
        "rateHard": rateHard,
        "reviewHard": reviewHard,
        "rateLike": rateLike,
        "reviewLike": reviewLike,
        "rateExam": rateExam,
        "reviewExam": reviewExam,
        "userId": userId
      });
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Put text data request to server
  ///
  ///Send put text data request to server 
  ///params [endPoint], [token], [subject],
  ///[teacher], [universityId], [facultyId],
  ///[major], [document], [rateHard], [reviewHard],
  ///[rateLike], [reviewLike], [rateExam], [reviewExam],
  ///[userId]. Throw exceptions if fails
  ///else return [response]
  Future<Response> putTextNewPostRequest(
      {required String endPoint,
      required String token,
      required String subject,
      required String teacher,
      required int universityId,
      required int facultyId,
      required String major,
      required String document,
      required int rateHard,
      required String reviewHard,
      required int rateLike,
      required String reviewLike,
      required int rateExam,
      required String reviewExam,
      required int userId}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.put(endPoint, data: {
        "subject": subject,
        "teacher": teacher,
        "universityId": universityId,
        "facultyId": facultyId,
        "major": major,
        "document": document,
        "rateHard": rateHard,
        "reviewHard": reviewHard,
        "rateLike": rateLike,
        "reviewLike": reviewLike,
        "rateExam": rateExam,
        "reviewExam": reviewExam,
        "userId": userId
      });
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Get profile request to server
  ///
  ///Send get profile request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future<Response> getProfileByIdRequest(
      {required String endPoint, required String token}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.get(endPoint);
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Get subject request to server
  ///
  ///Send get subject request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future<Response> getSubjectBySearchKeyRequest(
      {required String endPoint, required String token}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.get(endPoint);
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Delete like request to server
  ///
  ///Send delete like request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future<Response> deleteLikeRequest(
      {required String endPoint, required String token}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.delete(endPoint);
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Post like request to server
  ///
  ///Send post like request to server 
  ///params [endPoint], [token], [userId],
  ///[postId]. Throw exceptions 
  ///if fails else return [response]
  Future<Response> postLikeRequest(
      {required String endPoint,
      required int userId,
      required postId,
      required String token}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.post(endPoint, data: {
        "userId": userId,
        "postId": postId,
      });
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Post sign up request to server
  ///
  ///Send post sign up request to server 
  ///params [endPoint], [userName], [password],
  ///[fullName], [email], [avatar], [universityId], 
  ///[facultyId], [major]. Throw exceptions 
  ///if fails else return [response]
  Future<Response> postSignUpRequest(
      {required String endPoint,
      required String userName,
      required String password,
      required fullName,
      required String email,
      String avatar = "",
      required int universityId,
      required int facultyId,
      required String major}) async {
    Response response;
    try {
      response = await _dio.post(endPoint, data: {
        "userName": userName,
        "password": password,
        "email": email,
        "fullname": fullName,
        "avatar": avatar,
        "universityId": universityId,
        "facultyId": facultyId,
        "major": major
      });
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Post sign in request to server
  ///
  ///Send post sign in request to server 
  ///params [endPoint], [userName], [password],
  /// Throw exceptions 
  ///if fails else return [response]
  Future<Response> postSignInRequest(
      {required String endPoint,
      required String userName,
      required String password}) async {
    Response response;
    try {
      response = await _dio
          .post(endPoint, data: {"userName": userName, "password": password});
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Put change password request to server
  ///
  ///Send put change password request to server 
  ///params [endPoint], [userName], [password],
  ///[token]. Throw exceptions 
  ///if fails else return [response]
  Future<Response> putChangePasswordRequest(
      {required String endPoint,
      required String token,
      required String userName,
      required String password}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";

      response = await _dio
          .put(endPoint, data: {"userName": userName, "password": password});
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Get data request to server
  ///
  ///Send get data request to server 
  ///params [endPoint]. Throw exceptions 
  ///if fails else return [response]
  Future<Response> getDataRequest({required String endPoint}) async {
    Response response;
    try {
      response = await _dio.get(endPoint);
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Get faculties list request to server
  ///
  ///Send get faculties list request to server 
  ///params [endPoint]. Throw exceptions 
  ///if fails else return [response]
  Future<Response> getFacultyListRequest({required String endPoint}) async {
    Response response;
    try {
      response = await _dio.get(endPoint);
    } on DioError catch (e) {
      throw Exception(e.message);
    }

    return response;
  }

  ///Get uni list with key request to server
  ///
  ///Send get uni list with key request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future<List<SchoolModel>> getSearchUniRequest(
      {required String endPoint, required String token}) async {
    Response response;
    List<SchoolModel> listSchool;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.get(endPoint);
      var data = response.data as List;
      listSchool = data.map((data) => SchoolModel.fromJson(data)).toList();
      return listSchool;
    } on DioError catch (e) {
      throw Exception(e.message);
    }
  }

  ///Get all uni list request to server
  ///
  ///Send get all uni list request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future<List<SchoolModel>> getAllUniRequest(
      {required String endPoint, required String token}) async {
    Response response;
    List<SchoolModel> listSchool;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.get(endPoint);
      var data = response.data as List;
      listSchool = data.map((data) => SchoolModel.fromJson(data)).toList();
      return listSchool;
    } on DioError catch (e) {
      throw Exception(e.message);
    }
  }

  ///Get uni list by id request to server
  ///
  ///Send get uni list by id request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future getUniByIdRequest(
      {required String endPoint, required String token}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.get(endPoint);
      var data = response.data;
      return data;
    } on DioError catch (e) {
      throw Exception(e.message);
    }
  }

  ///Get history posts list request to server
  ///
  ///Send get history posts list request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future<List<PostResponse>> getHistoryPostRequest(
      {required String endPoint, required String token}) async {
    Response response;
    List<PostResponse> listHistoryPosts;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.get(endPoint);
      var data = response.data;
      listHistoryPosts = listPostResponseFromJson(data);
      return listHistoryPosts;
    } on DioError catch (e) {
      throw Exception(e.message);
    }
  }

  ///Get notifications list request to server
  ///
  ///Send get notifications list request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future<NotificationModelNew> getNotificationsRequest(
      {required String endPoint, required String token}) async {
    Response response;
    NotificationModelNew responseData;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.get(endPoint);
      var data = response.data;
      responseData = NotificationModelNew.fromJson(data);
      return responseData;
    } on DioError catch (e) {
      throw Exception(e.message);
    }
  }

  ///Put read notifications request to server
  ///
  ///Send put read notifications request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future<Response> putReadNotificationRequest(
      {required String endPoint, required String token}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";

      response = await _dio.put(endPoint);
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Put new avatar request to server
  ///
  ///Send put new avatar request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future<Response> putNewAvatarRequest(
      {required String endPoint,
      required String token,
      required File image}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";

      String fileName = image.path.split('/').last;

      FormData formData = FormData.fromMap({
        "avatar": await MultipartFile.fromFile(image.path, filename: fileName),
      });

      response = await _dio.put(endPoint, data: formData);
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Put change profile request to server
  ///
  ///Send put change profile request to server 
  ///params [endPoint], [token], [id], [status], 
  ///[created], [updated], [userName], [email], 
  ///[fullname], [avatar], [universityId], 
  ///[facultyId], [major], [point]. Throw exceptions 
  ///if fails else return [response]
  Future<Response> putChangeProfileRequest({
    required String endPoint,
    required String token,
    required int id,
    required int status,
    required String created,
    required String updated,
    required String userName,
    required String email,
    required String fullname,
    required String avatar,
    required int universityId,
    required int facultyId,
    required String major,
    required int point,
  }) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";

      response = await _dio.put(endPoint, data: {
        "id": id,
        "status": status,
        "created": "2022-09-15T10:57:41.84",
        "updated": "2022-09-15T10:57:41.84",
        "userName": userName,
        "email": email,
        "fullname": fullname,
        "avatar": avatar,
        "universityId": universityId,
        "facultyId": facultyId,
        "major": major,
        "point": point
      });
    } on DioError catch (e) {
      throw Exception(e.message);
    }
    return response;
  }

  ///Get top users request to server
  ///
  ///Send get top users request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future getTopUserRequest(
      {required String endPoint, required String token}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.get(endPoint);
      return response;
    } on DioError catch (e) {
      throw Exception(e.message);
    }
  }

  ///Get post by id request to server
  ///
  ///Send get post by id request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future getPostByIdRequest(
      {required String endPoint, required String token}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.get(endPoint);
      return response;
    } on DioError catch (e) {
      throw Exception(e.message);
    }
  }

  ///Delete post request to server
  ///
  ///Delete post request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future deletePostRequest({required String endPoint, required String token}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.delete(endPoint);
      return response;
    } on DioError catch (e) {
      throw Exception(e.message);
    }
  }

  ///Delete comment request to server
  ///
  ///Delete comment request to server 
  ///params [endPoint], [token]. Throw exceptions 
  ///if fails else return [response]
  Future deleteCommentRequest({required String endPoint, required String token}) async {
    Response response;
    try {
      _dio.options.headers["Authorization"] = "Bearer $token";
      response = await _dio.delete(endPoint);
      return response;
    } on DioError catch (e) {
      throw Exception(e.message);
    }
  }

  ///Create interceptors to manage
  ///connection
  initializeInterceptors() {
    _dio.interceptors.add(
        InterceptorsWrapper(onError: (error, errorInterceptorHandler) async {
      // print(error.message);
      return errorInterceptorHandler.next(error);
    }, onRequest: (request, requestInterceptorHandler) {
      // print("${request.method} ${request.path}");
      return requestInterceptorHandler.next(request);
    }, onResponse: (response, responseInterceptorHandler) {
      // print(response.data);
      return responseInterceptorHandler.next(response);
    }));
  }
}
