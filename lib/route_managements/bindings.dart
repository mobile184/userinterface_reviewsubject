import 'package:get/get.dart';
import 'package:review_subject/controllers/change_password_controller.dart';
import 'package:review_subject/controllers/history_posts_controller.dart';
import 'package:review_subject/controllers/modify_info_form_controller.dart';

import '../controllers/detail_controller.dart';
import '../controllers/home_controller.dart';
import '../controllers/network_controller.dart';
import '../controllers/notification_controller.dart';
import '../controllers/post_controller.dart';
import '../controllers/profile_controller.dart';
import '../controllers/search_school_controller.dart';
import '../controllers/search_subject_controller.dart';
import '../controllers/sign_in_controller.dart';
import '../controllers/sign_up_controller.dart';

class GlobalBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeController());
    Get.lazyPut(() => SearchSubjectController());
    Get.lazyPut(() => PostController());
    Get.lazyPut(() => ChangePasswordController());
  }
}

class HomeBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeController());
    Get.lazyPut(() => SearchSubjectController());
    Get.lazyPut(() => PostController());
    Get.lazyPut(() => ChangePasswordController());
  }
}

class SearchSchoolBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SearchSchoolController());
  }
}

class ProfileBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ProfileController());
  }
}

class SignInBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ProfileController());
    Get.lazyPut(() => ModifyInfoFormController());
    Get.lazyPut(() => SignInController());
    Get.lazyPut(() => HomeController());
    Get.lazyPut(() => SearchSubjectController());
    Get.lazyPut(() => PostController());
    Get.lazyPut(() => ChangePasswordController());
  }
}

class SignUpBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SignUpController());
  }
}

class NotificationBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => NotificationController());
  }
}

class SearchSubjectBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SearchSubjectController());
  }
}

class DetailBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DetailController());
  }
}

class PostBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PostController());
  }
}

class ChangePasswordBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ChangePasswordController());
  }
}

class HistoryPostBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HistoryPostController());
  }
}

class ModifyInfoBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ModifyInfoFormController());
  }
}

class NetworkBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => NetworkController(), fenix: true);
  }
}
