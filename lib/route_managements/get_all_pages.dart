import 'package:get/get.dart';
import 'package:review_subject/controller_page.dart';
import 'package:review_subject/pages/detail_page/components/full_screen_images.dart';
import 'package:review_subject/pages/history_posts_page/history_posts_page.dart';
import 'package:review_subject/pages/modify_info_page/modify_info_page.dart';
import 'package:review_subject/pages/reset_password_password/reset_password_page.dart';
import 'package:review_subject/pages/sign_in_page/sign_in_page.dart';
import 'package:review_subject/pages/sign_up_page/sign_up_page.dart';
import 'package:review_subject/pages/welcome_page/welcom_page.dart';
import 'package:review_subject/route_managements/bindings.dart';

import '../pages/detail_page/detail_page.dart';
import '../pages/notification_page/notification_page.dart';
import '../pages/search_school_page/search_school_page.dart';
import 'routes.dart';

class GetAllPage {
  static List<GetPage> getAllPage() {
    return [
      GetPage(name: kFullScreenImages, page: () => FullScreenImages()),
      GetPage(
          name: kHomePage,
          page: () => const ControllerPage(index: 0),
          binding: HomeBindings()),
      GetPage(
          name: kSearchSubjectPage,
          page: () => const ControllerPage(index: 1),
          binding: SearchSubjectBindings()),
      GetPage(
          name: kPostPage,
          page: () => const ControllerPage(index: 2),
          binding: PostBindings()),
      GetPage(
          name: kProfilePage,
          page: () => const ControllerPage(index: 3),
          binding: ProfileBindings()),
      GetPage(
          name: kSettingsPage,
          page: () => const ControllerPage(index: 4),
          binding: ChangePasswordBindings()),
      GetPage(
          name: kSignInPage,
          page: () => const SignInPage(),
          binding: SignInBindings()),
      GetPage(
          name: kSearchSchoolPage,
          page: () => const SearchSchoolPage(),
          binding: SearchSchoolBindings()),
      GetPage(
          name: kSignUpPage,
          page: () => const SignUpPage(),
          binding: SignUpBindings()),
      GetPage(
          name: kResetPasswordPage,
          page: () => const ResetPasswordPage(),
          binding: ChangePasswordBindings()),
      GetPage(
          name: kNotificationPage,
          page: () => const NotificationPage(),
          binding: NotificationBindings()),
      GetPage(
          name: kHistoryPostsPage,
          page: () => const HistoryPostsPage(),
          binding: HistoryPostBindings()),
      GetPage(
          name: kDetailPage,
          page: () => const DetailPage(),
          binding: DetailBindings()),
      GetPage(
          name: kControllerPage, page: () => const ControllerPage(index: 0)),
      GetPage(name: kNotificationPage, page: () => const NotificationPage()),
      GetPage(name: kWelcomePage, page: () => const WelcomePage()),
      GetPage(
          name: kModifyInfoPage,
          page: () => const ModifyInfoPage(),
          binding: ModifyInfoBindings()),
    ];
  }
}
