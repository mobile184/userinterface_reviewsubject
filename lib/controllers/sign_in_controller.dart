import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/show_dialog.dart';
import 'package:review_subject/controllers/my_shared_preference.dart';
import 'package:review_subject/route_managements/routes.dart';
import 'package:dio/dio.dart' as dio;
import '../models/sign_in_model.dart';
import '../service/service.dart';

class SignInController extends GetxController {
  Rx<TextEditingController> userNameTextController =
      TextEditingController().obs;
  Rx<TextEditingController> passwordTextController =
      TextEditingController().obs;
  RxBool isValidUserName = false.obs;
  RxBool isValidPassword = false.obs;
  RxBool isPassWordVisible = false.obs;
  late HttpService http = HttpService();

  ///Check [data] valid. If true set [isValidUserName]
  ///true, else false
  checkValidUserName(String data) {
    data != "" ? isValidUserName.value = true : isValidUserName.value = false;
  }

  ///Check [data] valid. If true set [isValidPassword]
  ///true, else false
  checkValidPassword(String data) {
    data != "" ? isValidPassword.value = true : isValidPassword.value = false;
  }

  ///Post sign in request to server
  ///
  ///Send post sign in request to server
  ///Show warning snackbar if fails, else
  ///reset local storage data and get to
  ///[kControllerPage]
  Future<void> signIn({required BuildContext context}) async {
    dio.Response response;
    try {
      showLoaderDialog(context);
      response = await http.postSignInRequest(
          endPoint: "api/Login/UserLogin",
          userName: userNameTextController.value.text.trim(),
          password: passwordTextController.value.text.trim());
      Get.back();
      if (response.statusCode == 200) {
        SignInResponse signInResponse = SignInResponse.fromJson(response.data);
        User? user = signInResponse.user;
        if (signInResponse.token == "0") {
          Get.snackbar("Cảnh báo", "Tài khoản không tồn tại!",
              colorText: Colors.yellow.shade800,
              backgroundColor: Colors.white,
              icon: Icon(
                Icons.warning,
                color: Colors.yellow.shade800,
              ));
        } else if (signInResponse.token == "1" && signInResponse.user == null) {
          Get.snackbar("Cảnh báo", "Mật khẩu không đúng!",
              colorText: Colors.yellow.shade800,
              backgroundColor: Colors.white,
              icon: Icon(
                Icons.warning,
                color: Colors.yellow.shade800,
              ));
        } else if (signInResponse.token == "2" && signInResponse.user == null) {
          Get.snackbar("Cảnh báo",
              "Tài khoản hiện đang bị khóa, vui lòng sử dụng tài khoản khác!",
              colorText: Colors.yellow.shade800,
              backgroundColor: Colors.white,
              icon: Icon(
                Icons.warning,
                color: Colors.yellow.shade800,
              ));
        } else {
          MySharedPreference.clearData();
          MySharedPreference.setUserAvatar(avatar: signInResponse.user!.avatar);
          MySharedPreference.setToken(token: signInResponse.token);
          MySharedPreference.setUserId(idUser: user!.id);
          MySharedPreference.setUserFullname(userFullname: user.fullname);
          var data = await http.getUniByIdRequest(
              endPoint: "api/Universities/${user.universityId}",
              token: signInResponse.token);
          String school = json.encode(data);
          MySharedPreference.setUniversity(university: school);
          Get.back();
          Get.offAllNamed(kControllerPage, arguments: []);
        }
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Check condition when click sign in button
  ///
  ///Check [isValidPassword], [isValidUserName]
  ///if any false, show warning snackbar, else
  ///call [signIn]
  onSignInButtonClick({required BuildContext context}) {
    if ([isValidPassword.value, isValidUserName.value]
        .any((element) => element == false)) {
      Get.snackbar("Cảnh báo", "Tài khoản hoặc mật khẩu không hợp lệ!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
    } else {
      signIn(context: context);
    }
  }

  ///Change to [kSignUpPage]
    onToSignUpButtonClick({required BuildContext context}) {
    Get.toNamed(kSignUpPage);
  }
}
