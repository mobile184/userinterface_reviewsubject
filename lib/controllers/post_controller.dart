// ignore_for_file: unrelated_type_equality_checks, invalid_use_of_protected_member

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:review_subject/components/show_dialog.dart';
import 'package:review_subject/controller_page.dart';
import 'package:review_subject/models/list_post_model.dart';
import 'package:review_subject/service/service.dart';

import '../models/faculty_model.dart';
import '../models/sign_up_model.dart';
import 'package:dio/dio.dart' as dio;

import 'my_shared_preference.dart';

class PostController extends GetxController {
  bool createPost = true;
  String token = "";
  int userId = 0;
  int postId = 0;
  RxList<String> schoolList = <String>["Chọn trường"].obs;
  RxMap<String, int> schoolListId = {"": 0}.obs;
  RxList<String> facultyList = <String>["Hãy chọn trường trước"].obs;
  RxMap<String, int> facultyListId = {"": 0}.obs;
  RxString currentSchool = "Chọn trường".obs;
  RxString currentFaculty = "Hãy chọn trường trước".obs;
  Rx<TextEditingController> majorController = TextEditingController().obs;
  Rx<TextEditingController> subjectController = TextEditingController().obs;
  Rx<TextEditingController> teacherController = TextEditingController().obs;
  Rx<TextEditingController> cirriculumController = TextEditingController().obs;
  Rx<TextEditingController> hardRateReasonController =
      TextEditingController().obs;
  Rx<TextEditingController> likeRateReasonController =
      TextEditingController().obs;
  Rx<TextEditingController> examRateReasonController =
      TextEditingController().obs;
  RxList<String> cirriculumList = <String>[].obs;
  RxList<File> imageFiles = <File>[].obs;
  Rx hardRate = 0.obs;
  RxInt likeRate = 0.obs;
  RxInt examRate = 0.obs;
  RxString dropdownSchoolValue = "".obs;
  RxString dropdownFacultyValue = "".obs;
  RxBool isNewPost = true.obs;
  HttpService http = HttpService();
  RxBool isValidMajor = true.obs;
  RxBool isValidSubject = true.obs;
  RxBool isValidTeacher = true.obs;
  RxBool isValidFaculty = true.obs;
  RxBool isValidRate = true.obs;
  RxBool isLoadingImages = false.obs;

  ///Reset data of controller with data
  ///like when controller is initialized
  void resetController() {
    createPost = true;
    token = "";
    userId = 0;
    postId = 0;
    schoolList = <String>["Chọn trường"].obs;
    schoolListId = {"": 0}.obs;
    facultyList = <String>["Hãy chọn trường trước"].obs;
    facultyListId = {"": 0}.obs;
    currentSchool = "Chọn trường".obs;
    currentFaculty = "Hãy chọn trường trước".obs;
    majorController = TextEditingController().obs;
    subjectController = TextEditingController().obs;
    teacherController = TextEditingController().obs;
    cirriculumController = TextEditingController().obs;
    hardRateReasonController = TextEditingController().obs;
    likeRateReasonController = TextEditingController().obs;
    examRateReasonController = TextEditingController().obs;
    cirriculumList = <String>[].obs;
    imageFiles = <File>[].obs;
    hardRate = 0.obs;
    likeRate = 0.obs;
    examRate = 0.obs;
    dropdownSchoolValue = "".obs;
    dropdownFacultyValue = "".obs;
    isNewPost = true.obs;
    isValidMajor = true.obs;
    isValidSubject = true.obs;
    isValidTeacher = true.obs;
    isValidFaculty = true.obs;
    isValidRate = true.obs;
  }

  ///Check if [majorController] is valid or not
  ///
  ///Returns void and change data of [isValidMajor]
  ///to true if satisfied and false if not
  checkValidMajor() {
    majorController.value.text == ""
        ? isValidMajor.value = false
        : isValidMajor.value = true;
  }

  ///Check if [currentFaculty] is valid or not
  ///
  ///Returns void and change data of [isValidFaculty]
  ///to true if satisfied and false if not
  checkValidFaculty() {
    return currentFaculty.value == "Hãy chọn trường trước"
        ? isValidFaculty.value = false
        : isValidFaculty.value = true;
  }

  ///Check if [subjectController] is valid or not
  ///
  ///Returns void and change data of [isValidSubject]
  ///to true if satisfied and false if not
  checkValidSubject() {
    return subjectController.value.text == ""
        ? isValidSubject.value = false
        : isValidSubject.value = true;
  }

  ///Check if [teacherController] is valid or not
  ///
  ///Returns true if [teacherController] is not null
  /// and false if not
  checkValidTeacher() {
    return teacherController.value.text == "" ? false : true;
  }

  ///Check if [hardRate], [likeRate], [examRate] is valid or not
  ///
  ///Returns true if [hardRate], [likeRate], [examRate] not equals to 0
  /// and false if not
  bool checkValidRate() {
    return hardRate == 0 || likeRate == 0 || examRate == 0
        ? isValidRate.value = false
        : isValidRate.value = true;
  }

  ///Load data from arguments and set values
  ///of all vars with that data
  loadDataFromLocal() async {
    token = await MySharedPreference.getToken();
    userId = await MySharedPreference.getUserId();
    if (Get.arguments != null &&
        Get.arguments != [] &&
        Get.arguments.length > 0) {
      PostResponse data = Get.arguments["data"];
      isNewPost.value = false;
      postId = data.post.id;
      currentSchool.value = data.universityName;
      currentFaculty.value = data.facultyName;
      schoolListId.value = {currentSchool.value: data.post.universityId};
      facultyListId.value = {currentFaculty.value: data.post.facultyId};
      facultyList.value = [currentFaculty.value];
      majorController.value.text = data.post.major;
      subjectController.value.text = data.post.subject;
      teacherController.value.text = data.post.teacher;
      cirriculumList.value = data.post.document.split(',');
      hardRate.value = data.post.rateHard;
      data.post.reviewHard == null
          ? hardRateReasonController.value.text = ""
          : hardRateReasonController.value.text = data.post.reviewHard!;
      likeRate.value = data.post.rateLike;
      data.post.reviewLike == null
          ? likeRateReasonController.value.text = ""
          : likeRateReasonController.value.text = data.post.reviewLike!;
      examRate.value = data.post.rateExam;
      data.post.reviewExam == null
          ? examRateReasonController.value.text = ""
          : examRateReasonController.value.text = data.post.reviewExam!;
      if (data.post.images.isNotEmpty && data.post.images[0] != "") {
        isLoadingImages.value = true;
        var oldImages =
            await http.fileListFromImageUrl(images: data.post.images);
        imageFiles.addAll(oldImages);
        isLoadingImages.value = false;
      }
      imageFiles.refresh();
    }
  }

  ///Load data of list university
  ///
  ///Send a get list universities request to
  ///server. Show warning snackbar if fails,
  ///else set data for [schoolList] and [schoolListId]
  Future<void> loadData({required BuildContext context}) async {
    dio.Response response;
    try {
      response = await http.getDataRequest(endPoint: "api/Universities");
      if (response.statusCode == 200) {
        dynamic data = signUpResponseFromJson(response.data);
        schoolList.value =
            data.map<String>((SignUpResponse data) => data.name).toList();
        for (int i = 0; i < schoolList.length; i++) {
          schoolListId[data[i].name] = data[i].id;
        }
        currentSchool.value = schoolList[0];
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");
        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Load data of list faculties
  ///
  ///Send a get list faculties request by [id] to
  ///server. Show warning snackbar if fails,
  ///else set data for [facultyList] and [facultyListId]
  Future<void> loadFacultyBySchoolId(
      {required BuildContext context, required int id}) async {
    dio.Response response;
    try {
      response = await http.getFacultyListRequest(
          endPoint: "api/Faculties/ByUniversityId?universityId=$id");
      if (response.statusCode == 200) {
        List<Faculty> data = facultyResponseFromJson(response.data);
        facultyList.value =
            data.map<String>((Faculty data) => data.name).toList();
        for (int i = 0; i < facultyList.length; i++) {
          facultyListId[facultyList[i]] = data[i].id;
        }
        currentFaculty.value = facultyList[0];
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");
        return response.data;
      }
    } on Exception catch (e) {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi $e",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
    }
  }

  ///Update text data of post
  ///
  ///Send a put text data request to update
  ///text data of post with param [subjectController],
  ///[teacherController], [schoolListId], [facultyListId],
  ///[currentSchool], [currentFaculty], [majorController],
  ///[cirriculumList], [hardRate], [likeRate], [examRate], [userId]
  ///server. Show warning snackbar if fails
  putTextData() async {
    dio.Response response;
    try {
      response = await http.putTextNewPostRequest(
        endPoint: "api/Posts/$postId?userId=$userId",
        token: token,
        subject: subjectController.value.text.trim(),
        teacher: teacherController.value.text.trim(),
        universityId: schoolListId[currentSchool.value] ?? 0,
        facultyId: facultyListId[currentFaculty.value] ?? 0,
        major: majorController.value.text.trim(),
        document: cirriculumList.join(','),
        rateHard: hardRate.value,
        reviewHard: hardRateReasonController.value.text.trim(),
        rateLike: likeRate.value,
        reviewLike: likeRateReasonController.value.text,
        rateExam: examRate.value,
        reviewExam: examRateReasonController.value.text,
        userId: userId,
      );
      if (response.statusCode == 200) {
        return response.data["id"];
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");
        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Check if all condition satisfied and call [putImageData]
  ///
  ///Check [isValidMajor], [isValidFaculty], [isValidSubject],
  ///[isValidTeacher], [isValidRate]. If any false, show warning snackbar
  ///else call [postTextData] and [putImageData] then back to [ControllerPage]
  onPostButtonPress(context) async {
    checkValidMajor();
    checkValidFaculty();
    checkValidSubject();
    checkValidTeacher();
    checkValidRate();
    if ([
      isValidMajor,
      isValidFaculty,
      isValidSubject,
      isValidTeacher,
      isValidRate
    ].any((element) => element.value == false)) {
      Get.snackbar("Cảnh báo", "Vui lòng nhập đúng thông tin!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
    } else {
      showLoaderDialog(context);
      isNewPost.value == true
          ? postTextData().then((value) {
              if (imageFiles != []) {
                putImageData(id: value).then((_) => backToHome());
              } else {
                backToHome();
              }
            })
          : putTextData().then((value) {
              if (imageFiles != []) {
                putImageData(id: value).then((_) => backToHome());
              } else {
                backToHome();
              }
            });
    }
  }

  ///Show success snackbar and get to [ControllerPage]
  backToHome() {
    Get.back();
    Get.snackbar("Thành công", "Đăng bài viết thành công!",
        colorText: Colors.green.shade800,
        backgroundColor: Colors.white,
        icon: Icon(
          Icons.warning,
          color: Colors.green.shade800,
        ));
    Get.off(() => const ControllerPage(index: 0), preventDuplicates: false);
  }

  ///Update image data of post
  ///
  ///Send a put image data request with param
  ///[id], [token], [imageFiles]
  ///Show warning snackbar if fails
  putImageData({required int id}) async {
    dio.Response response;
    try {
      response = await http.putImageNewPostRequest(
        endPoint: "api/Posts/UpLoadImages?id=$id",
        token: token,
        images: imageFiles,
      );
      if (response.statusCode == 200) {
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Post new text data of post
  ///
  ///Send a post text data request to create
  ///text data of post with param [subjectController],
  ///[teacherController], [schoolListId], [facultyListId],
  ///[currentSchool], [currentFaculty], [majorController],
  ///[cirriculumList], [hardRate], [likeRate], [examRate], [userId]
  ///server. Show warning snackbar if fails
  postTextData() async {
    dio.Response response;
    try {
      response = await http.postTextNewPostRequest(
        endPoint: "api/Posts",
        token: token,
        subject: subjectController.value.text.trim(),
        teacher: teacherController.value.text.trim(),
        universityId: schoolListId.value[currentSchool] ?? 0,
        facultyId: facultyListId.value[currentFaculty] ?? 0,
        major: majorController.value.text.trim(),
        document: cirriculumList.join(','),
        rateHard: hardRate.value,
        reviewHard: hardRateReasonController.value.text.trim(),
        rateLike: likeRate.value,
        reviewLike: likeRateReasonController.value.text.trim(),
        rateExam: examRate.value,
        reviewExam: examRateReasonController.value.text.trim(),
        userId: userId,
      );
      if (response.statusCode == 200) {
        return response.data["id"];
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Get images from device
  ///
  ///Get images from device and add to
  ///[imageFiles], Show warning snackbar if
  ///length > 5
  getImageFromGallery() async {
    // ignore: deprecated_member_use
    PickedFile? pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    if (pickedFile != null) {
      if (imageFiles.length < 5) {
        imageFiles.add(File(pickedFile.path));
      } else {
        Get.snackbar("Cảnh báo", "Tối đa 5 hình ảnh!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
      }
    }
  }

  ///Remove image from [imageFiles] at [id]
  removeImage(int id) {
    imageFiles.removeAt(id);
  }

  ///Add [data] to [cirriculumList]
  addCirriculum(String data) {
    cirriculumList.value.add(data);
  }

  ///Add data from [cirriculumList] at [id]
  removeCirriculum(int id) {
    cirriculumList.value.removeAt(id);
  }
}
