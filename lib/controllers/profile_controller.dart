// ignore_for_file: unnecessary_cast

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:review_subject/controllers/home_controller.dart';
import 'package:review_subject/controllers/my_shared_preference.dart';
import 'package:review_subject/models/profile_model.dart';
import 'package:review_subject/service/service.dart';
import 'package:dio/dio.dart' as dio;

class ProfileController extends GetxController {
  RxString avatar =
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqn6tPELJgjHUY9qp5OF9l_K0KAzrWeru6VA&usqp=CAU"
          .obs;
  RxInt id = 0.obs;
  Rx<Profile?> profile = Rx<Profile?>(null as Profile?);
  String token = "";
  int userId = 0;

  RxBool isUpdated = false.obs;
  Rx<File> rxAvatar = File('').obs;
  HomeController homeController = Get.put(HomeController());

  HttpService http = HttpService();

  ///Get image from gallery of device,
  ///return [File]
  Future<File> getImageFromGallery() async {
    // ignore: deprecated_member_use
    PickedFile? pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      maxWidth: 400,
      maxHeight: 400,
    );
    return File(pickedFile!.path);
  }

  ///Allow user to pick and update [rxAvatar]
  ///with image picked from gallery
  onNewAvatarClick({required BuildContext context}) async {
    int userId = await MySharedPreference.getUserId();
    String userToken = await MySharedPreference.getToken();

    File pickedImage = await getImageFromGallery();
    rxAvatar.value = pickedImage;

    sendNewAvatarRequest(
        context: context, id: userId, token: userToken, image: pickedImage);
  }

  ///Update avatar to server
  ///
  ///Send a put new avatar request to server with
  ///param [id], [token], [image]. Show warning snackbar
  ///if fails, else show success snackbar
  Future<void> sendNewAvatarRequest(
      {required BuildContext context,
      required int id,
      required String token,
      required File image}) async {
    dio.Response response;
    try {
      response = await http.putNewAvatarRequest(
          endPoint: "api/Users/NewAvatar?id=$id", token: token, image: image);
      Get.back();
      if (response.statusCode == 200) {
        ///Update userAvatar in local storage
        MySharedPreference.setUserAvatar(avatar: response.data).then((_) {
          homeController.userAvatar = response.data;
          homeController.ressetHomeData();
          homeController.loadDataFromLocal();
          Get.snackbar("", "Cập nhật avatar thành công",
              colorText: Colors.green,
              backgroundColor: Colors.white,
              icon: const Icon(
                Icons.warning,
                color: Colors.green,
              ));
        });
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nốiiii!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Load [token] and [userId] from local
  loadDataFromLocal() async {
    token = await MySharedPreference.getToken();
    userId = await MySharedPreference.getUserId();
  }

  ///Load profile data by [id]
  ///
  ///Send a get profile request with [profileId],
  ///[userId]. Show warning snackbar if fails
  ///else change [profile] with data
  loadProfileById({required int profileId}) async {
    dio.Response response;
    try {
      response = await http.getProfileByIdRequest(
          endPoint: "api/Profile/profileId=$profileId?userId=$userId",
          token: token);
      if (response.statusCode == 200) {
        profile.value = profileFromJson(response.data);
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }
}
