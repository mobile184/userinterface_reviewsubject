// ignore_for_file: invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/show_dialog.dart';
import 'package:review_subject/controllers/my_shared_preference.dart';
import 'package:review_subject/models/faculty_model.dart';
import 'package:review_subject/models/list_subject_model.dart';
import 'package:dio/dio.dart' as dio;
import 'package:review_subject/models/sign_up_model.dart';
import 'package:review_subject/service/service.dart';

import '../models/list_post_model.dart';

class SearchSubjectController extends GetxController
    with GetTickerProviderStateMixin {
  String token = "";
  int userId = 0;
  int page = 1;

  Rx<TextEditingController> searchController = TextEditingController().obs;
  RxString searchKey = "".obs;
  RxList<String> schoolList = <String>["-- Chọn trường"].obs;
  RxMap<String, int> schoolListId = {"-- Chọn trường": 0}.obs;
  RxList<String> facultyList = <String>["-- Chọn khoa"].obs;
  RxMap<String, int> facultyListId = {"-- Chọn khoa": 0}.obs;
  RxString currentSchool = "-- Chọn trường".obs;
  RxString currentFaculty = "-- Chọn khoa".obs;

  RxBool isLoading = false.obs;
  RxList<PostResponse> listSubject = <PostResponse>[].obs;
  HttpService http = HttpService();
  String userAvatar = "";
  String userFullname = "";
  RxBool isExpanded = false.obs;
  late AnimationController animationController;

  ///Load data when controller is initialized
  @override
  void onInit() {
    super.onInit();
    loadDataFromLocal().then((_) => loadMore());
    animationController =
        AnimationController(vsync: this, duration: const Duration(seconds: 1));
  }

  ///Send like or dislike request to server
  ///
  ///Check [isLiked]. If true send post like request
  ///else delete like request to server. Show warning snackbar
  ///if fails
  onLikedButtonPress({required int postId, required bool isLiked}) async {
    dio.Response response;
    try {
      if (isLiked == true) {
        response = await http.postLikeRequest(
          endPoint: "api/Likes",
          token: token,
          postId: postId,
          userId: userId,
        );
      } else {
        response = await http.deleteLikeRequest(
            endPoint: "api/Likes?postId=$postId&userId=$userId", token: token);
      }
      if (response.statusCode == 200) {
        listSubject.refresh();
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Reset data of page
  resetSearchSubjectData() {
    page = 1;
    listSubject.clear();
    onSearchWithKey();
  }

  ///Load data from local storage
  loadDataFromLocal() async {
    token = await MySharedPreference.getToken();
    userId = await MySharedPreference.getUserId();
    userAvatar = await MySharedPreference.getUserAvatar();
    userFullname = await MySharedPreference.getUserFullname();
  }

  ///Load universities from server. Show warning snackbar if fails
  ///else set data to [schoolList] and [schoolListId]
  Future<void> loadData({required BuildContext context}) async {
    dio.Response response;
    try {
      response = await http.getDataRequest(endPoint: "api/Universities");
      if (response.statusCode == 200) {
        dynamic data = signUpResponseFromJson(response.data);

        schoolList.value = ["-- Chọn trường"];
        schoolList.value.addAll(
            data.map<String>((SignUpResponse data) => data.name).toList());

        schoolListId["-- Chọn trường"] = 0;
        for (int i = 0; i < schoolList.length - 1; i++) {
          schoolListId[data[i].name] = data[i].id;
        }
        currentSchool.value = schoolList.value[0];
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");
        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Load faculties from server by [id]. Show warning snackbar if fails
  ///else set data to [facultyList] and [facultyListId]
  Future<void> loadFacultyBySchoolId(
      {required BuildContext context, required int id}) async {
    dio.Response response;
    try {
      showLoaderDialog(context);
      response = await http.getFacultyListRequest(
          endPoint: "api/Faculties/ByUniversityId?universityId=$id");
      Get.back();
      if (response.statusCode == 200) {
        List<Faculty> data = facultyResponseFromJson(response.data);

        facultyList.value = ["-- Chọn khoa"];
        facultyList.value
            .addAll(data.map<String>((Faculty data) => data.name).toList());

        facultyListId["-- Chọn khoa"] = 0;
        for (int i = 0; i < facultyList.length - 1; i++) {
          facultyListId[data[i].name] = data[i].id;
        }
        currentFaculty.value = facultyList.value[0];
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");
        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Search posts with [searchController] key
  ///
  ///Send get request to server.
  ///Show warning snackbar if fails, else
  ///change [listSubject] data
  onSearchWithKey() async {
    dio.Response response;
    int idUni = schoolListId.value[currentSchool.value]!;
    int idFaculty = 0;
    if (idUni != 0) {
      idFaculty = facultyListId[currentFaculty.value]!;
    }
    try {
      isLoading.value = true;
      response = await http.getSubjectBySearchKeyRequest(
          endPoint:
              "api/Search?universityId=$idUni&facultyId=$idFaculty&keySearch=${searchController.value.text}&page=$page&userId=$userId",
          token: token);
      isLoading.value = false;
      if (response.statusCode == 200) {
        List<PostResponse> newListSubject =
            listSubjectResponseFromJson(response.data);
        listSubject.clear();
        listSubject.addAll(newListSubject);
        listSubject.refresh();
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối tìm kiếm!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Load more posts if exists
  ///
  ///Send get subject with param [searchController],
  ///[page], [userId], [token] if [searchController]
  ///is not null. Show warning snackbar if fails,
  ///else add data to [listSubject]
  loadMore() async {
    dio.Response response;
    if (searchController.value.text != "") {
      try {
        isLoading.value = true;
        page++;
        response = await http.getSubjectBySearchKeyRequest(
            endPoint:
                "Search?universityId=0&facultyId=0&keySearch=${searchController.value.text}&page=$page&userId=$userId",
            token: token);
        isLoading.value = false;
        if (response.statusCode == 200) {
          List<PostResponse> newListSubject =
              listSubjectResponseFromJson(response.data);
          if (newListSubject.isEmpty) page--;
          listSubject.addAll(newListSubject);
        } else {
          Get.back();
          Get.snackbar("Cảnh báo", "Lỗi kết nối!",
              colorText: Colors.yellow.shade800,
              backgroundColor: Colors.white,
              icon: Icon(
                Icons.warning,
                color: Colors.yellow.shade800,
              ));
          // print("There is some problem status code ${response.statusCode}");

          return response.data;
        }
      } on Exception {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối tìm kiếm!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print(e);
      }
    }
  }
}
