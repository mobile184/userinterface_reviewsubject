import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/controllers/my_shared_preference.dart';
import 'package:review_subject/models/list_post_by_faculty_model.dart';
import 'package:review_subject/models/list_post_model.dart';
import 'package:review_subject/models/school_model.dart';
import 'package:dio/dio.dart' as dio;
import 'package:review_subject/models/top_user_model.dart';
import '../service/service.dart';

class HomeController extends GetxController {
  Rx<TextEditingController> textSearchSubjectController =
      TextEditingController().obs;
  RxList<TopUser> topUsers = <TopUser>[].obs;
  RxList<PostResponse> listPostResponse = <PostResponse>[].obs;
  RxList<FacultyResponse> listFacultyResponse = <FacultyResponse>[].obs;
  String token = "";
  int userId = 0;
  String userFullname = "";
  int schoolId = 0;
  int page = 1;
  String userAvatar = "";
  RxInt notiNumber = 0.obs;
  HttpService http = HttpService();

  ///Display current school use to search
  ///data for page
  Rx<SchoolModel> displayedUni = SchoolModel(
          name: "",
          address: "",
          avatar: "",
          id: 0,
          status: 0,
          created: DateTime.now(),
          updated: DateTime.now())
      .obs;

  ///Reset data of [HomePage]
  ///
  ///Reset data to default
  ///when method called
  ressetHomeData() {
    listFacultyResponse = <FacultyResponse>[].obs;
    page = 1;
    loadTopUser();
    loadPostFromPage();
    loadRelatedPost();
  }

  ///Load data from local storage
  ///
  ///Set value for [userId], [displayedUni],
  ///[token], [userAvatar], [userFullname] by data saved in
  ///local storage
  loadDataFromLocal() async {
    userId = await MySharedPreference.getUserId();
    if (displayedUni.value.name == '') {
      displayedUni.value = await MySharedPreference.getUniversity();
      token = await MySharedPreference.getToken();
      userAvatar = await MySharedPreference.getUserAvatar();
      userFullname = await MySharedPreference.getUserFullname();
    }
  }

  ///Load data for top user section
  ///
  ///Send a get top users request to
  ///server. Show warning snackbar if fails,
  ///else reload [topUsers] with data
  loadTopUser() async {
    dio.Response response;
    try {
      response = await http.getTopUserRequest(
          endPoint: "api/Home/TopUser", token: token);
      if (response.statusCode == 200) {
        topUsers.value = topUserFromJson(response.data);
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Load data for related posts section
  ///
  ///Send a get related posts request to
  ///server. Show warning snackbar if fails,
  ///else reload [listPostResponse] with data
  loadRelatedPost() async {
    dio.Response response;
    try {
      response = await http.getTopUserRequest(
          endPoint:
              "api/Home/RelatedPost?universityId=${displayedUni.value.id}&userId=$userId",
          token: token);
      if (response.statusCode == 200) {
        listPostResponse.value = listPostResponseFromJson(response.data);
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Load data for faculty posts section
  ///
  ///Send a get faculty posts by [page] request to
  ///server. Show warning snackbar if fails,
  ///else add new data to [listFacultyResponse] with data
  loadPostFromPage() async {
    dio.Response response;
    try {
      response = await http.getTopUserRequest(
          endPoint:
              "api/Home/ListPostFaculty?universityId=${displayedUni.value.id}&page=$page&userId=$userId",
          token: token);
      if (response.statusCode == 200) {
        List<FacultyResponse> newListFaculty =
            listPostByFacultyFromJson(response.data);
        if (newListFaculty.isNotEmpty) {
          // ignore: invalid_use_of_protected_member
          listFacultyResponse.value.addAll(newListFaculty);
          listFacultyResponse.refresh();
          page++;
        }
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }
}
