import 'dart:convert';

import 'package:review_subject/models/school_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

///Manage data on local storage
class MySharedPreference {
  /// Get [idUser] saved from local storage
  static Future getUserId() async {
    final prefs = await SharedPreferences.getInstance();
    int? value = prefs.getInt("idUser");
    return value;
  }

  /// Set [idUser] value to local storage
  ///
  /// Set [idUser] variable of local storage by
  /// value of param [idUser]
  static setUserId({required int idUser}) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt("idUser", idUser);
  }

  /// Get [token] saved from local storage
  ///
  /// Get a value named [token] saved
  /// in local storage
  static Future getToken() async {
    final prefs = await SharedPreferences.getInstance();
    String? value = prefs.getString("token");
    return value;
  }

  /// Set [token] value to local storage
  ///
  /// Set [token] variable of local storage by
  /// value of param [token]
  static setToken({required String token}) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString("token", token);
  }

  /// Get [userAvatar] saved from local storage
  ///
  /// Get a value named [userAvatar] saved
  /// in local storage
  static Future getUserAvatar() async {
    final prefs = await SharedPreferences.getInstance();
    String? value = prefs.getString("userAvatar");
    return value ?? "";
  }

  /// Set [userAvatar] value to local storage
  ///
  /// Set [userAvatar] variable of local storage by
  /// value of param [avatar]
  static setUserAvatar({required String avatar}) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString("userAvatar", avatar);
  }

  /// Get [userFullname] saved from local storage
  ///
  /// Get a value named [userFullname] saved
  /// in local storage
  static Future getUserFullname() async {
    final prefs = await SharedPreferences.getInstance();
    String value = prefs.getString("userFullname")!;
    return value;
  }

  /// Set [userFullname] value to local storage
  ///
  /// Set [userFullname] variable of local storage by
  /// value of param [userFullname]
  static setUserFullname({required String userFullname}) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString("userFullname", userFullname);
  }

  /// Get [university] saved from local storage
  ///
  /// Get a value named [university] saved
  /// in local storage
  static Future getUniversity() async {
    final prefs = await SharedPreferences.getInstance();
    SchoolModel value =
        SchoolModel.fromJson(json.decode(prefs.getString("university")!));
    return value;
  }

  /// Set [university] value to local storage
  ///
  /// Set [university] variable of local storage by
  /// value of param [university]
  static setUniversity({required String university}) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString("university", university);
  }

  ///Clear all data saved in local storage
  static Future clearData() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  ///Check if value named [key] exists in local storage
  /// using param [key]
  static Future containKey(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.containsKey(key);
  }
}
