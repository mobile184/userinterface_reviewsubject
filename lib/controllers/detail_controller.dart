import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/controllers/history_posts_controller.dart';
import 'package:review_subject/models/list_comment_model.dart';
import 'package:review_subject/models/list_post_model.dart';
import 'package:dio/dio.dart' as dio;
import 'package:review_subject/service/service.dart';

class DetailController extends GetxController {
  RxInt curSlider = 0.obs;
  Rx<TextEditingController> commentTextController = TextEditingController().obs;
  RxBool emojiIsShowing = false.obs;
  RxBool canLoadMoreComment = true.obs;
  RxBool isPostingComment = false.obs;
  // ignore: unnecessary_cast
  Rx<PostResponse?> postData = (null as PostResponse?).obs;
  RxList<String> listCurriculum = <String>[].obs;
  int userId = 0;
  int postId = 0;
  String userFullname = "";
  String token = "";
  String userAvatar = "";
  RxInt page = 1.obs;
  RxMap<int, String> intToStringTimeDiffMap =
      {1: "phút trước", 2: "giờ trước", 3: "ngày trước"}.obs;
  RxList<CommentResponse> listComment = <CommentResponse>[].obs;
  HttpService http = HttpService();

  Rx<FocusNode> commentFocusNode = FocusNode().obs;

  ///Initialized [historyPostController]
  HistoryPostController historyPostController =
      Get.put(HistoryPostController());

  ///Get data when controller initialized
  ///
  ///Get data in arguments and load data
  ///of the page
  @override
  void onInit() {
    super.onInit();
    userId = Get.arguments["userId"];
    token = Get.arguments["token"];
    postId = Get.arguments["postId"];
    userFullname = Get.arguments["userFullname"];
    userAvatar = Get.arguments["userAvatar"];
    commentFocusNode.value.addListener(() {
      if (commentFocusNode.value.hasFocus) {
        emojiIsShowing.value = false;
      }
    });
    loadPostData().then((_) {
      loadCommentByPage();
    });
  }

  ///Delete current post
  ///
  ///Send a delete request to server.
  ///Show warning snackbar if
  ///cannot delete post. Show
  ///success snackbar and get back
  ///if delete success
  deletePost() async {
    dio.Response response;
    try {
      response = await http.deletePostRequest(
          endPoint: "api/Posts/${postData.value!.post.id}", token: token);
      if (response.statusCode == 200) {
        Get.snackbar("Xóa bài viết", "Xóa thành công!",
            colorText: Colors.green,
            backgroundColor: Colors.white,
            icon: const Icon(
              Icons.warning,
              color: Colors.green,
            ));
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception catch (e) {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi $e",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
    }
  }

  ///Delete current user's comment
  ///
  ///Send a delete request by [id] which is
  ///userId to server.
  ///Show warning snackbar if fail to delete
  ///Show success snackbar if success and reload page
  deleteCommentById({required int id}) async {
    dio.Response response;
    try {
      response = await http.deleteCommentRequest(
          endPoint: "api/Comments/${listComment[id].comment.id}", token: token);
      if (response.statusCode == 200) {
        listComment.removeAt(id);
        loadPostData();
        Get.snackbar("Xóa bình luận", "Xóa thành công!",
            colorText: Colors.green,
            backgroundColor: Colors.white,
            icon: const Icon(
              Icons.warning,
              color: Colors.green,
            ));
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception catch (e) {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi $e",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
    }
  }

  ///Confirm delete request from user
  ///
  ///Show a confirm dialog with [context] to user
  ///If they choose Ok button, call
  ///[deleteCommentById] with param [id] else return
  void showDialogDeleteComment(
      {required context,
      required DetailController controller,
      required int id}) {
    AwesomeDialog(
      context: context,
      // ignore: deprecated_member_use
      animType: AnimType.SCALE,
      dialogType: DialogType.error,
      btnOkColor: Colors.red,
      // btnOkColor: Colors.green,
      btnCancelColor: Colors.grey,
      body: Center(
        child: Column(
          children: const [
            SizedBox(height: 10),
            Text(
              'Xóa bình luận?',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
            ),
            SizedBox(height: 7),
            Text('Bạn có chắc chắn muốn xóa?')
          ],
        ),
      ),
      title: 'This is Ignored',
      desc: 'This is also Ignored',
      btnOkOnPress: () {
        controller.deleteCommentById(id: id);
      },
      btnCancelOnPress: () {},
    ).show();
  }

  ///Load data of post except for comments
  ///
  ///Send get post request to server. Show
  ///snackbar if request fails
  loadPostData() async {
    dio.Response response;
    try {
      response = await http.getTopUserRequest(
          endPoint: "api/PostDetail?postId=$postId&userId=$userId",
          token: token);
      if (response.statusCode == 200) {
        postData.value = PostResponse.fromJson(response.data);
        listCurriculum.value = postData.value!.post.document.split(",");
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Load comments by [page]
  ///
  ///Send get comments request to
  ///server. Show warning snackbar
  ///if fails or add data to [listComment]
  ///if success
  loadCommentByPage() async {
    dio.Response response;
    try {
      response = await http.getTopUserRequest(
          endPoint:
              "api/PostDetail/GetComments?postId=${postData.value!.post.id}&page=${page.value}",
          token: token);
      if (response.statusCode == 200) {
        List<CommentResponse> newListComment =
            listCommentResponseFromJson(response.data);
        if (newListComment.isNotEmpty) {
          // ignore: invalid_use_of_protected_member
          listComment.value.addAll(newListComment);
          canLoadMoreComment.value = true;
          listComment.refresh();
          page++;
        } else {
          canLoadMoreComment.value = false;
        }
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception catch (e) {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi $e",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
    }
  }

  ///Press on like button
  ///
  ///Solve on like button click.
  ///Send a like request to server
  ///Show warning snackbar if like
  ///request fails, else refresh data
  onLikeButtonPress() async {
    postData.value!.isLiked = !postData.value!.isLiked;
    postData.value!.isLiked == true
        ? postData.value!.post.likeCount++
        : postData.value!.post.likeCount--;
    postData.refresh();
    dio.Response response;
    try {
      if (postData.value!.isLiked == true) {
        response = await http.postLikeRequest(
          endPoint: "api/Likes",
          token: token,
          postId: postData.value!.post.id,
          userId: userId,
        );
      } else {
        response = await http.deleteLikeRequest(
            endPoint:
                "api/Likes?postId=${postData.value!.post.id}&userId=$userId",
            token: token);
      }
      if (response.statusCode == 200) {
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception catch (e) {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi $e",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
    }
  }

  ///Change slider index to display images
  changeSlider(int index) {
    curSlider.value = index;
  }

  onPostCommentButtonPress() async {
    dio.Response response;
    if (commentTextController.value.text != "") {
      try {
        isPostingComment.value = true;
        response = await http.postNewCommentRequest(
          endPoint: "api/Comments",
          token: token,
          content: commentTextController.value.text,
          postId: postData.value!.post.id,
          userId: userId,
        );
        isPostingComment.value = false;
        commentTextController.value.text = "";
        if (response.statusCode == 200) {
          // print('userAvatar: ' +
          //     userAvatar +
          //     ' - ' +
          //     'userFullname: ' +
          //     userFullname);
          loadPostData();
          // ignore: invalid_use_of_protected_member
          listComment.value.isEmpty
              ? listComment.value = [
                  CommentResponse(
                      comment: Comment.fromJson(response.data),
                      userFullname: userFullname,
                      userAvatar: userAvatar,
                      countTime: 0,
                      typeTime: 1)
                ]
              // ignore: invalid_use_of_protected_member
              : listComment.value.insert(
                  0,
                  CommentResponse(
                      comment: Comment.fromJson(response.data),
                      userFullname: userFullname,
                      userAvatar: userAvatar,
                      countTime: 0,
                      typeTime: 1));
          listComment.refresh();
        } else {
          Get.back();
          Get.snackbar("Cảnh báo", "Lỗi kết nối!",
              colorText: Colors.yellow.shade800,
              backgroundColor: Colors.white,
              icon: Icon(
                Icons.warning,
                color: Colors.yellow.shade800,
              ));
          // print("There is some problem status code ${response.statusCode}");

          return response.data;
        }
      } on Exception catch (e) {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi $e",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
      }
    }
  }
}
