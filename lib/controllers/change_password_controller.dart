import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/show_dialog.dart';
import 'package:review_subject/controllers/my_shared_preference.dart';
import 'package:review_subject/route_managements/routes.dart';
import 'package:review_subject/service/service.dart';
import 'package:dio/dio.dart' as dio;

class ChangePasswordController extends GetxController {
  Rx<TextEditingController> userNameTextController =
      TextEditingController().obs;
  Rx<TextEditingController> passwordTextController =
      TextEditingController().obs;
  Rx<TextEditingController> confirmPasswordTextController =
      TextEditingController().obs;

  RxBool isValidUserName = true.obs;
  RxBool isValidPassword = true.obs;
  RxBool isValidConfirmPassword = true.obs;

  RxBool isPressedRecoverPassword = false.obs;
  late HttpService http = HttpService();

  ///Check if [userNameTextController] is valid or not
  ///
  ///Returns void and change data of [isValidUserName]
  ///to true if satisfied and false if not
  checkValidUserName(String data) {
    data != "" ? isValidUserName.value = true : isValidUserName.value = false;
  }

  ///Check if [passwordTextController] is valid or not
  ///
  ///Returns void and change data of [isValidPassword]
  ///to true if satisfied and false if not
  checkValidPassword(String data) {
    data != "" ? isValidPassword.value = true : isValidPassword.value = false;
  }

  ///Send a change password request to server
  ///
  ///Throws exception and show snackbar if
  ///cannot connect to server.
  ///Returns void and send request to server
  ///with param [context], [id], [token],
  ///[username], [password]. If success,
  ///return to [kSignInPage], else warning with
  ///a snackbar
  Future<void> sendChangePasswordRequest(
      {required BuildContext context,
      required int id,
      required String token,
      required String username,
      required String password}) async {
    dio.Response response;
    try {
      showLoaderDialog(context);
      response = await http.putChangePasswordRequest(
          endPoint: "api/Users/NewPassword?id=$id",
          token: token,
          userName: username,
          password: password);
      Get.back();
      if (response.statusCode == 200) {
        if (response.data == true) {
          Get.snackbar("", "Thay đổi mật khẩu thành công",
              colorText: Colors.green,
              backgroundColor: Colors.white,
              icon: const Icon(
                Icons.warning,
                color: Colors.green,
              ));
          Get.toNamed(kSignInPage, arguments: []);
        } else {
          Get.snackbar("Cảnh báo", "Lỗi kết nối!",
              colorText: Colors.yellow.shade800,
              backgroundColor: Colors.white,
              icon: Icon(
                Icons.warning,
                color: Colors.yellow.shade800,
              ));
          return response.data;
        }
      }
    } on Exception catch (e) {
      Get.snackbar("Cảnh báo", "Lỗi $e",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
    }
  }

  ///Click on Ok button
  ///
  ///Returns void and check valid
  ///of [username], [password],
  ///Show snackbar with [context]
  ///if condition not satisfied
  onOkButtonClick(
      {required BuildContext context,
      required String username,
      required String password}) async {
    int userId = await MySharedPreference.getUserId();
    String userToken = await MySharedPreference.getToken();
    if ([username, password].any((element) => element == '')) {
      Get.snackbar("Cảnh báo", "Tài khoản hoặc mật khẩu không hợp lệ!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
    } else {
      sendChangePasswordRequest(
          context: context,
          id: userId,
          token: userToken,
          username: username,
          password: password);
    }
  }
}
