// ignore_for_file: invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/models/sign_up_model.dart';
import 'package:dio/dio.dart' as dio;
import 'package:review_subject/route_managements/routes.dart';
import '../components/show_dialog.dart';
import '../models/faculty_model.dart';
import '../service/service.dart';

class SignUpController extends GetxController {
  RxList<String> schoolList = <String>["Chọn trường"].obs;
  RxMap<String, int> schoolListId = {"": 0}.obs;
  RxList<String> facultyList = <String>["Hãy chọn trường trước"].obs;
  RxMap<String, int> facultyListId = {"": 0}.obs;
  RxString currentSchool = "Chọn trường".obs;
  RxString currentFaculty = "Hãy chọn trường trước".obs;
  Rx<TextEditingController> userNameTextController =
      TextEditingController().obs;
  Rx<TextEditingController> passwordTextController =
      TextEditingController().obs;
  Rx<TextEditingController> passwordConfirmTextController =
      TextEditingController().obs;
  Rx<TextEditingController> fullNameTextController =
      TextEditingController().obs;
  Rx<TextEditingController> emailTextController = TextEditingController().obs;
  Rx<TextEditingController> majorTextController = TextEditingController().obs;

  RxBool isValidUserName = true.obs;
  RxBool isValidPassword = true.obs;
  RxBool isValidPasswordConfirm = true.obs;
  RxBool isValidFullName = true.obs;
  RxBool isValidEmail = true.obs;
  RxBool isValidFaculty = true.obs;
  RxBool isPassWordVisible = false.obs;
  RxBool isConfirmPassWordVisible = false.obs;

  late HttpService http = HttpService();

  ///Check [userNameTextController] 
  ///If valid set [isValidUserName] true
  ///else set false
  checkValidUserName() {
    userNameTextController.value.text == ""
        ? isValidUserName.value = false
        : isValidUserName.value = true;
  }

  ///Check [passwordTextController] 
  ///If valid set [isValidPassword] true
  ///else set false
  checkValidPassword() {
    passwordTextController.value.text == ""
        ? isValidPassword.value = false
        : isValidPassword.value = true;
  }

  ///Check [passwordConfirmTextController] 
  ///If valid set [isValidPasswordConfirm] true
  ///else set false
  checkValidPasswordConfirm() {
    passwordConfirmTextController.value.text != "" &&
            passwordTextController.value.text ==
                passwordConfirmTextController.value.text
        ? isValidPasswordConfirm.value = true
        : isValidPasswordConfirm.value = false;
  }

  ///Check [fullNameTextController] 
  ///If valid set [isValidFullName] true
  ///else set false
  checkValidFullName() {
    fullNameTextController.value.text == ""
        ? isValidFullName.value = false
        : isValidFullName.value = true;
  }

  ///Check [fullNameTextController] 
  ///If valid set [isValidFullName] true
  ///else set false
  checkValidFaculty() {
    currentFaculty.value == "Hãy chọn trường trước"
        ? isValidFaculty.value = false
        : isValidFaculty.value = true;
  }

  ///Check [emailTextController] 
  ///If valid set [isValidEmail] true
  ///else set false
  checkValidEmail() {
    if (emailTextController.value.text.trim() == '') {
      isValidEmail.value = true;
    } else {
      if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
              .hasMatch(emailTextController.value.text.trim()) ==
          false) isValidEmail.value = false;
    }
  }

  ///Load universities list from server
  ///
  ///Send get universities request. If fails
  ///show warning snackbar, else set [schoolList]
  ///and [schoolListId]
  Future<void> loadData({required BuildContext context}) async {
    dio.Response response;
    try {
      showLoaderDialog(context);
      response =
          await http.getDataRequest(endPoint: "api/Universities");
      Get.back();
      if (response.statusCode == 200) {
        dynamic data = signUpResponseFromJson(response.data);
        schoolList.value =
            data.map<String>((SignUpResponse data) => data.name).toList();
        for (int i = 0; i < schoolList.length; i++) {
          schoolListId[data[i].name] = data[i].id;
        }
        currentSchool.value = schoolList.value[0];
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");
        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Load faculties list from server
  ///
  ///Send get faculties request. If fails
  ///show warning snackbar, else set [facultyList]
  ///and [facultyListId]
  Future<void> loadFacultyBySchoolId(
      {required BuildContext context, required int id}) async {
    dio.Response response;
    try {
      showLoaderDialog(context);
      response = await http.getFacultyListRequest(
          endPoint: "api/Faculties/ByUniversityId?universityId=$id");
      Get.back();
      if (response.statusCode == 200) {
        List<Faculty> data = facultyResponseFromJson(response.data);
        facultyList.value =
            data.map<String>((Faculty data) => data.name).toList();
        for (int i = 0; i < facultyList.length; i++) {
          facultyListId[facultyList.value[i]] = data[i].id;
        }
        currentFaculty.value = facultyList.value[0];
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");
        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Post sign up request to server
  ///
  ///Send post sign up request to server
  ///Show warning snackbar if fails, else
  ///show success snackbar and get to
  ///[kSignInPage]
  Future<void> signUp({required BuildContext context}) async {
    dio.Response response;
    try {
      showLoaderDialog(context);
      response = await http.postSignUpRequest(
          endPoint: "api/Login/Register",
          userName: userNameTextController.value.text,
          password: passwordTextController.value.text,
          fullName: fullNameTextController.value.text,
          email: emailTextController.value.text,
          universityId: schoolListId.value[currentSchool]!,
          facultyId: facultyListId.value[currentFaculty]!,
          major: majorTextController.value.text);
      Get.back();
      if (response.statusCode == 200) {
        if (response.data["userName"] != null && response.data["id"] == 0) {
          Get.snackbar("Cảnh báo", "Tài khoản đã tồn tại!",
              colorText: Colors.yellow.shade800,
              backgroundColor: Colors.white,
              icon: Icon(
                Icons.warning,
                color: Colors.yellow.shade800,
              ));
        } else {
          Get.snackbar("Chúc mừng", "Đăng ký tài khoản thành công!",
              colorText: Colors.yellow.shade800,
              backgroundColor: Colors.white,
              icon: Icon(
                Icons.warning,
                color: Colors.yellow.shade800,
              ));
          Get.toNamed(kSignInPage);
        }
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");
        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Check condition when click sign in button
  ///
  ///Check [isValidEmail], [isValidFaculty],
  ///[isValidFullName], [isValidPassword],
  ///[isValidPasswordConfirm], [isValidUserName]
  ///if any false, show warning snackbar, else
  ///call [signUp]
  onSignUpButtonClick(BuildContext context) {
    checkValidUserName();
    checkValidPassword();
    checkValidPasswordConfirm();
    checkValidFullName();
    checkValidEmail();
    checkValidFaculty();
    if ([
      isValidEmail.value,
      isValidFaculty.value,
      isValidFullName.value,
      isValidPassword.value,
      isValidPasswordConfirm.value,
      isValidUserName.value
    ].any((element) => element == false)) {
      Get.snackbar("Cảnh báo", "Vui lòng kiểm tra lại thông tin!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
    } else {
      signUp(context: context);
    }
  }
}
