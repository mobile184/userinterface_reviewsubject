// ignore_for_file: unnecessary_cast

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/show_dialog.dart';
import 'package:review_subject/controllers/my_shared_preference.dart';
import 'package:review_subject/models/profile_model.dart';
import 'package:review_subject/service/service.dart';
import 'package:dio/dio.dart' as dio;

class ProfileUserController extends GetxController {
  Rx<Profile?> profile = Rx<Profile?>(null as Profile?);
  String token = "";
  int userId = 0;

  HttpService http = HttpService();

  ///Load [token] and [userId] from local
  loadDataFromLocal() async {
    token = await MySharedPreference.getToken();
    userId = await MySharedPreference.getUserId();
  }

  ///Load profile data by [id]
  ///
  ///Send a get profile request with [profileId],
  ///[userId]. Show warning snackbar if fails
  ///else change [profile] with data
  loadProfileById(
      {required BuildContext context, required int profileId}) async {
    dio.Response response;
    try {
      showLoaderDialog(context);
      response = await http.getProfileByIdRequest(
          endPoint: "api/Profile/profileId=$profileId?userId=$userId",
          token: token);
      Get.back();
      if (response.statusCode == 200) {
        profile.value = profileFromJson(response.data);
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }
}
