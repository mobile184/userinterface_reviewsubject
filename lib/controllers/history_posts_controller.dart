import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/controllers/my_shared_preference.dart';
import 'package:review_subject/models/list_post_model.dart';
import 'package:review_subject/service/service.dart';

class HistoryPostController extends GetxController {
  late HttpService http = HttpService();

  Rx<ScrollController> scrollController = ScrollController().obs;

  String token = "";
  int userId = 0;
  String userFullname = "";
  String userAvatar = "";

  RxInt pageNum = 1.obs;

  RxInt clientUserId = 0.obs;

  var fetchedList = <PostResponse>[].obs;
  RxBool isEnd = false.obs;

  ///Reset data of [HistoryPost]
  ressetHistoryData() {
    fetchedList.value = [];
    isEnd.value = false;
    //  isEnd = false.obs;
    pageNum.value = 1;
    getAllHistoryPost(
        displayedUserId: clientUserId.value, pageNum: pageNum.value);
  }

  ///Load data from [MySharedPreference]
  ///
  ///Load data of [userId], [token],
  ///[userAvatar], [userFullname]
  ///from [MySharedPreference]
  loadDataFromLocal() async {
    userId = await MySharedPreference.getUserId();

    token = await MySharedPreference.getToken();
    userAvatar = await MySharedPreference.getUserAvatar();
    userFullname = await MySharedPreference.getUserFullname();
  }

  ///Get posts of [HistoryPost] page
  ///
  ///Send get history posts request to
  ///server, use [sendHistoryPostRequest]
  ///with param [displayedUserId] and [pageNum]
  getAllHistoryPost(
      {
      // required BuildContext context,
      required int displayedUserId,
      required int pageNum}) async {
    // int userId = await MySharedPreference.getUserId();
    String userToken = await MySharedPreference.getToken();

    sendHistoryPostRequest(
        // context: context,
        token: userToken,
        userId: userId,
        profileId: displayedUserId,
        pageNum: pageNum);
  }

  ///Load history posts of page by
  ///[pageNum]
  ///
  ///Send get history posts request
  ///to server with param [token],
  ///[userId], [profileId], [pageNum],
  ///add data to [fetchedList] if success
  ///else show warning snackbar
  Future<void> sendHistoryPostRequest(
      {
      // required BuildContext context,
      required String token,
      required int userId,
      required int profileId,
      required int pageNum}) async {
    try {
      isEnd.value = false;
      var result = await http.getHistoryPostRequest(
          endPoint:
              "api/Profile/history=$userId?profileId=$profileId&page=$pageNum",
          token: token);

      isEnd.value = true;

      fetchedList.addAll(result);
      // Get.back();
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nốiiii!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }
}
