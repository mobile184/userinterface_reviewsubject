// ignore_for_file: unnecessary_brace_in_string_interps

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/show_dialog.dart';
import 'package:review_subject/controllers/my_shared_preference.dart';
import 'package:review_subject/models/list_post_model.dart';
import 'package:review_subject/models/notification_model_new.dart';
import 'package:review_subject/service/service.dart';
import 'package:dio/dio.dart' as dio;

class NotificationController extends GetxController {
  late HttpService http = HttpService();

  Rx<ScrollController> scrollController = ScrollController().obs;

  String token = "";
  int userId = 0;
  String userFullname = "";
  String userAvatar = "";

  RxList<ListElement> listNewRx = <ListElement>[].obs;
  RxList<ListElement> listOldRx = <ListElement>[].obs;

  /// List with 2 part [listNew] and [listOld]
  var fetchedData = NotificationModelNew(listNew: [], listOld: []).obs;

  RxBool isEnd = false.obs;

  ///Load data from local storage
  ///
  ///Set value for [userId],
  ///[token], [userAvatar], [userFullname] by data saved in
  ///local storage
  loadDataFromLocal() async {
    userId = await MySharedPreference.getUserId();
    token = await MySharedPreference.getToken();
    userAvatar = await MySharedPreference.getUserAvatar();
    userFullname = await MySharedPreference.getUserFullname();
  }

  ///Load data of post by [displayedUserId]
  ///
  ///Send a get posts request with param [postId] [displayedUserId] to
  ///server. Show warning snackbar if fails,
  ///else return data
  loadPostById(context, postId, displayedUserId) async {
    dio.Response response;
    try {
      showLoaderDialog(context);
      response = await http.getPostByIdRequest(
          endPoint: "api/PostDetail?postId=${postId}&userId=${displayedUserId}",
          token: token);
      Get.back();
      if (response.statusCode == 200) {
        PostResponse postResponse = PostResponse.fromJson(response.data);
        return postResponse;
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");

        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Set data of [fetchedData] to [listNewRx], [listOldRx]
  getListElements() {
    listNewRx.clear();
    listOldRx.clear();
    listNewRx.addAll(fetchedData.value.listNew);
    listOldRx.addAll(fetchedData.value.listOld);
  }

  ///Load list notifications by [pageNum]
  ///
  ///Send a get notifications request to
  ///server using [sendNotificationsRequest] method
  ///with param [userId], [userToken], [pageNum]
  getAllNotifications(
      {required BuildContext context, required int pageNum}) async {
    int userId = await MySharedPreference.getUserId();
    String userToken = await MySharedPreference.getToken();
    sendNotificationsRequest(
        context: context, token: userToken, userId: userId, pageNum: pageNum);
  }

  ///Send get notifications request
  ///
  ///Send a get notifications request to
  ///server with param [userId], [userToken], [pageNum]
  Future<void> sendNotificationsRequest(
      {required BuildContext context,
      required String token,
      required int userId,
      required int pageNum}) async {
    try {
      isEnd.value = false;
      var result = await http.getNotificationsRequest(
          endPoint: "api/Notifications?userId=${userId}&page=${pageNum}",
          token: token);
      isEnd.value = true;
      var listNewTemp = fetchedData.value.listNew;
      var listOldTemp = fetchedData.value.listOld;
      listNewTemp.addAll(result.listNew);
      listOldTemp.addAll(result.listOld);
      fetchedData.value =
          NotificationModelNew(listNew: listNewTemp, listOld: listOldTemp);
      getListElements();
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nốiiii!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Update list with read notifications
  ///
  ///Call [sendReadNotificationRequest] with
  ///param [userToken], [notificationId], [context]
  updateReadNotifications(
      {required BuildContext context, required int notificationId}) async {
    String userToken = await MySharedPreference.getToken();
    sendReadNotificationRequest(
        context: context, token: userToken, notificationId: notificationId);
  }

  ///Send get read notification request to server
  ///
  ///Send a get read notifications request to
  ///server with param [userToken], [notificationId], [context]
  Future<void> sendReadNotificationRequest(
      {required BuildContext context,
      required String token,
      required int notificationId}) async {
    dio.Response response;
    try {
      // showLoaderDialog(context);
      response = await http.putReadNotificationRequest(
          endPoint: "api/Notifications/Readed?id=${notificationId}",
          token: token);
      // Get.back();
      if (response.statusCode == 200) {
        if (response.data == true) {
          Get.snackbar("", "Đã đọc",
              colorText: Colors.green,
              backgroundColor: Colors.white,
              icon: const Icon(
                Icons.warning,
                color: Colors.green,
              ));

          Get.back();
        } else {
          Get.back();
          Get.snackbar("Cảnh báo", "Lỗi kết nối!",
              colorText: Colors.yellow.shade800,
              backgroundColor: Colors.white,
              icon: Icon(
                Icons.warning,
                color: Colors.yellow.shade800,
              ));
          // print("There is some problem status code ${response.statusCode}");
          return response.data;
        }
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nốiiii!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }
}
