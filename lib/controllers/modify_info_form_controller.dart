import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/show_dialog.dart';
import 'package:review_subject/controllers/home_controller.dart';
import 'package:review_subject/controllers/my_shared_preference.dart';
import 'package:review_subject/controllers/profile_controller.dart';
import 'package:review_subject/models/faculty_model.dart';
import 'package:review_subject/models/profile_model.dart';
import 'package:review_subject/models/school_model.dart';
import 'package:review_subject/models/sign_up_model.dart';
import 'package:review_subject/service/service.dart';
import 'package:dio/dio.dart' as dio;

class ModifyInfoFormController extends GetxController {
  // Rx<TextEditingController> fullNameController = TextEditingController().obs;
  // Rx<TextEditingController> emailController = TextEditingController().obs;

  RxList<String> schoolList = <String>["Chọn trường"].obs;
  RxMap<String, int> schoolListId = {"": 0}.obs;
  RxList<String> facultyList = <String>["Hãy chọn trường trước"].obs;
  RxMap<String, int> facultyListId = {"": 0}.obs;

  RxString currentSchool = "Chọn trường".obs;
  RxString currentFaculty = "Hãy chọn trường trước".obs;

  Rx<TextEditingController> fullNameTextController =
      TextEditingController().obs;
  Rx<TextEditingController> emailTextController = TextEditingController().obs;
  Rx<TextEditingController> majorTextController = TextEditingController().obs;

  RxBool isValidFullName = false.obs;
  RxBool isValidEmail = false.obs;
  RxBool isValidMajor = false.obs;
  RxBool isValidFaculty = false.obs;

  ProfileController profileController = Get.find();
  HomeController homeController = Get.find();

  late HttpService http = HttpService();

  ///Check if [fullNameTextController] is valid
  ///
  ///Change data of [isValidFullName]
  checkValidFullName() {
    fullNameTextController.value.text != ""
        ? isValidFullName.value = true
        : isValidFullName.value = false;
  }

  ///Check if [currentFaculty] is valid
  ///
  ///Change data of [isValidFaculty]
  checkValidFaculty() {
    currentFaculty.value != "Hãy chọn trường trước"
        ? isValidFaculty.value = true
        : isValidFaculty.value = false;
  }

  ///Check if [majorTextController] is valid
  ///
  ///Change data of [isValidMajor]
  checkValidMajor() {
    majorTextController.value.text != ""
        ? isValidMajor.value = true
        : isValidMajor.value = false;
  }

  ///Check if [emailTextController] is valid
  ///
  ///Change data of [isValidEmail]
  checkValidEmail() {
    if (emailTextController.value.text == "") {
      isValidEmail.value = false;
    } else {
      if (RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(emailTextController.value.text)) {
        isValidEmail.value = true;
      } else {
        isValidEmail.value = false;
      }
    }
  }

  ///Change data when click on change button
  ///
  ///Send [sendChangeProfileRequest] with
  ///param [profile], [userId], [userToken]
  ///Show warning snackbar if input data invalid
  onButtonChangeClick(
      {required BuildContext context, required Profile profile}) async {
    int userId = await MySharedPreference.getUserId();
    String userToken = await MySharedPreference.getToken();

    checkValidFullName();
    checkValidEmail();
    checkValidFaculty();
    checkValidMajor();

    if ([
      isValidEmail.value,
      isValidFaculty.value,
      isValidFullName.value,
      isValidMajor.value,
    ].any((element) => element == false)) {
      Get.snackbar("Cảnh báo", "Vui lòng kiểm tra lại thông tin!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
    } else {
      sendChangeProfileRequest(
          context: context, id: userId, token: userToken, profile: profile);
    }
  }

  ///Send change profile data request to server
  ///
  ///Send change data profile request to server
  ///with param [context], [id], [token], [profile],
  ///Show success snackbar if success, else
  ///show warning snackbar
  Future<void> sendChangeProfileRequest({
    required BuildContext context,
    required int id,
    required String token,
    required Profile profile,
  }) async {
    dio.Response response;
    try {
      showLoaderDialog(context);
      response = await http.putChangeProfileRequest(
          endPoint: "api/Users/$id",
          token: token,
          id: 1,
          status: 0,
          created: profile.user.created.toString(),
          updated: profile.user.updated.toString(),
          userName: "test",
          email: emailTextController.value.text,
          fullname: fullNameTextController.value.text,
          avatar: "string",
          // ignore: invalid_use_of_protected_member
          universityId: schoolListId.value[currentSchool]!,
          // ignore: invalid_use_of_protected_member
          facultyId: facultyListId.value[currentFaculty]!,
          major: majorTextController.value.text,
          point: 0);
      Get.back();
      if (response.statusCode == 200) {
        // print("RESPONSE: " + response.data.toString());

        String token = "";
        token = await MySharedPreference.getToken().then((_) async {
          var user = User.fromJson(response.data);
          var data = await http.getUniByIdRequest(
              endPoint: "api/Universities/${user.universityId}", token: token);
          String school = json.encode(data);

          MySharedPreference.setUniversity(university: school);

          // print("UniId: " + user.universityId.toString());
          homeController.displayedUni.value = SchoolModel.fromJson(data);

          return '';
        });

        homeController.loadDataFromLocal().then((_) {
          homeController.ressetHomeData();
          showDialogSuccess(context);
          Get.snackbar("", "Thay đổi thông tin thành công",
              colorText: Colors.green,
              backgroundColor: Colors.white,
              icon: const Icon(
                Icons.warning,
                color: Colors.green,
              ));
          Get.back();
        });
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nốiiii!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Load data of user need to change data
  ///
  ///Send get data request to server
  Future<void> loadData({required BuildContext context}) async {
    dio.Response response;
    try {
      showLoaderDialog(context);
      response = await http.getDataRequest(endPoint: "api/Universities");
      Get.back();
      if (response.statusCode == 200) {
        dynamic data = signUpResponseFromJson(response.data);
        schoolList.value =
            data.map<String>((SignUpResponse data) => data.name).toList();
        for (int i = 0; i < schoolList.length; i++) {
          schoolListId[data[i].name] = data[i].id;
        }
        // ignore: invalid_use_of_protected_member
        currentSchool.value = schoolList.value[0];
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");
        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  Future<void> loadFacultyBySchoolId(
      {required BuildContext context, required int id}) async {
    dio.Response response;
    try {
      showLoaderDialog(context);
      response = await http.getFacultyListRequest(
          endPoint: "api/Faculties/ByUniversityId?universityId=$id");
      Get.back();
      if (response.statusCode == 200) {
        List<Faculty> data = facultyResponseFromJson(response.data);
        facultyList.value =
            data.map<String>((Faculty data) => data.name).toList();
        for (int i = 0; i < facultyList.length; i++) {
          // ignore: invalid_use_of_protected_member
          facultyListId[facultyList.value[i]] = data[i].id;
        }
        // ignore: invalid_use_of_protected_member
        currentFaculty.value = facultyList.value[0];
      } else {
        Get.back();
        Get.snackbar("Cảnh báo", "Lỗi kết nối!",
            colorText: Colors.yellow.shade800,
            backgroundColor: Colors.white,
            icon: Icon(
              Icons.warning,
              color: Colors.yellow.shade800,
            ));
        // print("There is some problem status code ${response.statusCode}");
        return response.data;
      }
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nối!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  Future<void> showDialogSuccess(context) async {
    int userId = await MySharedPreference.getUserId();
    AwesomeDialog(
      context: context,
      // ignore: deprecated_member_use
      animType: AnimType.SCALE,
      // ignore: deprecated_member_use
      dialogType: DialogType.SUCCES,
      body: Center(
        child: Column(
          children: const [
            SizedBox(height: 10),
            Text(
              'Thành công!',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
            ),
            SizedBox(height: 7),
            Text('Thông tin của bạn đã được cập nhật')
          ],
        ),
      ),
      title: 'This is Ignored',
      desc: 'This is also Ignored',
      btnOkOnPress: () {
        profileController.loadProfileById(profileId: userId);
        Get.back();
      },
    ).show();
  }
}
