import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:review_subject/components/show_dialog.dart';
import 'package:review_subject/controllers/my_shared_preference.dart';
import 'package:review_subject/models/school_model.dart';
import 'package:review_subject/service/service.dart';

class SearchSchoolController extends GetxController {
  Rx<TextEditingController> keywordTextController = TextEditingController().obs;

  late HttpService http = HttpService();

  var fetchedList = <SchoolModel>[].obs;

  ///Get universities from server
  ///
  ///Send get universities request to server
  ///param [token]. Show warning snackbar if fails
  ///else change data of [fetchedList]
  Future<void> sendAllUniversityRequest(
      {required BuildContext context, required String token}) async {
    try {
      showLoaderDialog(context);
      var result = await http.getAllUniRequest(
          endPoint: "api/Universities", token: token);
      fetchedList.value = result;
      Get.back();
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nốiiii!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Search university by [keyword]
  ///
  ///Send get search uni request to server
  ///param [keyword], [token]. Show warning snackbar
  ///if fails, else change data of [fetchedList]
  Future<void> sendSearchUniversityRequest(
      {required BuildContext context,
      required String token,
      required String keyword}) async {
    try {
      showLoaderDialog(context);
      var result = await http.getSearchUniRequest(
          endPoint: "api/Search/university=$keyword", token: token);
      fetchedList.value = result;
      Get.back();
    } on Exception {
      Get.back();
      Get.snackbar("Cảnh báo", "Lỗi kết nốiiii!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
      // print(e);
    }
  }

  ///Call [sendSearchUniversityRequest]
  ///using param [keyword]
  onSearchButtonClick(
      {required BuildContext context, required String keyword}) async {
    String userToken = await MySharedPreference.getToken();
    if (keyword.isEmpty) {
      Get.snackbar("Cảnh báo", "Tên trường không được để trống!",
          colorText: Colors.yellow.shade800,
          backgroundColor: Colors.white,
          icon: Icon(
            Icons.warning,
            color: Colors.yellow.shade800,
          ));
    } else {
    sendSearchUniversityRequest(
        context: context, token: userToken, keyword: keyword);
    }
  }

  ///Get universities by default
  ///
  ///Send get search uni request to server
  ///Show warning snackbar
  ///if fails, else change data of [fetchedList]
  getDefaultListUni({required BuildContext context}) async {
    String userToken = await MySharedPreference.getToken();
    sendAllUniversityRequest(context: context, token: userToken);
  }
}
